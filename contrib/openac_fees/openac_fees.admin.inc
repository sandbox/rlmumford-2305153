<?php
/***
 * @file
 * Admin pages for openac_fees.
 */

/**
 * Menu callback; Course Fee Invoice Generation.
 */
function openac_fees_course_fee_generate($form, &$form_state) {
  // Retrieve our course options
  $courses = openac_course_options_list();

  $form['course_id'] = array(
    '#type' => 'select',
    '#title' => openac_entity_type_label('course'),
    '#description' => t('Select a @lcourse to generate invoices for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#options' => $courses,
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'openac_fees_course_fee_generate_course_options',
      'wrapper' => 'course-info-container',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['course_info'] = array(
    '#type' => 'container',
    '#id' => 'course-info-container',
  );

  if (!empty($form_state['values']['course_id'])) {
    if ($course = course_load($form_state['values']['course_id'])) {
      $form['course_info']['course_title'] = array(
        '#prefix' => '<h4>',
        '#markup' => $course->title,
        '#suffix' => '</h4>',
      );

      $price = field_get_items('course', $course, 'course_fee');
      $form['course_info']['course_price'] = array(
        '#type' => 'item',
        '#title' => t('@course Fees', array('@course' => openac_entity_type_label('course'))),
        '#markup' => $price[0]['amount'] / 100 . ' ' . $price[0]['currency_code'],
      );

      $query = db_select('openac_reports__invoice_status', 'invoice_status');
      $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
      $query->condition('cr.course', $course->id);
      $query->addExpression('COUNT(DISTINCT invoice_status.course_registration)', 'total');
      $student_counts = $query->execute()->fetchAssoc();

      $query = db_select('openac_reports__invoice_status', 'invoice_status');
      $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
      $query->fields('invoice_status', array('course_registration'))
        ->condition('cr.course', $course->id)
        ->groupBy('course_registration')
        ->having('count = 0');
      $query->addExpression('COUNT(DISTINCT invoice_status.status__course_fee__due) + COUNT(DISTINCT invoice_status.status__course_fee__pending)', 'count');

      $student_counts['to_generate'] = $query->countQuery()->execute()->fetchField();

      $form['course_info']['course_students'] = array(
        '#type' => 'item',
        '#title' => t('Number of Students'),
        '#markup' => $student_counts['total'],
      );

      $form['course_info']['to_generate'] = array(
        '#type' => 'item',
        '#title' => t('Un-Invoiced Students'),
        '#markup' => $student_counts['to_generate'],
      );

      $invoice_status = variable_get('openac_fees_generate_status', array());
      $form['course_info']['last_run'] = array(
        '#type' => 'item',
        '#title' => t('Invoices last generated'),
      );
      if (!empty($invoice_status['course_fees'][$course->id])) {
        $form['course_info']['last_run']['#markup'] = format_string('@interval (@date)', array(
          '@interval' => format_interval(time() - $invoice_status['course_fees'][$course->id]),
          '@date' => format_date($invoice_status['course_fees'][$course->id], 'long'),
        ));
      }
      else {
        $form['course_info']['last_run']['#markup'] = t('Never');
      }
    }
    else {
      $form['course_info']['error'] = array(
        '#prefix' => '<p>',
        '#markup' => t("Sorry, we couldn't find the @lcourse you selected.", array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
        '#suffix' => '</p>',
      );
    }
  }

  $form['options'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="course_id"]' => array('value' => ''),
      ),
    ),
  );

  $form['options']['deposit_add'] = array(
    '#type' => 'checkbox',
    '#title' => t('A deposit has been taken.'),
  );

  $form['options']['deposit'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="deposit_add"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['options']['deposit']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Deposit Amount'),
    '#default_value' => '250.00',
    '#size' => 10,
    '#field_suffix' => variable_get('commerce_default_currency'),
  );

  $form['options']['deposit']['currency_code'] = array(
    '#type' => 'value',
    '#default_value' => variable_get('commerce_default_currency'),
  );

  $form['options']['admin_fee_add'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add an administration fee.'),
  );

  $form['options']['admin_fee'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="admin_fee_add"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['options']['admin_fee']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin Fee Amount'),
    '#default_value' => '45.00',
    '#size' => 10,
    '#field_suffix' => variable_get('commerce_default_currency'),
  );

  $form['options']['admin_fee']['currency_code'] = array(
    '#type' => 'value',
    '#default_value' => variable_get('commerce_default_currency'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['help'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="course_id"]' => array('value' => ''),
      ),
    ),
  );

  $form['actions']['help']['help_text'] = array(
    '#prefix' => '<p>',
    '#markup' => t("Invoices will be generated in the 'Pending' state allowing you to review and make changes before sending them out."),
    '#suffix' => '</p>',
  );

  $form['actions']['course_info'] = array(
    '#type' => 'submit',
    '#value' => t('View @course Info', array('@course' => openac_entity_type_label('course'))),
    '#submit' => array('openac_fees_course_fee_generate_course_info'),
    '#attributes' => array(
      'class' => array('js-hide'),
    ),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate Invoices'),
    '#states' => array(
      'invisible' => array(
        ':input[name="course_id"]' => array('value' => ''),
      ),
    ),
  );

  return $form;
}

/**
 * Ajax callback for updating the course information.
 */
function openac_fees_course_fee_generate_course_options($form, $form_state) {
  return $form['course_info'];
}

/**
 * Validate callback for invoice generation.
 */
function openac_fees_course_fee_generate_validate(&$form, &$form_state) {
  if (empty($form_state['values']['course_id']) || !course_load($form_state['values']['course_id'])) {
    form_set_error('course_id', t('Please select a @lcourse to generate invoices for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
  }

  if (!empty($form_state['values']['deposit_add'])) {
    if (empty($form_state['values']['deposit']['amount'])) {
      form_set_error('deposit][amount', t('Please set an amount for the deposit.'));
    }
    if (empty($form_state['values']['deposit']['currency_code'])) {
      form_set_error('deposit][currency_code', t('Please choose a currency code for the deposit.'));
    }
  }

  if (!empty($form_state['values']['admin_fee_add'])) {
    if (empty($form_state['values']['admin_fee']['amount'])) {
      form_set_error('admin_fee][amount', t('Please set an amount for the admin fee.'));
    }
    if (empty($form_state['values']['admin_fee']['currency_code'])) {
      form_set_error('admin_fee][currency_code', t('Please choose a currency code for the admin fee.'));
    }
  }
}

/**
 * Submit callback for getting course info.
 */
function openac_fees_course_fee_generate_course_info($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit callback for invoice generation.
 */
function openac_fees_course_fee_generate_submit($form, &$form_state) {
  // Process our form values.
  $options = $form_state['values'];
  if (empty($options['deposit_add'])) {
    unset($options['deposit']);
  }
  unset($options['deposit_add']);
  if (empty($options['admin_fee_add'])) {
    unset($options['admin_fee']);
  }
  unset($options['admin_fee_add']);

  $batch = array(
    'title' => t('Generating Invoices'),
    'operations' => array(
      array('openac_fees_batch_invoices', array($options)),
    ),
    'finished' => 'openac_fees_batch_invoices_finished',
    'file' => drupal_get_path('module', 'openac_fees') . '/openac_fees.admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch operation callback for generating invoices.
 */
function openac_fees_batch_invoices($options, &$context) {
  $course_id = $options['course_id'];

  if (empty($context['sandbox'])) {
    $context['results']['course'] = course_load($course_id);
    // Set up our batch operation sandbox...
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['course'] = $context['results']['course'];
    $context['sandbox']['current_reg'] = 0;

    $query = db_select('openac_reports__invoice_status', 'invoice_status')
      ->fields('invoice_status', array('course_registration'));
    $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
    $query->condition('cr.course', $context['sandbox']['course']->id)
      ->groupBy('invoice_status.course_registration')
      ->having('count = 0');
    $query->addExpression('COUNT(DISTINCT invoice_status.status__course_fee__due) + COUNT(DISTINCT invoice_status.status__course_fee__pending)', 'count');

    $context['results']['count'] =
      $context['sandbox']['max'] = $query->countQuery()->execute()->fetchField();
    $context['results']['items'] = array();
  }

  // Get hold of our parties...
  $query = db_select('openac_reports__invoice_status', 'invoice_status');
  $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
  $query->fields('invoice_status', array('course_registration'))
    ->condition('cr.course', $context['sandbox']['course']->id)
    ->condition('invoice_status.course_registration', $context['sandbox']['current_reg'], '>')
    ->groupBy('invoice_status.course_registration')
    ->having('count = 0')
    ->range(0, 10);
  $query->addExpression('COUNT(DISTINCT invoice_status.status__course_fee__due) + COUNT(DISTINCT invoice_status.status__course_fee__pending)', 'count');
  $registrations = $query->execute()->fetchCol();

  // Iterate over, building out invoice
  module_load_include('inc', 'openac_fees', 'openac_fees.commerce_line_items');
  foreach ($registrations as $registration) {
    // Update the sandbox.
    $context['sandbox']['current_reg'] = $registration;
    $context['sandbox']['progress']++;

    // Generate our invoice and record our result.
    $context['results']['items'][] = openac_fees_generate_course_fee_order($registration, $options);
  }

  // Update our progress
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback for generating invoices.
 */
function openac_fees_batch_invoices_finished($success, $results, $operations) {
  // Check our main success variable and check no errors were reported.
  if ($success && !count(array_keys($results['items'], FALSE, TRUE))) {
    // All good, return a nice message.
    drupal_set_message(format_plural(count($results['items']),
      'One invoice generated for @course.',
      '@count invoices generated for @course.',
      array('@course' => entity_label('course', $results['course']))));

    // Store a record of it.
    $invoice_status = variable_get('openac_fees_generate_status', array());
    $invoice_status['course_fees'][$results['course']->id] = time();
    variable_set('openac_fees_generate_status', $invoice_status);
  }
  else {
    drupal_set_message(t('Finished with an error.'), 'error');
  }
}

///////////////////////////////////////////////////////////////////////////////
// Invoice sending batch
///////////////////////////////////////////////////////////////////////////////

/**
 * Menu callback; Course Fee Invoice Emailing.
 */
function openac_fees_course_fee_send_invoices($form, &$form_state) {

  $form['course_id'] = array(
    '#type' => 'select',
    '#title' => openac_entity_type_label('course'),
    '#description' => t('Select a @lcourse to send invoices for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#options' => openac_course_options_list(),
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'openac_fees_course_fee_send_invoices_course_options',
      'wrapper' => 'course-info-container',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['course_info'] = array(
    '#type' => 'container',
    '#id' => 'course-info-container',
  );

  if (!empty($form_state['values']['course_id'])) {
    if ($course = course_load($form_state['values']['course_id'])) {
      $form['course_info']['course_title'] = array(
        '#prefix' => '<h4>',
        '#markup' => format_string('@title - @year', array(
          '@title' => $course->title,
          '@year' => date("Y", $course->start),
        )),
        '#suffix' => '</h4>',
      );

      $query = db_select('openac_reports__invoice_status', 'invoice_status');
      $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
      $query->condition('cr.course', $course->id);
      $query->addExpression('COUNT(DISTINCT invoice_status.course_registration)', 'total');
      $query->addExpression('COUNT(DISTINCT invoice_status.status__course_fee__pending)', 'to_send');
      $student_counts = $query->execute()->fetchAssoc();

      $form['course_info']['course_students'] = array(
        '#type' => 'item',
        '#title' => t('Number of Students'),
        '#markup' => $student_counts['total'],
      );

      $form['course_info']['to_send'] = array(
        '#type' => 'item',
        '#title' => t('Un-Sent Invoices'),
        '#markup' => $student_counts['to_send'],
      );

      $invoice_status = variable_get('openac_fees_invoices_sent', array());
      $form['course_info']['last_run'] = array(
        '#type' => 'item',
        '#title' => t('Invoices last sent'),
      );
      if (!empty($invoice_status['course_fees'][$course->id])) {
        $form['course_info']['last_run']['#markup'] = format_string('@interval (@date)', array(
          '@interval' => format_interval(time() - $invoice_status['course_fees'][$course->id]),
          '@date' => format_date($invoice_status['course_fees'][$course->id], 'long'),
        ));
      }
      else {
        $form['course_info']['last_run']['#markup'] = t('Never');
      }
    }
    else {
      $form['course_info']['error'] = array(
        '#prefix' => '<p>',
        '#markup' => t("Sorry, we couldn't find the @lcourse you selected.", array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
        '#suffix' => '</p>',
      );
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['help'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        ':input[name="course_id"]' => array('value' => ''),
      ),
    ),
  );
  $form['actions']['help']['help_text'] = array(
    '#prefix' => '<p>',
    '#markup' => t("Invoices in the 'Pending' state will be sent to their invoicees."),
    '#suffix' => '</p>',
  );
  $form['actions']['course_info'] = array(
    '#type' => 'submit',
    '#value' => t('View @course Info', array('@course' => openac_entity_type_label('course'))),
    '#submit' => array('openac_fees_course_fee_generate_course_info'),
    '#attributes' => array(
      'class' => array('js-hide'),
    ),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send Invoices'),
    '#states' => array(
      'invisible' => array(
        ':input[name="course_id"]' => array('value' => ''),
      ),
    ),
  );

  return $form;
}

/**
 * Ajax callback for updating the course information.
 */
function openac_fees_course_fee_send_invoices_course_options($form, $form_state) {
  return $form['course_info'];
}

/**
 * Validate callback for invoice generation.
 */
function openac_fees_course_fee_send_invoices_validate($form, &$form_state) {
  if (empty($form_state['values']['course_id']) || !course_load($form_state['values']['course_id'])) {
    form_set_error('course_id', t('Please select a @lcourse to send the invoices for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
  }
}

/**
 * Submit callback for invoice generation.
 */
function openac_fees_course_fee_send_invoices_submit($form, &$form_state) {
  $batch = array(
    'title' => t('Sending Invoices'),
    'operations' => array(
      array('openac_fees_batch_send_invoices', array($form_state['values']['course_id'])),
    ),
    'finished' => 'openac_fees_batch_send_invoices_finished',
    'file' => drupal_get_path('module', 'openac_fees') . '/openac_fees.admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch operation callback for sending invoices.
 */
function openac_fees_batch_send_invoices($course_id, &$context) {
  if (empty($context['sandbox'])) {
    $context['results']['course'] = course_load($course_id);
    // Set up our batch operation sandbox...
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['course'] = $context['results']['course'];
    $context['sandbox']['current_reg'] = 0;
    $query = db_select('openac_reports__invoice_status', 'invoice_status');
    $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
    $query->condition('cr.course', $context['sandbox']['course']->id)
      ->condition('invoice_status.status', 'course_fee__pending');
    $query->addExpression('COUNT(DISTINCT invoice_status.course_registration)');
    $context['results']['count'] =
      $context['sandbox']['max'] = $query->execute()->fetchField();
    $context['results']['items'] = array();
  }

  // Get hold of our registrations...
  $query = db_select('openac_reports__invoice_status', 'invoice_status');
  $query->join('course_registration', 'cr', 'cr.id = invoice_status.course_registration');
  $query->fields('invoice_status', array('course_registration'))
    ->condition('cr.course', $context['sandbox']['course']->id)
    ->condition('invoice_status.status', 'course_fee__pending')
    ->condition('invoice_status.course_registration', $context['sandbox']['current_reg'], '>')
    ->orderBy('course_registration')
    ->range(0, 10);
  $registrations = $query->execute()->fetchCol();

  foreach ($registrations as $registration) {
    // Update the sandbox.
    $context['sandbox']['current_reg'] = $registration;
    $context['sandbox']['progress']++;

    // Generate our invoice and record our result.
    $context['results']['items'][] = openac_fees_send_invoice($registration);
  }

  // Update our progress
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch finished callback for sending out invoices.
 */
function openac_fees_batch_send_invoices_finished($success, $results, $operations) {
  // Check our main success variable and check no errors were reported.
  if ($success && !count(array_keys($results['items'], FALSE, TRUE))) {
    // All good, return a nice message.
    drupal_set_message(format_plural(count($results['items']),
      'One invoice sent for @course.',
      '@count invoices for @course sent.',
      array('@course' => entity_label('course', $results['course']))));

    // Store a record of it.
    $invoice_status = variable_get('openac_fees_invoices_sent', array());
    $invoice_status['course_fees'][$results['course']->course_id] = time();
    variable_set('openac_fees_invoices_sent', $invoice_status);
  }
  else {
    drupal_set_message(t('Finished with an error.'), 'error');
  }
}

/**
 * Form callback for the notify invoicee confirm form.
 */
function openac_fees_notify_confirm_form($form, &$form_state, $order) {
  $form_state['invoice'] = $order;

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $form_state['student'] = $party = $wrapper->party->value();
  $form_state['registration'] = $wrapper->invoice_course_registration->value();

  $form['#submit'][] = 'openac_fees_notify_confirm_form_submit';
  $form = confirm_form($form,
    t('Are you sure you want to notify @student of their invoice?', array('@student' => $party->label)),
    'admin/college/course-fees/invoices',
    '<p>' . t('Invoice notification will be sent to @email for invoice #@invoice.', array('@email' => $party->email, '@invoice' => $order->order_number)) . '</p>',
    t('Send'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for the notify invoicee confirm form.
 */
function openac_fees_notify_confirm_form_submit($form, &$form_state) {
  openac_fees_send_invoice($form_state['registration']->id, TRUE);
  $form_state['redirect'] = 'admin/college/course-fees/invoices';
}

///////////////////////////////////////////////////////////////////////////////
// Settings Forms
///////////////////////////////////////////////////////////////////////////////

/**
 * Form callback to the settings form.
 */
function openac_fees_settings($form, &$form_state) {
  $form['email_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Settings'),
  );

  $form['email_settings']['from_name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('From Name'),
    '#description' => t('The sender name displayed on @lcourse fee notification emails.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#default_value' => variable_get('openac_fees__email__from_name'),
  );
  $form['email_settings']['from_address'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('From Address'),
    '#description' => t('The email address from which fee notification emails should be sent.'),
    '#default_value' => variable_get('openac_fees__email__from_address'),
  );
  $form['email_settings']['subject'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Subject'),
    '#description' => t('The email subject.'),
    '#default_value' => variable_get('openac_fees__email__subject'),
  );
  $form['email_settings']['body'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => t('Message Body'),
    '#description' => t('The message body. The following tokens are available:') . '
      <br />!name - The full name of the student.
      <br />@firstname - The first name of the student.
      <br />@email - The email address of the student.
      <br />!link - The link to the students fees dashboard.
      <br />@course - The ' . openac_entity_type_label('course', FALSE, TRUE) . ' name.',
    '#default_value' => variable_get('openac_fees__email__body'),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 500,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );

  return $form;
}

function openac_fees_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('openac_fees__email__from_name', $values['from_name']);
  variable_set('openac_fees__email__from_address', $values['from_address']);
  variable_set('openac_fees__email__subject', $values['subject']);
  variable_set('openac_fees__email__body', $values['body']);

  drupal_set_message(t('The fees settings have been updated.'));
}
