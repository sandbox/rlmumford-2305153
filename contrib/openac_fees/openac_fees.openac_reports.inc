<?php
/**
 * @file
 *  Mysql View Definition For the Dashboard.
 */

/**
 * Implements hook_openac_reports_mysql_views_info().
 */
function openac_fees_openac_reports_mysql_views_info() {
  $views = array();
  $views['invoice_status'] = array(
    'label' => t('Invoice Status'),
    'description' => t('Cache table storing where student fee statuses.'),
    'primary_field' => 'course_registration',
    'fields' => array(
      'course_registration' => array(
        'type' => 'int',
        'description' => t('The @lcourse registration ID', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'location' => array(
        'type' => 'int',
        'description' => t('The location ID'),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'order_id' => array(
        'title' => t('Order'),
        'type' => 'int',
        'description' => t('The Commerce Order ID'),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'status' => array(
        'type' => 'varchar',
        'description' => t('The Invoice Status'),
        'length' => 255,
        'title' => t('Status'),
      ),
    ),
    'foreign keys' => array(
      'course_registration' => array(
        'table' => 'course_registration',
        'columns' => array('course_registration' => 'id'),
      ),
      'location' => array(
        'table' => 'opencrm_location',
        'columns' => array('location' => 'id'),
      ),
      'order_id' => array(
        'table' => 'commerce_order',
        'columns' => array('order_id' => 'order_id'),
      ),
    ),
  );

  // Build Query
  $query = db_select('party');

  // Require a join to the hats table
  $query->innerJoin('field_data_party_hat', 'party_hat',
    "party_hat.entity_id = party.pid AND party_hat.entity_type = :entity_type AND party_hat.deleted = :deleted AND party_hat.party_hat_hat_name = :hat_name",
    array(
      ':entity_type' => 'party',
      ':deleted' => 0,
      ':hat_name' => 'openac_enrolled',
    ));
  $query->leftJoin('course_registration', 'cr', 'cr.student = party.pid');
  $query->leftJoin('field_data_course_reg_location', 'crl', 'cr.id = crl.entity_id');
  $query->leftJoin('field_data_invoice_course_registration', 'icr', 'icr.invoice_course_registration_target_id = cr.id AND icr.entity_type = :commerce_order AND icr.deleted = :deleted',
    array(
      ':commerce_order' => 'commerce_order',
      ':deleted' => 0,
    ));
  $query->leftJoin('commerce_order', 'co', 'co.order_id = icr.entity_id AND co.status LIKE :status',
    array(':status' => 'course_fee__%'));

  $query->addField('cr', 'id', 'course_registration');
  $query->addField('crl', 'course_reg_location_target_id', 'location');
  $query->fields('co', array('order_id', 'status'));

  // Build our statuses
  $statuses = commerce_order_statuses(array('state' => 'course_fee'));
  foreach ($statuses as $key => &$status) {
    $title = $status['title'];

    if ($status['state'] == 'course_fee') {
      $status = $status['name'];
      $query->addExpression('IF(co.status = :status__' . $status . ', party.pid, NULL)',
        'status__' . $status,
        array(':status__' . $status => $status));
    }
    else {
      unset($statuses[$key]);
    }

    $views['invoice_status']['fields']['status__' . $status] = array(
      'type' => 'boolean',
      'title' => $title,
      'description' => t('Is the order at that the @status status?', array('@status' => $title)),
      'not null' => TRUE,
      'unsigned' => TRUE,
    );
  }

  $query->addExpression('IF(co.order_id IS NULL OR co.status NOT IN (:status__none), party.pid, NULL)',
      'status__none',
      array(':status__none' => $statuses));
  $title = t('- Unassigned -');
  $views['invoice_status']['fields']['status__none'] = array(
    'type' => 'boolean',
    'title' => $title,
    'description' => t('The incvoice does not have a status or does not extist.'),
    'not null' => TRUE,
    'unsigned' => TRUE,
  );

  $query->groupBy('cr.id');
  $query->groupBy('co.order_id');
  $views['invoice_status']['query'] = $query;


  return $views;
}
