<?php
/**
 * @file
 * Contains field definitions for course fees.
 */

$t = get_t();
$fields = array();

// Course Fee Field
$field_name = 'course_fee';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'commerce_price',
    'cardinality' => '1',
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'course',
    'bundle' => 'course',
    'label' => $t('@course Fee', array('@course' => openac_entity_type_label('course'))),
    'description' => $t('The yearly fee for studying this @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  ),
);

// Course Registration ER Field
$field_name = 'invoice_course_registration';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => '1',
    'settings' => array(
      'target_type' => 'course_registration',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'commerce_order',
    'bundle' => 'openac_fees',
    'label' => $t('Registration'),
    'description' => $t('The the @lcourse registration this invoice is for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  ),
);
