<?php
/***
 * @file openac_fees.views.inc
 */

/**
 * Implement hook_views_data_alter().
 */
function openac_fees_views_data_alter(&$data) {
  foreach ($data['commerce_order'] as $name => &$field) {
    if (!empty($field['field']['handler']) && $field['field']['handler'] == 'commerce_order_handler_field_order') {
      $field['field']['handler'] = 'openac_fees_handler_field_order';
    }
  }
}

