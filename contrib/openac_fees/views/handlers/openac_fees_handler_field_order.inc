<?php

/**
 * @file
 * Contains the basic order field handler adjusted for openac_fees.
 */

/**
 * Field handler to provide simple renderer that allows linking to an order.
 */
class openac_fees_handler_field_order extends commerce_order_handler_field_order {

  /**
   * Overload to add the uid and adjust the url.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_order']) && in_array($this->options['link_to_order'], array('customer', 'admin')) && $data !== NULL && $data !== '') {
      $uid = $this->get_value($values, 'uid');
      $order_id = $this->get_value($values, 'order_id');

      if ($this->options['link_to_order'] == 'customer') {
        if (empty($uid)) {
          // Retrieve the UID from the order
          $order = commerce_order_load($order_id);
          $party = field_get_items('commerce_order', $order, 'party');
          if (!empty($party[0]['target_id']) && $party = party_load($party[0]['target_id'])) {
            // Load our connection to the user.
            $user_controller = party_get_crm_controller($party, 'user');
            if ($user = $user_controller->getEntity()) {
              $uid = $user->uid;
            }
          }
        }

        if (!empty($uid)) {
          $this->options['alter']['make_link'] = TRUE;
          $this->options['alter']['path'] = 'user/' . $uid . '/my-fees/' . $order_id;
        }
      }
      elseif ($this->options['link_to_order'] == 'admin') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = 'admin/commerce/orders/' . $order_id;
      }
    }

    return $data;
  }
}
