<?php
$view = new view();
$view->name = 'commerce_user_orders';
$view->description = 'Display a list of completed orders for a user.';
$view->tag = 'commerce';
$view->base_table = 'commerce_order';
$view->human_name = 'User orders';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['title'] = 'My Fees';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'party_hat';
$handler->display->display_options['access']['hats'] = array(
  'enrolled' => 'enrolled',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = 25;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'created' => 'created',
  'commerce_order_total' => 'commerce_order_total',
  'status' => 'status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'commerce_order_total' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['text']['id'] = 'text';
$handler->display->display_options['empty']['text']['table'] = 'views';
$handler->display->display_options['empty']['text']['field'] = 'area';
$handler->display->display_options['empty']['text']['content'] = t('You do not have any @lcourse fee invoices.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));
$handler->display->display_options['empty']['text']['format'] = 'filtered_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['party_target_id']['id'] = 'party_target_id';
$handler->display->display_options['relationships']['party_target_id']['table'] = 'field_data_party';
$handler->display->display_options['relationships']['party_target_id']['field'] = 'party_target_id';
$handler->display->display_options['relationships']['party_target_id']['label'] = 'Student';
$handler->display->display_options['relationships']['party_target_id']['required'] = TRUE;
/* Relationship: Party: Attached User */
$handler->display->display_options['relationships']['eid_user']['id'] = 'eid_user';
$handler->display->display_options['relationships']['eid_user']['table'] = 'party_attached_entity';
$handler->display->display_options['relationships']['eid_user']['field'] = 'eid_user';
$handler->display->display_options['relationships']['eid_user']['relationship'] = 'party_target_id';
$handler->display->display_options['relationships']['eid_user']['label'] = 'User';
$handler->display->display_options['relationships']['eid_user']['required'] = TRUE;
$handler->display->display_options['relationships']['eid_user']['data_set'] = 'user';
$handler->display->display_options['relationships']['eid_user']['main'] = 0;
/* Field: Commerce Order: Created date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = '';
$handler->display->display_options['fields']['created']['exclude'] = TRUE;
$handler->display->display_options['fields']['created']['alter']['path'] = 'users/%user';
$handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['created']['date_format'] = 'month_year';
/* Field: Commerce Order: Order ID */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Date';
$handler->display->display_options['fields']['order_id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['order_id']['alter']['text'] = '[created]';
$handler->display->display_options['fields']['order_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['order_id']['link_to_order'] = 'customer';
/* Field: Commerce Order: Order total */
$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Total';
$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
  'calculation' => FALSE,
);
/* Field: Commerce Order: Order Balance */
$handler->display->display_options['fields']['balance']['id'] = 'balance';
$handler->display->display_options['fields']['balance']['table'] = 'commerce_order';
$handler->display->display_options['fields']['balance']['field'] = 'balance';
$handler->display->display_options['fields']['balance']['label'] = 'Balance Outstanding';
/* Field: Commerce Order: Order status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'commerce_order';
$handler->display->display_options['fields']['status']['field'] = 'status';
/* Field: Commerce Order: Order ID */
$handler->display->display_options['fields']['order_id_1']['id'] = 'order_id_1';
$handler->display->display_options['fields']['order_id_1']['table'] = 'commerce_order';
$handler->display->display_options['fields']['order_id_1']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id_1']['label'] = '';
$handler->display->display_options['fields']['order_id_1']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['order_id_1']['alter']['text'] = 'View Invoice';
$handler->display->display_options['fields']['order_id_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['order_id_1']['link_to_order'] = 'customer';
/* Sort criterion: Commerce Order: Created date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'commerce_order';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: User: Uid */
$handler->display->display_options['arguments']['uid']['id'] = 'uid';
$handler->display->display_options['arguments']['uid']['table'] = 'users';
$handler->display->display_options['arguments']['uid']['field'] = 'uid';
$handler->display->display_options['arguments']['uid']['relationship'] = 'eid_user';
$handler->display->display_options['arguments']['uid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['uid']['title'] = 'User %1';
$handler->display->display_options['arguments']['uid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['uid']['validate']['type'] = 'current_user_or_role';
$handler->display->display_options['arguments']['uid']['validate_options']['restrict_roles'] = TRUE;
$handler->display->display_options['arguments']['uid']['validate_options']['roles'] = array(
  3 => '3',
);
/* Filter criterion: Commerce Order: Order state */
$handler->display->display_options['filters']['state_1']['id'] = 'state_1';
$handler->display->display_options['filters']['state_1']['table'] = 'commerce_order';
$handler->display->display_options['filters']['state_1']['field'] = 'state';
$handler->display->display_options['filters']['state_1']['value'] = array(
  'course_fee' => 'course_fee',
);
/* Filter criterion: Commerce Order: Order status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'commerce_order';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['operator'] = 'not in';
$handler->display->display_options['filters']['status']['value'] = array(
  'course_fee__pending' => 'course_fee__pending',
);

/* Display: My Fees */
$handler = $view->new_display('page', 'My Fees', 'my_fees_page');
$handler->display->display_options['enabled'] = FALSE;
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'user/%/my-fees';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'My Fees';
$handler->display->display_options['menu']['weight'] = '15';
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Orders';
$handler->display->display_options['tab_options']['description'] = 'User orders in the store.';
$handler->display->display_options['tab_options']['weight'] = '';
$handler->display->display_options['tab_options']['name'] = 'user-menu';

/* Display: Course Fees */
$handler = $view->new_display('panel_pane', t('@course Fees', array('@course' => openac_entity_type_label('course'))), 'panel_pane_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = t('@course Fees', array('@course' => openac_entity_type_label('course')));
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['text']['id'] = 'text';
$handler->display->display_options['empty']['text']['table'] = 'views';
$handler->display->display_options['empty']['text']['field'] = 'area';
$handler->display->display_options['empty']['text']['content'] = t('No @lcourse fee invoices for this student.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));
$handler->display->display_options['empty']['text']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Commerce Order: Created date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'commerce_order';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = '';
$handler->display->display_options['fields']['created']['exclude'] = TRUE;
$handler->display->display_options['fields']['created']['alter']['path'] = 'users/%user';
$handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['created']['date_format'] = 'month_year';
/* Field: Commerce Order: Order ID */
$handler->display->display_options['fields']['order_id']['id'] = 'order_id';
$handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
$handler->display->display_options['fields']['order_id']['field'] = 'order_id';
$handler->display->display_options['fields']['order_id']['label'] = 'Date';
$handler->display->display_options['fields']['order_id']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['order_id']['alter']['text'] = '[created]';
$handler->display->display_options['fields']['order_id']['element_label_colon'] = FALSE;
/* Field: Commerce Order: Order total */
$handler->display->display_options['fields']['commerce_order_total']['id'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['table'] = 'field_data_commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['field'] = 'commerce_order_total';
$handler->display->display_options['fields']['commerce_order_total']['label'] = 'Total';
$handler->display->display_options['fields']['commerce_order_total']['click_sort_column'] = 'amount';
$handler->display->display_options['fields']['commerce_order_total']['type'] = 'commerce_price_formatted_amount';
$handler->display->display_options['fields']['commerce_order_total']['settings'] = array(
  'calculation' => FALSE,
);
/* Field: Commerce Order: Order status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'commerce_order';
$handler->display->display_options['fields']['status']['field'] = 'status';
/* Field: Commerce Order: Operations links */
$handler->display->display_options['fields']['operations']['id'] = 'operations';
$handler->display->display_options['fields']['operations']['table'] = 'commerce_order';
$handler->display->display_options['fields']['operations']['field'] = 'operations';
$handler->display->display_options['fields']['operations']['label'] = '';
$handler->display->display_options['fields']['operations']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Commerce Order: Student (party) */
$handler->display->display_options['arguments']['party_target_id']['id'] = 'party_target_id';
$handler->display->display_options['arguments']['party_target_id']['table'] = 'field_data_party';
$handler->display->display_options['arguments']['party_target_id']['field'] = 'party_target_id';
$handler->display->display_options['arguments']['party_target_id']['default_action'] = 'empty';
$handler->display->display_options['arguments']['party_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['party_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['party_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['party_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['party_target_id']['validate']['type'] = 'numeric';
$handler->display->display_options['arguments']['party_target_id']['validate']['fail'] = 'empty';
$handler->display->display_options['argument_input'] = array(
  'party_target_id' => array(
    'type' => 'user',
    'context' => 'entity:application.field-app-res-att',
    'context_optional' => 0,
    'panel' => '0',
    'fixed' => '',
    'label' => 'Party',
  ),
);

/* Display: My Fees */
$handler = $view->new_display('panel_pane', 'My Fees', 'panel_pane_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['argument_input'] = array(
  'uid' => array(
    'type' => 'user',
    'context' => 'user_edit_form.uid',
    'context_optional' => 0,
    'panel' => '0',
    'fixed' => '',
    'label' => 'User',
  ),
);
