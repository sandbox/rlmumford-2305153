<?php
/**
 * @file
 * openac_fees.pages_default.inc
 */

/**
 * Implementation of hook_default_page_manager_pages().
 */
function openac_fees_default_page_manager_pages() {
  $export = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/default_page_manager_pages', '/\.panel$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$page->name] = $page;
    }
  }

  return $export;
}
