<?php
/***
 * @file
 * Implements the line item type hooks and contains our various line item
 * functions and helpers.
 */

/**
 * Implements hook_commerce_line_item_type_info().
 *
 * Defines the line item for course fees.
 */
function openac_fees_commerce_line_item_type_info() {
  $line_item_types['course_fees'] = array(
    'type' => 'course_fees',
    'name' => 'Course Fees', //t('@course Fees', array('@course' => openac_entity_type_label('course'))),
    'description' => 'Line item for course fees', //t('Line item for @lcourse fees.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'product' => FALSE,
    'add_form_submit_value' => 'Add Course Fees', //t('Add @course Fees', array('@course' => openac_entity_type_label('course'))),
    'callbacks' => array(
      'title' => 'openac_fees_course_fees_title',
      'add_form_submit' => 'openac_fees_course_fees_add_course_details',
    ),
  );
  $line_item_types['custom_item'] = array(
    'type' => 'custom_item',
    'name' => t('Custom Item'),
    'description' => t('Line item for custom charges/discounts.'),
    'product' => FALSE,
    'add_form_submit_value' => t('Add Item'),
    'callbacks' => array(
      'title' => 'openac_fees_custom_item_title',
      'add_form' => 'openac_fees_custom_item_add_form',
      'add_form_submit' => 'openac_fees_custom_item_add_form_submit',
    ),
  );
  return $line_item_types;
}

/**
 * Retrieve a course from a line item.
 *
 * Looks up the course for a line item based off the student the line item
 * connects to. The line item must have an order_id, the order a connected
 * party and the party a student profile with an assigned course.
 *
 * @param $line_item
 *   A commerce line item.
 * @param $order
 *   The commerce order entity. If adding a line item to a new order, you
 *   might need this to form the connection as there's no order_id on the
 *   line item.
 *
 * @return
 *   A course entity if it exists, or FALSE.
 */
function _openac_fees_course_from_line_item($line_item, $order = NULL) {
  if (!isset($order) && (empty($line_item->order_id) || !($order = commerce_order_load($line_item->order_id)))) {
    // Can't load an order, so there's nothing we can do here.
    return FALSE;
  }

  $party = field_get_items('commerce_order', $order, 'party');
  if (!empty($party[0]['target_id']) && $party = party_load($party[0]['target_id'])) {
    // Load our connection to the course.
    $course_id = db_select('course_registration', 'cr')
      ->condition('student', $party->pid)
      ->fields('cr', array('course'))
      ->execute()
      ->fetchField();
  }

  return !empty($course_id) ? entity_load_single('course', $course_id): FALSE;
}

/**
 * Title callback for course fees line item.
 */
function openac_fees_course_fees_title($line_item) {
  $ids = explode(':', $line_item->line_item_label);
  if ($entity = entity_load_single($ids[0], $ids[1])) {
    return t('@label @course Fees', array('@label' => entity_label($ids[0], $entity), '@course' => openac_entity_type_label('course')));
  }
  else {
    return t('@course Fees', array('@course' => openac_entity_type_label('course')));
  }
}

/**
 * Generate label and price for a line item.
 *
 * @param $line_item
 *   A commerce_line_item that we're working on.
 *
 * Looks up the course to generate a title and price for a line item based off
 * the student the line item connects to. This function operates as the
 * add_form_submit callback on the commerce_line_item_type.
 *
 * @return
 *   Normally doesn't return anything, but in case of an error, returns an
 *   error message.
 */
function openac_fees_course_fees_add_course_details(&$line_item, $element = NULL) {

  if (empty($element)) {
    $course = _openac_fees_course_from_line_item($line_item);
  }
  else {
    $course = _openac_fees_course_from_line_item($line_item, $element['#entity']);
  }

  if (!$course) {
    return t('Please make sure this order is linked to a student.');
  }

  $line_item->line_item_label = 'course:' . $course->id;

  // Wrap the line item and product to easily set field information.
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Set our initial price.
  $line_item->commerce_unit_price = $course->course_fee;

  if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
    // Add the base price to the components array.
    if (!commerce_price_component_load($line_item_wrapper->commerce_unit_price->value(), 'base_price')) {
      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        $line_item_wrapper->commerce_unit_price->value(),
        'base_price',
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE
      );
    }
  }
}

/**
 * Create a course fee line item.
 *
 * To generate a full line item with label and pricing, the order must have
 * a full path to a course as described in
 * _openac_fees_course_from_line_item().
 *
 * @param $order_id
 *   A commerce order id.
 *
 * @return
 *   A commerce line item entity with it's fields populated.
 */
function openac_fees_course_fees_new($order_id = NULL) {
  // Create the new line item.
  $line_item = entity_create('commerce_line_item', array(
    'type' => 'course_fees',
    'order_id' => $order_id,
    'quantity' => 1,
  ));

  // Add our details
  openac_fees_course_fees_add_course_details($line_item);

  return $line_item;
}

/**
 * Title callback for custom item line item.
 */
function openac_fees_custom_item_title($line_item) {
  return $line_item->line_item_label;
}

/**
 * Generate the form for adding this line item.
 */
function openac_fees_custom_item_add_form($element, &$form_state) {
  $form = array();

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#required' => TRUE,
  );

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#size' => 10,
    '#field_suffix' => variable_get('commerce_default_currency'),
    '#required' => TRUE,
  );

  $form['currency_code'] = array(
    '#type' => 'value',
    '#default_value' => variable_get('commerce_default_currency'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Add line item form submission.
 *
 * @param $line_item
 *   A commerce_line_item that we're working on.
 * @param $element
 *   The form element from the add line item form or NULL to use $options.
 *
 * Use the submitted values to generate a line item.
 *
 * @return
 *   Normally doesn't return anything, but in case of an error, returns an
 *   error message.
 */
function openac_fees_custom_item_add_form_submit(&$line_item, $element) {
  // Call our helper function.
  openac_fees_custom_item_add_line_item($line_item, array(
    'label' => $element['actions']['label']['#value'],
    'amount' => $element['actions']['amount']['#value'],
    'currency_code' => $element['actions']['currency_code']['#value'],
  ));
}

/**
 * Add the label and price to a line item from a set of options.
 *
 * @param $line_item
 *   A commerce_line_item that we're working on.
 * @param $options
 *   If $element is null, an array of options:
 *    - 'label'
 *    - 'amount' (in pounds)
 *    - 'currency_code'
 */
function openac_fees_custom_item_add_line_item($line_item, $options) {
  // Add the label
  $line_item->line_item_label = $options['label'];

  // Wrap the line item and product to easily set field information.
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Set our initial price.
  $line_item->commerce_unit_price = array(LANGUAGE_NONE => array(
    array(
      'amount' => $options['amount'] * 100,
      'currency_code' => $options['currency_code'],
    ),
  ));

  if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
    // Add the base price to the components array.
    if (!commerce_price_component_load($line_item_wrapper->commerce_unit_price->value(), 'base_price')) {
      $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
        $line_item_wrapper->commerce_unit_price->value(),
        'base_price',
        $line_item_wrapper->commerce_unit_price->value(),
        TRUE
      );
    }
  }
}

/**
 * Create a course fee line item.
 *
 * To generate a full line item with label and pricing, the order must have
 * a full path to a course as described in
 * _openac_fees_course_from_line_item().
 *
 * @param $order_id
 *   A commerce order id.
 * @param $options
 *   An array of options:
 *    - 'label'
 *    - 'amount'
 *    - 'currency_code'
 *
 * @return
 *   A commerce line item entity with it's fields populated.
 */
function openac_fees_custom_item_new($order_id = NULL, $options) {
  // Create the new line item.
  $line_item = entity_create('commerce_line_item', array(
    'type' => 'custom_item',
    'order_id' => $order_id,
    'quantity' => 1,
  ));

  // Add our details.
  openac_fees_custom_item_add_line_item($line_item, $options);

  return $line_item;
}
