<?php
/***
 * @file
 * Order helpers for wtc invoicing.
 */

/**
 * Implements hook_commerce_order_state_info().
 *
 * Define our custom order state for invoice processing.
 */
function openac_fees_commerce_order_state_info() {
  $order_states['course_fee'] = array(
    'name' => 'course_fee',
    'title' => t('@course Fee', array('@course' => openac_entity_type_label('course'))),
    'description' => t('Orders in this state are specifically to do with @lcourse fee invoicing.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'weight' => 50,
    'default_status' => 'pending',
  );
  return $order_states;
}

/**
 * Implements hook_commerce_order_status_info().
 *
 * Define our custom order statuses for invoice processing.
 */
function openac_fees_commerce_order_status_info() {
  $weight = 0;
  $order_statuses['course_fee__pending'] = array(
    'name' => 'course_fee__pending',
    'title' => t('Not Sent'),
    'state' => 'course_fee',
    'weight' => $weight++,
  );
  $order_statuses['course_fee__due'] = array(
    'name' => 'course_fee__due',
    'title' => t('Due'),
    'state' => 'course_fee',
    'weight' => $weight++,
  );
  $order_statuses['course_fee__completed'] = array(
    'name' => 'course_fee__completed',
    'title' => t('Paid'),
    'state' => 'course_fee',
    'weight' => $weight++,
  );
  return $order_statuses;
}

/**
 * Function to create an order.
 *
 * @param $id
 *   ID of the course registration we're generating for.
 * @param $options
 *   An array of options. This can include:
 *    - 'deposit': an array of 'amount' and 'currency_code'.
 *    - 'admin_fee': an array of 'amount' and 'currency_code'.
 *
 * @return
 *   A commerce order.
 */
function openac_fees_generate_course_fee_order($id, $options) {
  $registration = entity_metadata_wrapper('course_registration', $id);

  // Build our order.
  if ($order = commerce_order_new(0, 'course_fee__pending', 'openac_fees')) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->party->set($registration->student->getIdentifier());
    $order_wrapper->invoice_course_registration->set($registration->getIdentifier());
    entity_save('commerce_order', $order);

    // Build our couse fee line item.
    $line_item = openac_fees_course_fees_new($order->order_id);
    entity_save('commerce_line_item', $line_item);
    $order_wrapper->commerce_line_items[] = $line_item;

    // Build our admin fee line item.
    if (!empty($options['admin_fee'])) {
      $options['admin_fee']['label'] = t('Instalments Admin Fee');
      $line_item = openac_fees_custom_item_new($order->order_id, $options['admin_fee']);
      entity_save('commerce_line_item', $line_item);
      $order_wrapper->commerce_line_items[] = $line_item;
    }

    // Add in the waiver bursary waiver
    // Get the application
    // @todo: Move this to a hook somewhere.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'application');
    $query->propertyCondition('party', $order_wrapper->party->getIdentifier());
    $query->propertyOrderBy('type', 'DESC');
    $ents = $query->execute();

    $application_id = key($ents['application']);
    $application = application_load($application_id);
    if ($application && !empty($application->field_app_bursary_waiver)) {
      $options = array(
        'label' => t('Bursary'),
        'amount' => 0 - ($application->field_app_bursary_waiver[LANGUAGE_NONE][0]['amount'] / 100),
        'currency_code' => $application->field_app_bursary_waiver[LANGUAGE_NONE][0]['currency_code'],
      );

      $line_item = openac_fees_custom_item_new($order->order_id, $options);
      entity_save('commerce_line_item', $line_item);
      $order_wrapper->commerce_line_items[] = $line_item;
    }

    // Add our deposit payment
    if (!empty($options['deposit'])) {
      $payment_method = commerce_payment_method_load('openac_fees_manual_payment');
      $options['deposit']['amount'] = $options['deposit']['amount'] * 100;
      openac_fees_manual_payment_transaction($payment_method, $order, $options['deposit'], t('Deposit'));
      // The transaction sets the status to course_fee__completed, so here we
      // set it back.
      $order_wrapper->status->set('course_fee__pending');
    }

    entity_save('commerce_order', $order);
    return $order;
  }

  // If we've got here there's been a problem.
  return FALSE;
}

/**
 * Implements hook_commerce_payment_totals_row_info_alter().
 */
function openac_fees_commerce_payment_totals_row_info_alter(&$rows, $totals, $order) {
  foreach ($rows as &$row) {
    if ($row['data'][0]['data'] == t('Order balance')) {
      $row['data'][0]['data'] = t('Balance Outstanding');
    }
  }
}
