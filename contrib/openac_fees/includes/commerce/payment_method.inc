<?php
/***
 * @file
 * Implements the payment method hooks and contains functions and helpers.
 */

/**
 * Implements hook_commerce_payment_method_info().
 */
function openac_fees_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['openac_fees_manual_payment'] = array(
    'title' => t('Manual Payment'),
    'description' => t('Track offline payments.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function openac_fees_manual_payment_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();

  // Merge in values from the order.
  if (!empty($order->data['openac_fees_manual_payment'])) {
    $pane_values += $order->data['openac_fees_manual_payment'];
  }

  // Merge in default values.
  $pane_values += array(
    'reference' => '',
  );

  $form['reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Reference'),
    '#description' => t('Enter a reference or description for the transaction to aid book keeping.'),
    '#default_value' => $pane_values['reference'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Payment method callback: submit form submission.
 */
function openac_fees_manual_payment_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['openac_fees_manual_payment'] = $pane_values;

  openac_fees_manual_payment_transaction($payment_method, $order, $charge, $pane_values['reference']);
}

/**
 * Creates a manual payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $reference
 *   The reference entered on the submission form.
 */
function openac_fees_manual_payment_transaction($payment_method, $order, $charge, $reference) {
  $transaction = commerce_payment_transaction_new('openac_fees_manual_payment', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = '@reference';
  $transaction->message_variables = array('@reference' => $reference);

  commerce_payment_transaction_save($transaction);
}
