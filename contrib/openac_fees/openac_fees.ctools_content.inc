<?php
/**
 * @file
 * openac_fees.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function openac_fees_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'course_fee__address_logo';
  $content->admin_title = 'Address and Logo';
  $content->admin_description = '';
  $content->category = t('@course Fees', array('@course' => openac_entity_type_label('course')));
  $content->settings = array(
    'admin_title' => 'Address and Logo',
    'title' => '',
    'body' => '<img src="/sites/all/themes/wtc2/logo.png" class="logo" />Suite 5, Winchcombe House
  5-39 Winchcombe Street
  Cheltenham
  GL52 2LZ',
    'format' => 'full_html',
    'substitute' => 0,
  );
  $export[$content->name] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'course_fee__footer';
  $content->admin_title = 'Footer';
  $content->admin_description = '';
  $content->category = t('@course Fees', array('@course' => openac_entity_type_label('course')));
  $content->settings = array(
    'admin_title' => 'Footer',
    'title' => '',
    'body' => 'Many thanks,

  Bill Westwood
  Chief Operating Officer

  <div class="fixed-footer">
  <strong style="color:#ADD136">WTC</strong> Suite 5, Winchcombe House, 5-39 Winchombe Street, Cheltenham, GL52 2LZ
  T: 030 0040 6200 | W: <a href="http://www.wtctheology.org.uk">www.wtctheology.org.uk</a>
  A Company Registeredd in England and Wales Number 06506007 and a Register Charity Number 1123573
  </div>',
    'format' => 'full_html',
    'substitute' => 0,
  );
  $export[$content->name] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'course_fee__payment_methods';
  $content->admin_title = 'Payment Methods';
  $content->admin_description = '';
  $content->category = t('@course Fees', array('@course' => openac_entity_type_label('course')));
  $content->settings = array(
    'admin_title' => 'Payment Methods',
    'title' => 'Payment Methods',
    'body' => '<ul class="payment-methods">
  <li><strong>By cheque</strong> - made payable to \'Westminster Theological Centre\'. Please send to the office address above.</li>
  <li><strong>By direct back transfer</strong> to our account as follows:
  <dl>
  <dt>Bank name:</dt><dd>Co-operative Bank</dd>
  <dt>Bank address:</dt><dd>PO Box 250, Delf House, Southway, Skelmersdale, WN8 6TW</dd>
  <dt>Account name:</dt><dd>Westminster Theological Centre</dd>
  <dt>Account no:</dt><dd>65483125</dd>
  <dt>Sort code:</dt><dd>08-92-99</dd>
  </dd>
  Please include our invoice number in your payment reference.</li>
  </ul>',
    'format' => 'panopoly_wysiwyg_text',
    'substitute' => 0,
  );
  $export[$content->name] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'user_dashboard__my_fees_intro';
  $content->admin_title = 'User Dashboard - My Fees Intro';
  $content->admin_description = '';
  $content->category = 'User Dashboard';
  $content->settings = array(
    'admin_title' => 'User Dashboard - My Fees Intro',
    'title' => '',
    'body' => '<h4>Please note that all invoices will be updated following the Residential. If you have sent a payment through and have not heard anything from us please consider your money received. WTC will be in touch if a payment is missing before the Residential.<br /><br />This page is updated by the 1st of the month for all the payments received in the prior month.</h4>',
    'format' => 'panopoly_wysiwyg_text',
    'substitute' => 0,
  );
  $export['user_dashboard__my_fees_intro'] = $content;

  return $export;
}
