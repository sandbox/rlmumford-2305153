(function($){
  Drupal.behaviors.openAcLocationCourseLocationJointSelector = {
    attach: function (context, settings) {
      var defaults = {
        operator: '==',
        comparison: '1',
        actions: ['hide', 'show', 'hide'],
      },
      runAction = function(target, action) {
        if (action == 'enable') {
          target.attr('disabled', false);
        }
        else if (action == 'disable') {
          target.attr('disabled', 'disabled');
        }
        else {
          try {
            target[action]();
          } catch (err) {}
        }
      };
      $.each(settings.openac_location_joint.conditional_elements, function(i) {
        // Extend the defaults
        var settings = $.extend({}, defaults, this);

        // Build our control and target
        settings.controls = $(settings.controls);
        settings.target = $(settings.target);

        // Create our handler and fire it for the selected item
        settings.controls.change(function(){
          // Set our initial state
          var initialised = settings.controls.data('course_location_joint_initialised');
          if (!initialised) {
            initialised = [];
          }
          if (!initialised[i] && settings.actions[0]) {
            runAction(settings.target, settings.actions[0]);
            initialised[i] = true;
            settings.controls.data('course_location_joint_initialised', initialised);
          }

          if (!this.checked) {
            // Only interested if this is the selected value
            return;
          }

          // If there are any before actions, let's run them
          if (settings.before) {
            runAction(settings.target, settings.before);
          }

          // Check we want to do a comparative action
          if (settings.actions[1] || settings.actions[2]) {
            // Compare our value
            if (eval('this.value ' + settings.operator + ' settings.comparison')) {
              runAction(settings.target, settings.actions[1]);

              // Trigger any child change functions on the target
              settings.target.find(':input').removeData('course_location_joint_initialised').trigger('change');
            }
            else {
              runAction(settings.target, settings.actions[2]);
            };
          }

          // If there are any before actions, let's run them
          if (settings.after) {
            runAction(settings.target, settings.after);
          }

        }).trigger('change');
      });
    }
  };
})(jQuery);
