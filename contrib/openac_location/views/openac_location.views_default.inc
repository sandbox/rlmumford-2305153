<?php
/**
 * @file
 * Provides the default views.
 */

/**
 * Implements hook_views_default_views_alter().
 */
function openac_location_views_default_views_alter(&$views) {
  // Scan this directory for any .view files
  $files = file_scan_directory(dirname(__FILE__), '/\.view_alter$/', array('key' => 'name'));
  foreach ($files as $key => $file) {
    $view = $views[$key];
    if ((include $file->uri) == 1) {
      $views[$view->name] = $view;
    }
  }
}
