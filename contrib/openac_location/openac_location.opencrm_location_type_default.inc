<?php
/**
 * @file
 * Contains default configuration for location types.
 */

/**
 * Implements hook_default_opencrm_location_type().
 */
function openac_location_default_opencrm_location_type() {
  $t = get_t();

  $types = array();
  $types['openac_location'] = entity_create('opencrm_location_type', array(
    'type' => 'openac_location',
    'label' => $t('Academy Location'),
    'reusable' => TRUE,
  ));
  return $types;
}
