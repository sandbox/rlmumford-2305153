<?php
/**
 * @file
 *  Mysql View Definition For the Dashboard.
 */

/**
 * Implements hook_openac_reports_mysql_views_info().
 */
function openac_location_openac_reports_mysql_views_info() {
  $views = array();
  $views['student_locations'] = array(
    'label' => t('Student Locations'),
    'description' => t('Cache table storing where students are studying.'),
    'primary_field' => 'course_registration',
    'fields' => array(
      'course_registration' => array(
        'type' => 'int',
        'description' => t('The @lcourse registration ID', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'location' => array(
        'type' => 'int',
        'description' => t('The location ID'),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
    ),
    'foreign keys' => array(
      'course_registration' => array(
        'table' => 'course_registration',
        'columns' => array('course_registration' => 'id'),
      ),
      'location' => array(
        'table' => 'opencrm_location',
        'columns' => array('location' => 'id'),
      ),
    ),
  );

  // Build Query
  $query = db_select('party');

  // Require a join to the hats table
  $query->innerJoin('field_data_party_hat', 'party_hat',
    "party_hat.entity_id = party.pid AND party_hat.entity_type = :entity_type AND party_hat.deleted = :deleted AND party_hat.party_hat_hat_name = :hat_name",
    array(
      ':entity_type' => 'party',
      ':deleted' => 0,
      ':hat_name' => 'openac_enrolled',
    ));
  $query->leftJoin('course_registration', 'cr', 'cr.student = party.pid');
  $query->leftJoin('field_data_course_reg_location', 'crl', 'cr.id = crl.entity_id');

  $query->addField('cr', 'id', 'course_registration');
  $query->addField('crl', 'course_reg_location_target_id', 'location');

  // Build our statuses
  $locations = db_select('opencrm_location', 'l')
    ->condition('type', 'openac_location')
    ->fields('l', array('id', 'title'))
    ->execute()
    ->fetchAllKeyed();

  $useNull = TRUE;
  foreach ($locations as $id => $title) {
    $query->addExpression('IF(crl.course_reg_location_target_id = :location__' . $id . ', cr.id, null)',
      'location__' . $id,
      array(':location__' . $id => $id));

    $views['student_locations']['fields']['location__'.$id] = array(
      'type' => 'boolean',
      'title' => $title,
      'description' => t('Does the student study at @location', array('@location' => $title)),
      'not null' => TRUE,
      'unsigned' => TRUE,
    );
  }
  $query->addExpression('IF(crl.course_reg_location_target_id IS NULL, cr.id, null)',
    'location__unassigned');
  $title = t('- Unassigned -');
  $views['student_locations']['fields']['location__unassigned'] = array(
    'type' => 'boolean',
    'title' => $title,
    'description' => t('Is the student not registered at any location?'),
    'not null' => TRUE,
    'unsigned' => TRUE,
  );

  $query->groupBy('cr.id');
  $views['student_locations']['query'] = $query;


  return $views;
}
