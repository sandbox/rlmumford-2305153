<?php
/**
 * @file
 * Contains OpenAcLocationFlexiformElementCourseLocationJoint
 */

class OpenAcLocationFlexiformElementCourseLocationJoint extends FlexiformElement {

  public function form($form, &$form_state, $application, $language = LANGUAGE_NONE) {
    $parents = $form['#parents'];
    $parents[] = 'course_location_joint';

    $form[$this->element_namespace] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#parents' => $parents,
    );

    // Add the two fields.
    field_attach_form('application', $application, $form[$this->element_namespace], $form_state, $language, array('field_name' => 'application_course'));
    field_attach_form('application', $application, $form[$this->element_namespace], $form_state, $language, array('field_name' => 'application_location'));

    // Work out the element names.
    $base_element_name = array_shift($parents);
    while ($parent = array_shift($parents)) {
      $base_element_name .= "[{$parent}]";
    }
    $location_element_name = $base_element_name . "[application_location][{$language}]";
    $course_element_name = $base_element_name . "[application_course][{$language}]";

    // Alter the options available.
    $application_type = entity_load_single('application_type', $application->type);
    $course_select = &$form[$this->element_namespace]['application_course'][LANGUAGE_NONE];
    foreach ($course_select['#options'] as $id => $label) {
      if ($id == '_none') {
        continue;
      }

      if (!in_array($id, $application_type->data['courses'])) {
        unset($course_select['#options'][$id]);
      }
    }

    $location_select = &$form[$this->element_namespace]['application_location'][LANGUAGE_NONE];
    $not_in_use_ids = db_select('opencrm_location', 'l')
      ->fields('l', array('id'))
      ->condition('type', 'openac_location')
      ->condition('status', 'inactive')
      ->execute()->fetchCol();

    foreach ($not_in_use_ids as $id) {
      unset($location_select['#options'][$id]);
    }

    // Set up out condition information.
    $elements = array();
    $course_ops = $form[$this->element_namespace]['application_course'][$language]['#options'];
    foreach (entity_load('course', array_keys($course_ops)) as $course) {
      $course_wrapper = entity_metadata_wrapper('course', $course);
      foreach ($course_wrapper->course_locations as $location_wrapper) {
        $location_id = $location_wrapper->getIdentifier();
        $elements[] = array(
          'controls' => ":input[type=radio][name=\"{$location_element_name}\"][value={$location_id}]",
          'target' => ":input[name=\"{$course_element_name}\"][value={$course->id}]",
          'comparison' => $location_id,
          'actions' => array(FALSE, 'disable', 'enable'),
        );
      }
    }

    /*    $form[$this->element_namespace]['#attached']['js'][drupal_get_path('module', 'openac_location') . '/js/course_location_joint.js'] = array('type' => 'file');
    $form[$this->element_namespace]['#attached']['js'][] = array(
      'type' => 'setting',
      'data' => array(
        'openac_location_joint' => array(
          'conditional_elements' => $elements,
        ),
      ),
    );*/

    return parent::form($form, $form_state, $application, $language);
  }

  /**
   * Validate the form element.
   */
  public function formValidate($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
  }

  /**
   * Submit the form element.
   */
  public function formSubmit($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    $values = $this->formExtractValues($form, $form_state, $entity);
    $entity->application_course = $values['application_course'];
    $entity->application_location = $values['application_location'];
  }

  /**
   * Extract the submitted values for this form element.
   */
  public function formExtractValues($form, &$form_state, $entity) {
    $parents = $form['#parents'];
    $parents[] = $this->getEntityNamespace();
    $parents[] = 'course_location_joint';

    $values = drupal_array_get_nested_value($form_state['values'], $parents);
    return $values;
  }

}
