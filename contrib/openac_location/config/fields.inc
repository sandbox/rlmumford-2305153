<?php
/**
 * @files
 * Contains field definitions for the assigments system.
 */

$t = get_t();
$fields = array();

// Addres Field
$field_name = 'location_address';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'cardinality' => 1,
    'type' => 'addressfield',
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'opencrm_location',
    'bundle' => 'openac_location',
    'label' => $t('Address'),
    'description' => $t("The address of this location."),
    'widget' => array(
      'type' => 'addressfield_standard',
      'settings' => array(
        'format_handlers' => array(
          'address' => 'address',
          'address-hide-country' => 'address-hide-country',
        ),
        'available_countries' => array(
          'GB' => 'GB',
          'US' => 'US',
        ),
      ),
    ),
  ),
);

$field_name = 'location_tutor';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'party',
      'handler' => 'base',
      'handler_settings' => array(
        'target_hats' => array('openac_faculty' => 'openac_faculty'),
        'behaviors' => array(
          'views-autocomplete' => array(
            'status' => 1,
          ),
        ),
      ),
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'opencrm_location',
    'bundle' => 'openac_location',
    'label' => $t('Location Tutor'),
    'description' => $t('The tutor responsible for the location'),
    'widget' => array(
      'type' => 'options_buttons',
    ),
  ),
);

// Locations on a Course.
$field_name = 'course_locations';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'opencrm_location',
      'handler' => 'base',
      'handler_settings' => array(
        'target_bundles' => array('openac_location' => 'openac_location'),
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
      ),
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'course',
    'bundle' => 'course',
    'label' => $t('Locations'),
    'description' => $t('The Locations this @course is available at.', array('@course' => openac_entity_type_label('course'))),
    'widget' => array(
      'type' => 'options_buttons',
    ),
  ),
);

// Course Registration Location
$field_name = 'course_reg_location';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'opencrm_location',
      'handler' => 'base',
      'handler_settings' => array(
        'target_bundles' => array('openac_location' => 'openac_location'),
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 1,
          ),
        ),
      ),
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'course_registration',
    'bundle' => 'course_registration',
    'label' => $t('Location'),
    'description' => $t('The location the @lcourse is being studied at.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'widget' => array(
      'type' => 'options_buttons',
    ),
  ),
);
