<?php
/**
 * @file
 * Flexiform hooks.
 */

/**
 * Implements hook_flexiform_element_info().
 */
function openac_location_flexiform_element_info() {
  $elements = array();
  if (!module_exists('openac_application')) {
    return;
  }
  $application_info = entity_get_info('application');
  foreach ($application_info['bundles'] as $bundle => $info) {
    $elements['application'][$bundle]['application:course_location_joint'] = array(
      'label' => t('Unified @course/Location Selector', array('@course' => openac_entity_type_label('course'))),
      'class' => 'OpenAcLocationFlexiformElementCourseLocationJoint',
      'type' => 'course_location',
      'group' => 'Application',
    );
  }

  return $elements;
}
