<?php

/**
 * Implements hook_entity_property_info_alter().
 */
function openac_application_entity_property_info_alter(&$info) {
  // Application Properties
  $info['application']['properties']['title'] = array(
    'label' => t('Title'),
    'type' => 'text',
    'description' => 'The generated title for the application.',
    'getter callback' => 'application_get_properties',
  );

  unset($info['application']['properties']['party']);
  $info['application']['properties']['applicant'] = array(
    'label' => t('Applicant'),
    'type' => 'party',
    'getter callback' => 'entity_property_getter_method',
    'setter callback' => 'entity_property_setter_method',
    'description' => 'The Party of the Applicant',
    'schema field' => 'party',
    'required' => TRUE,
  );

  $info['application']['properties']['type'] = array(
    'label' => t('Application Type'),
    'type' => 'application_type',
    'description' => 'The Application Type entity',
    'schema field' => 'type',
    'required' => TRUE,
  );

  $info['application']['properties']['status']['getter callback'] = 'application_get_status_label';
  $info['application']['properties']['status']['setter callback'] = 'entity_property_verbatim_set';
  $info['application']['properties']['status']['options list'] = 'application_get_all_statuses';

  $info['party']['properties']['applications'] = array(
    'label' => t('Applications'),
    'description' => t('The applications for this party.'),
    'type' => 'application',
    'computed' => TRUE,
    'getter callback' => 'openac_application_party_application_property',
  );
}
