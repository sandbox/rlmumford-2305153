<?php
/**
 * @file
 * Providing extra functionality for the Application UI via views.
 */

/**
 * Implements hook_views_data_alter()
 */
function openac_application_views_data_alter(&$data) {
  // Set up our application entity
  $data['application']['link_application'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the application.'),
      'handler' => 'application_handler_link_field',
    ),
  );
  $data['application']['edit_application'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the application.'),
      'handler' => 'application_handler_edit_link_field',
    ),
  );
  $data['application']['delete_application'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the application.'),
      'handler' => 'application_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows openac_applications/openac_application/%openac_application_id/op
  $data['application']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this openac_application.'),
      'handler' => 'application_handler_application_operations_field',
    ),
  );
  // Field handler for statuses
  $data['application']['status'] = array(
    'field' => array(
      'title' => t('Status'),
      'help' => t('Display the processing status of this application'),
      'handler' => 'application_handler_field_status',
    ),
    'filter' => array(
      'title' => t('Status'),
      'description' => t('Filter by the Status of the application'),
      'handler' => 'application_handler_filter_status',
    ),
    'sort' => $data['application']['status']['sort'],
    'argument' => $data['application']['status']['argument'],
  );
}

/**
 * Implements hook_views_pre_build()
 */
function openac_application_views_pre_build(&$view) {
  // Make sure we're only fiddling with the desired view
  if ($view->name == 'manage_applications_dashboard_') {
    // Find all our exposed filters
    $exposed = array();
    foreach ($view->filter as $key => $filter) {
      if (!empty($view->filter[$key]->options['exposed'])) {
        $exposed[$key] = $view->filter[$key]->options['expose']['identifier'];
      }
    }

    // Iterate over them checking $_REQUEST and saving their state to session
    foreach ($exposed as $key => $identifier) {
      if (!isset($_REQUEST[$identifier])) {
        // Let's load from the session
        if (isset($_SESSION['openac_application']['view:' . $view->name]['filters'][$key])) {
          $view->filter[$key]->value = $_SESSION['openac_application']['view:' . $view->name]['filters'][$key];
        }
      }
      else {
        // Let's save our value to the session
        $_SESSION['openac_application']['view:' . $view->name]['filters'][$key] =
          $view->filter[$key]->value = $_REQUEST[$identifier];
      }
    }
  }
}
