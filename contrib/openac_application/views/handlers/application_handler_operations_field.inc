<?php

/**
 * This field handler aggregates operations that can be done on a openac_application
 * under a single field providing a more flexible way to present them in a view
 */
class application_handler_operations_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['application_id'] = 'application_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {

    $links = menu_contextual_links('openac_application', 'admin/college/applications/application', array($this->get_value($values, 'application_id')));
    
    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
