<?php

/**
 * @file
 * Contains a Views field handler to take care of application statuses
 * as fields.
 */

class application_handler_field_status extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }

  function get_value($values, $field = NULL) {
    $type = $values->{$this->aliases['type']};
    $raw = parent::get_value($values, $field);

    $status = application_get_statuses($type, $raw);
    return (!empty($status)) ? $status['label'] : $raw;
  }
}