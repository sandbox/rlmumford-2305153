<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class application_handler_delete_link_field extends application_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};
    
    //Creating a dummy application to check access against
    $dummy_application = (object) array('type' => $type);
    if (!application_access('edit', $dummy_application)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $application_id = $values->{$this->aliases['application_id']};
    
    return l($text, 'admin/content/applications/application/' . $application_id . '/delete');
  }
}
