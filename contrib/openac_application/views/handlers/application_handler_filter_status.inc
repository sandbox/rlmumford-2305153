<?php
/**
 * @file
 * Contains a Views filter handler to take care of application statuses
 * as fields.
 */

class application_handler_filter_status extends views_handler_filter_equality {

  /**
   * Provide the form
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $form['value'] = array();
    $form['value']['#tree'] = TRUE;

    $options = application_get_all_statuses();

    $form['value'] = array(
      '#type' => 'select',
      '#title' => empty($form_state['exposed']) ? t('Value') : '',
      '#options' => $options,
      '#default_value' => $this->value,
    );
  }
}