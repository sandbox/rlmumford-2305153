<?php

/**
 * @file
 * Contains a Views field handler to take care of application statuses
 * as fields.
 */

class openac_application_handler_field_status extends views_handler_field {
  function get_value($values, $field = NULL) {
    $statuses = application_get_statuses();
    
    $status = parent::get_value($values, $field);
    
    return (isset($statuses[$status])) ? $statuses[$status]['label'] : $status;
  }
}