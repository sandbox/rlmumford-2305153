<?php
/**
 * @file
 * Contains OpenAcApplicationCourseSelectionHandler
 */

/**
 * Selects courses that can be applied for using an application.
 */
class OpenAcApplicationCourseSelectionHandler extends EntityReference_SelectionHandler_Generic {
  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    if ($field['settings']['target_type'] != 'course') {
      return parent::getInstance($field, $instance, $entity_type, $entity);
    }
    return new OpenAcApplicationCourseSelectionHandler($field, $instance, $entity_type, $entity);
  }

  /**
   * Build an EntityFieldQuery to get referenceable entities.
   */
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityFieldQuery($match, $match_operator);

    if (!empty($instance['bundle']) && $instance['entity_type'] == 'application') {
      $application_type = entity_load_single('application_type', $instance['bundle']);
      if (!empty($application_type->data['courses'])) {
        $query->propertyCondition('id', $application_type->data['courses']);
      }
    }

    return $query;
  }
}
