<?php
$plugin = array(
  'title' => t('Application @course Selection Handler', array('@course' => openac_entity_type_label('course'))),
  'class' => 'OpenAcApplicationCourseSelectionHandler',
);
