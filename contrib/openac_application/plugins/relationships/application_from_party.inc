<?php

/**
 * @file
 * Plugin to provide an relationship handler for term from node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Application from Party'),
  'keyword' => 'application',
  'description' => t('Adds an Application entity from the Party context'),
  'required context' => new ctools_context_required(t('Party'), 'party'),
  'context' => 'openac_application_application_from_party_context',
  'edit form' => 'openac_application_application_from_party_settings_form',
  'defaults' => array('bundle' => 'student'),
);

/**
 * Return a new context based on an existing context.
 */
function openac_application_application_from_party_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:application', NULL);
  }

  $party = $context->data;

  $query = new EntityFieldQuery();
  $ents = $query->entityCondition('entity_type', 'application')
    ->entityCondition('bundle', $conf['bundle'])
    ->propertyCondition('party', $party->pid)
    ->execute();

  if (empty($ents['application'])) {
    return ctools_context_create_empty('entity:application', NULL);
  }

  $application_id = reset($ents['application'])->application_id;
  $application = application_load($application_id);

  if ($application) {
    return ctools_context_create('entity:application', $application);
  }
}

/**
 * Settings form for the relationship.
 */
function openac_application_application_from_party_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $options = array();
  foreach (application_get_types() as $type => $info) {
    $options[$type] = $info->label;
  }
  $form['bundle'] = array(
    '#title' => t('Application Type'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['bundle'],
    '#prefix' => '<div class="clearfix">',
    '#suffix' => '</div>',
  );

  return $form;
}
