<?php
/**
 * @file Plugin to display the status form
 */
$plugin = array(
  'title' => t('Application Workflow'),
  'content type' => 'openac_application_application_workflow_content_type',
  'content types' => 'openac_application_application_workflow_content_types',
);

function openac_application_application_workflow_content_types() {
  $types = drupal_static(__FILE__, array());

  $types = array(
    'application_status_form_context' => array(
      'title' => t('Application Workflow '),
      'required context' => new ctools_context_required(t('Application'), 'entity:application'),
    ),
  );

  foreach ($types as $key => $array) {
    $types[$key] += array(
      'category' => t('Workflow'),
      'description' => 'Application Workflow',
    );
  }

  return $types;
}

function openac_application_application_workflow_content_type($subtype) {
  $types = openac_application_application_workflow_content_types();

  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

function openac_application_application_workflow_content_type_render($subtype, $conf, $panel_args, $context) {
  // Get the Application
  $application = isset($context->data) ? $context->data : NULL;

  // If the application is not set properly return false
  if (!isset($application->application_id)) {
    return false;
  }

  // Get the status array
  $status = application_get_statuses($application->type, $application->status);

  // Do whatever the status said.
  $content = '';
  $block = FALSE;
  switch ($status['type']) {
    case 'mini_panel':
      $mini = panels_mini_load($status['settings']['mini_panel_id']);
      if (!$mini) {
        $content = t('Mini Panel not Found');
      }
      // Load up any contexts we might be using.
      $contexts['application'] = $context;
      $context = ctools_context_match_required_contexts($mini->requiredcontexts, $contexts);
      $mini->context = $mini->display->context = ctools_context_load_contexts($mini, FALSE, $context);

      if (empty($mini) || !empty($mini->disabled)) {
        return;
      }

      $mini->display->args = $panel_args;
      $mini->display->css_id = panels_mini_get_id($status['settings']['mini_panel_id']);
      $mini->display->owner = $mini;
      // unique ID of this mini.
      $mini->display->owner->id = $mini->name;

      $block = new stdClass();
      $block->module  = 'panels_mini';
      $block->delta   = $status['mini_panel_id'];
      $block->content = panels_render_display($mini->display);
      $block->title = $mini->display->get_title();

      if (user_access('administer mini panels')) {
        $block->admin_links = array(
          array(
            'title' => t('Configure mini panel'),
            'href' => "admin/structure/mini-panels/list/{$status['settings']['mini_panel_id']}/edit/content",
            'query' => drupal_get_destination(),
          ),
        );
      }
      break;
    case 'flexiform':
      $flexiform = entity_load_single('flexiform', $status['settings']['flexiform']);
      $content = flexiform_wrapper($flexiform, $application);
      break;
    case 'callback':
      $content = drupal_get_form($status['settings']['callback'], $application);
      break;
  }

  if (!$block) {
    $block = new stdClass();
    $block->subject = t('Application Status');
    $block->content = $content;
  }

  return $block;
}

function openac_application_application_workflow_content_type_edit_form($form, &$form_state) {
  return $form;
}

function openac_application_application_workflow_content_type_admin_title($subtype, $conf, $context) {
  $types = openac_application_application_workflow_content_types();

  return $types[$subtype]['title'];
}