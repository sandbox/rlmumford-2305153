<?php
/**
 * @file
 *  Mysql View Definition For the Applicant Dashboard.
 */

/**
 * Implements hook_openac_reports_mysql_views_info().
 */
function openac_application_openac_reports_mysql_views_info() {
  $views = array();
  $views['application_status'] = array(
    'label' => t('Application Status'),
    'description' => t('Cache table storing statuses of applications.'),
    'primary_field' => 'application',
    'fields' => array(
      'application' => array(
        'type' => 'int',
        'description' => t('The application ID'),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'party' => array(
        'type' => 'int',
        'description' => t('The applicant ID'),
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'type' => array(
        'type' => 'varchar',
        'description' => t('The application type'),
        'not null' => TRUE,
      ),
    ),
    'foreign keys' => array(
      'application' => array(
        'table' => 'application',
        'columns' => array('application' => 'application_id'),
      ),
      'party' => array(
        'table' => 'party',
        'columns' => array('party' => 'pid'),
      ),
    ),
  );

  // Build Query
  $query = db_select('application', 'a');

  $query->addField('a', 'application_id', 'application');
  $query->addField('a', 'party', 'party');
  $query->addField('a', 'type', 'type');

  $useNull = TRUE;
  $info = entity_get_info('application');
  foreach ($info['bundles'] as $type => $binfo) {
    foreach (application_get_statuses($type) as $name => $status) {
      $query->addExpression('IF(a.status = :status__' . $name . ', a.application_id, null)',
        'status__' . $name,
        array(':status__' . $name => $name));

      $views['application_status']['fields']['status__' . $name] = array(
        'type' => 'boolean',
        'title' => $status['label'],
        'description' => $status['description'],
        'not null' => TRUE,
        'unsigned' => TRUE,
      );
    }
  }

  $views['application_status']['query'] = $query;

  return $views;
}
