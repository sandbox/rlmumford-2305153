; Open Academy Application  make file for use with an existing OpenCRM site.
; Builds any requirements that are additional to OpenCRM.

api = 2
core = 7.x

; Utilities

projects[message][subdir] = contrib
projects[message][version] = 1.12
