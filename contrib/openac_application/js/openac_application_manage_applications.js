(function ($) {
  Drupal.behaviors.openAcApplicationManageApplication = {
    attach: function (context, settings) {
      $('.openac-application-manage-link', context).once('openac-application-manage-link', function() {
        var element_settings = {};
        if ($(this).attr('href')) {
          element_settings.url = $(this).attr('href');
          element_settings.event = 'click';
          element_settings.progress = { type: 'throbber' };
        }
        var base = $(this).attr('href');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      });
    }
  };
})(jQuery);
