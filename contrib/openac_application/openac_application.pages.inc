<?php

/**
 * Page Callback to load a minipanel (preferably using JS).
 */
function openac_application_manage_application($application, $js) {
  $panel_id = 'openac_application__manage_app';
  $mini = panels_mini_load($panel_id);
  if (!$mini) {
    $content = t('Mini Panel not Found');
  }

  // Load up any contexts we might be using.
  ctools_include('context');
  $context = ctools_context_create('entity:application', $application);
  $contexts['application'] = $context;
  $context = ctools_context_match_required_contexts($mini->requiredcontexts, $contexts);
  $mini->context = $mini->display->context = ctools_context_load_contexts($mini, FALSE, $context);

  if (empty($mini) || !empty($mini->disabled)) {
    return;
  }

  $mini->display->args = $panel_args;
  $mini->display->css_id = panels_mini_get_id($panel_id);
  $mini->display->owner = $mini;
  // unique ID of this mini.
  $mini->display->owner->id = $mini->name;

  $block = new stdClass();
  $block->module  = 'panels_mini';
  $block->delta   = $panel_id;
  $block->content = array('#markup' => panels_render_display($mini->display));
  $block->title = $mini->display->get_title();

  if (user_access('administer mini panels')) {
    $block->admin_links = array(
      array(
        'title' => t('Configure mini panel'),
        'href' => "admin/structure/mini-panels/list/{$panel_id}/edit/content",
        'query' => drupal_get_destination(),
      ),
    );
  }

  $content = array(
    '#theme_wrappers' => array('block'),
    '#block' => $block,
  ) + $block->content;

  if (!$js) {
    return $content;
  }
  else {
    $content['#prefix'] = '<div class="manage-application-area">';
    $content['#suffix'] = '</div>';
    $commands = array();
    $commands[] = ajax_command_replace('.manage-application-area', drupal_render($content));
    ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
    drupal_exit();
  }
}
