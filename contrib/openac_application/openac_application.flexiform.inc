<?php

/**
 * Implements hook_flexiform_display_info().
 */
function openac_application_flexiform_display_info() {
  $displays['OpenAcApplicationFlexiformPage'] = array(
    'class' => 'OpenAcApplicationFlexiformPage',
    'label' => t('Apply Page'),
    'description' => t('A page that will get the application for the current user.'),
  );

  return $displays;
}

/**
 * Implements hook_flexiform_entity_getter_info().
 */
function openac_application_flexiform_entity_getter_info() {
  $getters = array();
  $getters['openac_application_student'] = array(
    'label' => 'Student from Application',
    'description' => 'Load a Party from an Application',
    'params' => array(
      'application' => array(
        'entity_type' => 'application',
      ),
    ),
    'entity_types' => array('party'),
    'class' => 'OpenAcApplicationFlexiformFormEntityStudent',
  );

  return $getters;
}

/**
 * Implements hook_flexiform_element_info().
 */
function openac_application_flexiform_element_info() {
  $elements = array();
  $info = entity_get_info('application');
  foreach ($info['bundles'] as $bundle => $info) {
    $elements['application'][$bundle]['application:course_notes'] = array(
      'label' => t('Course Notes'),
      'class' => 'OpenAcApplicationFlexiformElementCourseNotes',
      'type' => 'application',
      'group' => t('Application'),
    );
  }

  return $elements;
}

/**
 * Implements hook_flexiform_group_info().
 */
function openac_application_flexiform_group_info() {
  $groups = array();
  $groups['openac_application'] = array(
    'label' => t('OpenCRM Academy Application Forms'),
  );
  return $groups;
}
