<?php
/**
 * @file application.entity.inc
 *   Classes and Controllers for the Application Entity
 *
 * @todo: Static Cache Statuses?
 */

/**
 * The class used for Application entities
 */
class Application extends OpenAcEntity {

  /**
   * The Applicant entity.
   */
  protected $applicant_entity;

  public function __construct($values = array()) {
    parent::__construct($values, 'application');
  }

  public function defaultLabel() {
    // Get bundle's nice name
    $bundle = entity_load_single('application_type', $this->type);
    // Get the party
    if ($this->getApplicant()) {
      return format_string('@party (@bundle)', array(
        '@party' => $this->getApplicant()->label(),
        '@bundle' => $bundle->label,
      ));
    }

    return $bundle->label;
  }

  protected function defaultUri() {
    return array('path' => 'application/' . $this->application_id);
  }

  /**
   * Get the applicant entity.
   *
   * @return Party
   */
  public function getApplicant() {
    if (!isset($this->applicant_entity)) {
      $this->applicant_entity = party_load($this->party);
    }

    return $this->applicant_entity;
  }

  public function applicant() {
    return $this->getApplicant();
  }

  /**
   * Set the applicant.
   */
  public function setApplicant(Party $party) {
    $this->applicant_entity = $party;
    unset($this->party);

    if (!empty($this->applicant_entity->pid)) {
      $this->party = $this->applicant_entity->pid;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $this->changed = REQUEST_TIME;
    if (!empty($this->applicant_entity) && empty($this->party)) {
      if (empty($this->applicant_entity->pid)) {
        $this->applicant_entity->save();
      }

      $this->party = $this->applicant_entity->pid;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    if ($update == 'insert') {
      $values = array(
        'timestamp' => REQUEST_TIME,
      );
      $message = message_create('application_started', $values);
      $wrapper = entity_metadata_wrapper('message', $message);
      $wrapper->application = $this;
      $wrapper->party = $this->party;
      $wrapper->save();
    }
    else if ($this->status != $this->original->status) {
      $application_type = entity_load_single('application_type', $this->type);

      $old_info = $application_type->data['statuses'][$this->original->status];
      $new_info = $application_type->data['statuses'][$this->status];

      $values = array(
        'arguments' => array(
          '@old_status' => $old_info['label'],
          '@new_status' => $new_info['label'],
        ),
        'timestamp' => REQUEST_TIME,
      );
      $message = message_create('application_status_change', $values);
      $wrapper = entity_metadata_wrapper('message', $message);
      $wrapper->application = $this;
      $wrapper->party = $this->party;
      $wrapper->application_log_old_status = $this->original->status;
      $wrapper->application_log_new_status = $this->status;
      if (!empty($this->status_change_comment)) {
        $wrapper->application_log_comment = $this->status_change_comment;
      }
      $wrapper->save();
    }
  }
}

/**
 * The Controller for Application entities
 */
class ApplicationController extends OpenAcEntityController {

  /**
   * Create a openac_application - we first set up the values that are specific
   * to our openac_application schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the openac_application.
   *
   * @return
   *   A openac_application object with all default fields initialized.
   */
  public function create(array $values = array()) {
    if (empty($values['type'])) {
      throw new Exception(t('You must specify an application type'));
    }

    // Add values that are specific to our Application
    $values += array(
      'application_id' => '',
      'is_new' => TRUE,
      'title' => '',
      'created' => time(),
      'changed' => time(),
      'data' => '',
    );

    // Work out the status.
    $application_type = entity_load_single('application_type', $values['type']);
    if (empty($values['status'])) {
      $values['status'] = $application_type->data['default_status'];
    }

    if (empty($values['application_course'])
        && !empty($application_type->data['courses'])
        && count($application_type->data['courses']) == 1) {
      $values['application_course'][LANGUAGE_NONE][0]['target_id'] = reset($application_type->data['courses']);
    }

    return parent::create($values);
  }
}
