<?php
/**
 * @file
 * Define Display plugin for the application page.
 */

/**
 * Class for application page displays.
 */
class OpenAcApplicationFlexiformPage extends FlexiformDisplayPageBase {

  /**
   * Define the menu links
   */
  public function hook_menu() {
    $items = array();

    $items['apply/'.$this->configuration['path_element']] = array(
      'title callback' => 'flexiform_page_title',
      'title arguments' => $this->getMenuArguments(),
      'page callback' => 'flexiform_page_wrapper',
      'page arguments' => $this->getMenuArguments(),
      'access callback' => 'openac_application_application_form_access',
      'access arguments' => array($this->flexiform->form),
      'file' => 'flexiform.pages.inc',
    );

    return $items;
  }

  /**
   * Get the configuration form.
   */
  public function configForm($form, &$form_state) {
    $form['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => 'Enabled',
      '#default_value' => !empty($this->configuration['enabled']),
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => !empty($this->configuration['title']) ? $this->configuration['title'] : '',
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#description' => t('The description to appear underneath the link to this application.'),
      '#rows' => 2,
      '#default_value' => !empty($this->configuration['description']) ? $this->configuration['description'] : '',
    );

    $form['path_element'] = array(
      '#type' => 'textfield',
      '#title' => t('Path Element'),
      '#description' => t('The sub-path of /apply where you want this application form to appear.'),
      '#default_value' => !empty($this->configuration['path_element']) ? $this->configuration['path_element'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseEntity($context = array()) {
    global $user;
    $student = party_user_get_party($user);

    $id = db_select('application', 'a')
      ->fields('a', array('application_id'))
      ->condition('type', $this->flexiform->base_entity_bundle)
      ->condition('party', $student->pid)
      ->execute()
      ->fetchField();

    if (empty($id)) {
      $values = array(
        'party' => $student->pid,
        'type' => $this->flexiform->base_entity_bundle,
      );
      $application = entity_create('application', $values);
      $application->save();

      // Assign the applicant hat.
      party_hat_hats_assign($student, array('applicant'));
    }
    else {
      $application = entity_load_single('application', $id);
    }

    return $application;
  }

}
