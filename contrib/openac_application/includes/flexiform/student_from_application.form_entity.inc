<?php
/**
 * @file
 *  Contains class for a basic entity getter.
 */

/**
 * Form entity to get the party from a application.
 */
class OpenAcApplicationFlexiformFormEntityStudent extends FlexiformFormEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    $settings = $this->settings;

    // Get the party entity.
    $application = $this->getParam('application');
    if ($application) {
      return party_load($application->party);
    }

    return FALSE;
  }
}
