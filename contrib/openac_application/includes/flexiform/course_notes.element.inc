<?php

/**
 * @file
 * Contains class for the Course Notes form element.
 */
class OpenAcApplicationFlexiformElementCourseNotes extends FlexiformRepeatableElement {

  /**
   * Returns the custom html that is defined in the configure form.
   */
  public function form($form, &$form_state, $entity) {
    $parents = $form['#parents'];
    $parents[] = 'course_notes';

    $wrapper = entity_metadata_wrapper('application', $entity);
    $course = $wrapper->application_course->value();
    $display = array(
      'label' => 'hidden',
    );

    if (!$course) {
      return $form;
    }

    $form[$this->element_namespace] = array(
      '#type' => 'item',
      '#parents' => $parents,
      'notes' => field_view_field('course', $course, 'course_application_notes', $display),
    );

    $form = parent::form($form, $form_state, $entity);

    return $form;
  }
}
