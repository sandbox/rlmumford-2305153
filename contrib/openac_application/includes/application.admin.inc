<?php

/**
 * @file
 * Application editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class ApplicationUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Applications',
      'description' => 'Add edit and update applications.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'position' => 'left',
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
      'type' => MENU_NORMAL_ITEM,
    );

    $items['apply'] = array(
      'title' => 'Apply',
      'description' => 'Apply for a ' . openac_entity_type_label('course'),
      'page callback'  => 'openac_application_apply_page',
      'access callback'  => TRUE,
      'weight' => 20,
      'file' => 'includes/application.admin.inc',
      'file path' => drupal_get_path('module', 'openac_application'),
    );

    return $items;
  }
}

/**
 * Apply Page listing live applications.
 */
function openac_application_apply_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  // Bypass the node/add listing if only one application is available.
  if (empty($content)) {
    return array(
      '#markup' => t('Sorry, we are not taking new applications at the moment.'),
    );
  }
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }
  return theme('node_add_list', array('content' => $content));
}