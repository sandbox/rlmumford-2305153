<?php

/**
 * @file
 * Application type editing UI.
 */

/**
 * UI controller.
 */
class ApplicationTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $id_count = count(explode('/', $this->path));
		$items[$this->path]['description'] = 'Manage application entity types, including adding
		and removing fields and the display of fields.';
    $items[$this->path . '/manage/%application_type/clone']['access callback'] = 'entity_access';
    $items[$this->path . '/manage/%application_type/clone']['access arguments'] = array('update', 'application_type', $id_count + 1);
    $items[$this->path . '/manage/%application_type/clone']['page callback'] = 'application_type_ui_get_clone_form';
    $items[$this->path . '/manage/%application_type/clone']['page arguments'] = array($id_count + 1);
    $items[$this->path . '/manage/%application_type/clone']['file'] = 'includes/application_type.admin.inc';
    $items[$this->path . '/manage/%application_type/clone']['file path'] = drupal_get_path('module', 'openac_application');

    return $items;
  }
}

/**
 * Get the clone form for the Application Type UI.
 */
function application_type_ui_get_clone_form($application_type) {
  $form_id = 'application_type_form';

  // Do not use drupal_get_form(), but invoke drupal_build_form() ourself so
  // we can prepulate the form state.
  $form_state['wrapper_callback'] = 'entity_ui_main_form_defaults';
  $form_state['entity_type'] = 'application_type';
  form_load_include($form_state, 'inc', 'entity', 'includes/entity.ui');

  $application_type = clone $application_type;
  $application_type->id = FALSE;
  $application_type->cloned_from = $application_type->type;
  $application_type->type = FALSE;
  $application_type->is_new = TRUE;
  $application_type->status = ENTITY_CUSTOM;

  // We don't pass the entity type as first parameter, as the implementing
  // module knows the type anyway. However, in order to allow for efficient
  // hook_forms() implementiations we append the entity type as last argument,
  // which the module implementing the form constructor may safely ignore.
  // @see entity_forms()
  $form_state['build_info']['args'] = array($application_type, 'clone', $entity_type);
  return drupal_build_form($form_id, $form_state);
}

/**
 * Generates the application type editing form.
 */
function application_type_form($form, &$form_state, $application_type, $op = 'edit') {
  if ($op == 'clone') {
    $application_type->label .= ' (cloned)';
    $application_type->type = '';
  }

  // Set the application type object everywhere important.
  $form_state['build_info']['args'][0] =
    $form_state['application_type'] =
    $form['#application_type'] = $application_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $application_type->label,
    '#description' => t('The human-readable name of this application type.'),
    '#required' => TRUE,
    '#size' => 30,
    '#weight' => -20,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($application_type->type) ? $application_type->type : '',
    '#maxlength' => 32,
    '#disabled' => !empty($application_type->id) && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'application_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this application type. It must only contain lowercase letters, numbers, and underscores.'),
    '#weight' => -19,
  );

  $form['open_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Open Date'),
    '#date_format' => "d/m/Y",
    '#default_value' => isset($application_type->open_date) ? date("Y-m-d", $application_type->open_date) : '',
    '#required' => TRUE,
    '#weight' => -4,
  );

  $form['close_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Close Date'),
    '#date_format' => "d/m/Y",
    '#default_value' => isset($application_type->close_date) ? date("Y-m-d", $application_type->close_date) : '',
    '#required' => TRUE,
    '#weight' => -3,
  );

  $form['data']['#tree'] = TRUE;

  $courses = !empty($application_type->data['courses']) ? $application_type->data['courses'] : array();
  $form['data']['courses'] = array(
    '#type' => 'checkboxes',
    '#title' => openac_entity_type_label('course', TRUE),
    '#description' => t('@courses that can be applied for with this application type.', array('@courses' => openac_entity_type_label('course', TRUE))),
    '#options' => openac_course_options_list(TRUE, $courses),
    '#multiple' => TRUE,
    '#default_value' => $courses,
  );

  $form['data']['statuses'] = array(
    '#type' => 'fieldset',
    '#title' => t('Statuses'),
    '#description' => t('Use this session to configure the steps the process must go through'),
  );

  $form['data']['statuses']['table'] = array(
    '#theme' => 'table',
    '#parents' => array('data', 'statuses'),
    '#tree' => TRUE,
    '#header' => array(
      t('Machine Name'),
      t('Label'),
      t('Default'),
      t('Type'),
      t('Settings'),
      t('Operations'),
    ),
    '#rows' => array(),
  );

  $flexiform_options = db_select('flexiform', 'f')
    ->condition('base_entity', 'application')
    ->condition('base_entity_bundle', $application_type->type)
    ->fields('f', array('form', 'label'))
    ->execute()
    ->fetchAllKeyed();

  $statuses = array();
  if (!empty($application_type->data['statuses'])) {
    $statuses = &$application_type->data['statuses'];
  }
  foreach ($statuses as $name => $info) {
    $row = array();
    $row['name'] = array(
      '#markup' => $name,
    );
    $row['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#title_display' => 'invisible',
      '#required' => TRUE,
      '#default_value' => $info['label'],
      '#size' => 30,
    );
    $row['default'] = array(
      '#type' => 'radio',
      '#title' => t('Is the Default Status'),
      '#title_display' => 'invisible',
      '#parents' => array('data', 'default_status'),
      '#attributes' => array(
        'value' => $name,
      ),
    );
    if (!empty($application_type->data['default_status'])
      && $application_type->data['default_status'] == $name) {
      $row['default']['#attributes']['checked'] = 'checked';
    }
    $row['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#title_display' => 'invisible',
      '#options' => array(
        'mini_panel' => t('Render a Mini Panel'),
        'flexiform' => t('Flexiform'),
        'callback' => t('Form Callback'),
      ),
      '#default_value' => $info['type'],
    );
    $row['settings'] = array(
      '#type' => 'container',
    );
    $row['settings']['mini_panel_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Mini Panel Name'),
      '#description' => t('Enter the minipanel machine name.'),
      '#default_value' => !empty($info['settings']['mini_panel_id']) ? $info['settings']['mini_panel_id'] : '',
      '#states' => array(
        'visible' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'mini_panel'),
        ),
        'required' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'mini_panel'),
        ),
      ),
    );
    $row['settings']['flexiform'] = array(
      '#type' => 'select',
      '#title' => t('Flexiform'),
      '#options' => $flexiform_options,
      '#default_value' => !empty($info['settings']['flexiform']) ? $info['settings']['flexiform'] : '',
      '#states' => array(
        'visible' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'flexiform'),
        ),
        'required' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'flexiform'),
        ),
      ),
    );
    $row['settings']['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback'),
      '#description' => t('Please enter the form id. This will get passed to drupal_get_form.'),
      '#default_value' => !empty($info['settings']['callback']) ? $info['settings']['callback'] : '',
      '#states' => array(
        'visible' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'callback'),
        ),
        'required' => array(
          ':input[name="data[statuses]['.$name.'][type]"]' => array('value' => 'callback'),
        ),
      ),
    );
    $row['operations'] = array(
      '#type' => 'submit',
      '#value' => t('Remove @status', array('@status' => $info['label'])),
      '#status_name' => $name,
      '#limit_validation_errors' => array(),
      '#submit' => array('application_type_form_submit_remove_status'),
    );

    $form['data']['statuses']['table'][$name] = array(
      'name' => &$row['name'],
      'label' => &$row['label'],
      'default' => &$row['default'],
      'type' => &$row['type'],
      'settings' => &$row['settings'],
      '#status' => $info,
      '#status_name' => $name,
      'operations' => &$row['operations'],
    );
    $form['data']['statuses']['table']['#rows'][$name] = array(
      'name' => array('data' => &$row['name']),
      'label' => array('data' => &$row['label']),
      'default' => array('data' => &$row['default']),
      'type' => array('data' => &$row['type']),
      'settings' => array('data' => &$row['settings']),
      'operations' => array('data' => &$row['operations']),
    );

    unset($row);
  }

  // Add new Row.
  $row = array();
  $row['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#title_display' => 'invisible',
    '#size' => 30,
  );
  $row['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#title_display' => 'invisible',
    '#size' => 30,
  );
  $row['default'] = array(
    '#type' => 'radio',
    '#title' => t('Is the Default Status'),
    '#title_display' => 'invisible',
    '#parents' => array('data', 'default_status'),
    '#attributes' => array(
      'value' => 'add_new',
    ),
  );
  $row['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#title_display' => 'invisible',
    '#options' => array(
      'mini_panel' => t('Render a Mini Panel'),
      'flexiform' => t('Flexiform'),
    ),
  );
  $row['settings'] = array(
    '#type' => 'container',
  );
  $row['settings']['mini_panel_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Mini Panel Name'),
    '#description' => t('Enter the minipanel machine name.'),
    '#states' => array(
      'visible' => array(
        ':input[name="data[statuses][add_new][type]"]' => array('value' => 'mini_panel'),
      ),
      'required' => array(
        ':input[name="data[statuses][add_new][type]"]' => array('value' => 'mini_panel'),
      ),
    ),
  );
  $row['settings']['flexiform'] = array(
    '#type' => 'select',
    '#title' => t('Flexiform'),
    '#options' => $flexiform_options,
    '#states' => array(
      'visible' => array(
        ':input[name="data[statuses][add_new][type]"]' => array('value' => 'flexiform'),
      ),
      'required' => array(
        ':input[name="data[statuses][add_new][type]"]' => array('value' => 'flexiform'),
      ),
    ),
  );
  $row['operations'] = array(
    '#type' => 'submit',
    '#value' => t('Add Status'),
    '#limit_validation_errors' => array(array('data', 'statuses', 'add_new'), array('data', 'default_status')),
    '#submit' => array('application_type_form_submit_add_status'),
  );

  $form['data']['statuses']['table']['add_new'] = array(
    'name' => &$row['name'],
    'label' => &$row['label'],
    'default' => &$row['default'],
    'type' => &$row['type'],
    'settings' => &$row['settings'],
    'operations' => &$row['operations'],
  );
  $form['data']['statuses']['table']['#rows']['add_new'] = array(
    'name' => array('data' => &$row['name']),
    'label' => array('data' => &$row['label']),
    'default' => array('data' => &$row['default']),
    'type' => array('data' => &$row['type']),
    'settings' => array('data' => &$row['settings']),
    'operations' => array('data' => &$row['operations']),
  );

  // Clone Operations.
  // Extended Cloning Options
  $form['cloned_from'] = array(
    '#type' => 'value',
    '#value' => !empty($application_type->cloned_from) ? $application_type->cloned_from : NULL,
  );

  $form['clone_fields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Copy Field Definitions'),
    '#description' => t('This will copy the field instance settings from the original application type.'),
    '#default_value' => FALSE,
    '#access' => ($op == 'clone'),
  );

  $flexiforms = array();
  if ($op == 'clone') {
    $query = db_select('flexiform', 'f');
    $query->fields('f', array('form', 'label'));
    $query->condition('base_entity', 'application');
    $query->condition('base_entity_bundle', $application_type->cloned_from);
    $flexiforms = $query->execute()->fetchAllKeyed();
  }

  $form['clone_flexiforms'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Clone Flexible Forms'),
    '#description' => t('Tick which flexible forms to clone with this flexiform webform. The machine name of this flexiform webform will replace or be appended to the machine names and paths associated with these flexible forms.'),
    '#default_value' => array(),
    '#options' => $flexiforms,
    '#access' => ($op =='clone'),
    '#states' => array(
      'visible' => array(
        ':input[name="clone_fields"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save application type'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback to add a status.
 */
function application_type_form_submit_add_status($form, &$form_state) {
  $values = $form_state['values']['data']['statuses']['add_new'];
  $application_type = $form_state['application_type'];
  $application_type->data['statuses'][$values['name']] = $values;

  if ($form_state['values']['data']['default_status'] == 'add_new') {
    $application_type->data['default_status'] = $values['name'];
  }

  unset($form_state['input']['data']['statuses']['add_new']);
  unset($form_state['values']['data']['statuses']['add_new']);
  $form_state['rebuild'] = TRUE;
}

/**
 * Form API submit callback for the type form.
 */
function application_type_form_submit(&$form, &$form_state) {
  // Prepare form state values.
  $vals = &$form_state['values'];
  $vals['open_date'] = DateTime::createFromFormat("Y-m-d", $vals['open_date'])
    ->getTimestamp();
  $vals['close_date'] = DateTime::createFromFormat("Y-m-d", $vals['close_date'])
    ->getTimestamp();

  $application_type = entity_ui_form_submit_build_entity($form, $form_state);
  $application_type->data['courses'] = array_filter($application_type->data['courses']);

  if (!empty($application_type->data['statuses']['add_new'])) {
    unset($application_type->data['statuses']['add_new']);
  }
  $application_type->save();

  $form_state['redirect'] = 'admin/college/admin/application_types';

  // Copy fields.
  if (!empty($form_state['values']['clone_fields'])) {
    $original = $form_state['values']['cloned_from'];
    foreach (field_info_instances('application', $original) as $instance) {
      $instance['bundle'] = $application_type->type;
      if (!field_info_instance('application', $instance['field_name'], $application_type->type)) {
        field_create_instance($instance);
      }
    }
  }
  else {
    return;
  }

  // Copy Flexiforms
  $forms = array_filter($form_state['values']['clone_flexiforms']);
  $cloned = array();
  $original = $form_state['values']['cloned_from'];
  foreach (entity_load('flexiform', $forms) as $oldform) {
    $newForm = clone $oldform;

    // Clear id
    unset($newForm->id);

    // Work out new machine name.
    $new_form = str_replace($original, $application_type->type, $oldform->form, $count);
    $newForm->form = (!empty($count)) ? $new_form : $newForm->form . '_' . $application_type->type;

    // Stop from getting too long
    if (strlen($newForm->form) > 252) {
      $newForm->form = substr($newForm->form, 0, 252);
    }

    // Reduce duplicate.
    $query = db_select('flexiform')->condition('form', $newForm->form)->countQuery();
    if ($query->execute()->fetchField() > 0) {
      $i = 1;
      $new_form = $newForm->form . '_' . $i;

      $query = db_select('flexiform')->condition('form', $new_form)->countQuery();
      while ($query->execute()->fetchField() > 0) {
        $new_form = $newForm->form . "_{$i}";
        $query = db_select('flexiform')->condition('form', $new_form)->countQuery();
        $i++;
      }

      $newForm->form = $new_form;
    }

    // Change base_entity_bundle
    $newForm->base_entity_bundle = $application_type->type;
    $newForm->entities['base_entity']['bundle'] = $application_type->type;

    // Work out menu paths:
    $displays = array(
      'flexiform_create_entity_page',
      'flexiform_edit_entity_page'
    );

    foreach ($displays as $display) {
      if ($newForm->displays[$display]['enabled']) {
        $path = &$newForm->displays[$display]['path'];
        $new_path = str_replace($original, $application_type->type, $path, $count);
        $path = (!empty($count)) ? $new_path : $path . '/' . $application_type->type;
      }
    }

    $newForm->save();

    if (module_exists('flexiform_element_group')) {
      flexiform_element_group_clone_groups($oldform->form, $newForm->form);
    }

    $cloned[] = l($newForm->label, "admin/structure/flexiforms/manage/{$newForm->form}", array('attributes' => array('target' => '_blank')));
  }

  if (!empty($cloned)) {
    $message = t('The following flexiforms have been cloned:', array('%field' => $instance['label']));
    $list = array(
      '#theme' => 'item_list',
      '#items' => $cloned,
    );
    $message .= drupal_render($list);
    drupal_set_message($message);
  }
}

/**
 * Form API submit callback for the delete button.
 */
function application_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/college/admin/application_types/manage/' . $form_state['application_type']->type . '/delete';
}
