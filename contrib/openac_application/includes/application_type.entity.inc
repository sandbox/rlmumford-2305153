<?php
/**
 * @file application_type.entity.inc
 *   Classes and Controllers for the Application Type Entity
 */

/**
 * The class used for Application Type entities
 */
class ApplicationType extends Entity {
  public $type;
  public $label;

  public function __construct($values = array()) {
    parent::__construct($values, 'application_type');
  }
}

/**
 * The Controller for Application Type entities
 */
class ApplicationTypeController extends EntityAPIControllerExportable {

  /**
   * Create a openac_application type - we first set up the values that are specific
   * to our openac_application type schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the openac_application.
   *
   * @return
   *   A openac_application type object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Application
    $values += array(
      'id' => '',
      'is_new' => TRUE,
      'data' => '',
    );
    return parent::create($values);
  }
}
