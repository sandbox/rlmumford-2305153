<?php
/**
 * @files
 * Contains field definitions for the assigments system.
 */

$t = get_t();
$fields = array();

// Old Status
$field_name = 'application_log_old_status';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'message',
    'bundle' => 'application_status_change',
    'label' => $t('Previous Status'),
    'description' => $t('The status of the application before it changed.'),
  ),
);
// New Status
$field_name = 'application_log_new_status';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'message',
    'bundle' => 'application_status_change',
    'label' => $t('New Status'),
    'description' => $t('The new status of the application.'),
  ),
);
// Application Log Comments
$field_name = 'application_log_comment';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'message',
    'bundle' => 'application_status_change',
    'label' => $t('Comment'),
    'description' => $t('Why this status change has been made.'),
    'widget' => array('type' => 'text_textarea'),
  ),
);

// Course Application Notes
$field_name = 'course_application_notes';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'course',
    'bundle' => 'course',
    'label' => $t('Application Form Notes'),
    'description' => $t('Notes to display on the application form when a applicant selects this course.'),
    'widget' => array('type' => 'text_textarea'),
  ),
);
