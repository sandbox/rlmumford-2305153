<?php

/**
 * @file
 * Contains all the batch/queue functions.
 */

/**
 * Batch API Callback for generating submissions for an assignment.
 */
function openac_assignments_batch_generate_submissions($id, $max, $first_party = 1, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_party'] = $first_party - 1;
    $context['sandbox']['max'] = $max;
  }
  if (empty($context['results'][$id])) {
    $context['results'][$id] = array(
      'success' => 0,
      'errors' => array(),
    );
  }
  $limit = min(20, $context['sandbox']['max'] - $context['sandbox']['progress']);

  $assignment = reset(entity_load('assignment', array($id)));
  $controller = entity_get_controller('assignment');
  $errors = $controller->generateSubmissionsCallback($assignment, $context['sandbox']['current_party'], $limit, $context['sandbox']['progress']);

  if (!empty($errors)) {
    foreach ($errors as &$party) {
      $party = party_load($party);
      $party = $party->label;
    }
    $context['results'][$id]['errors'] += $errors;
  }
  $context['results'][$id]['success'] += $limit - count($errors);

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Finished callback for openac_assignments_batch_generate_submissions().
 */
function openac_assignments_batch_generate_submissions_finished($success, $results, $operations) {
  foreach ($results as $id => $result) {
    $assignment = entity_label('assignment', reset(entity_load('assignment', array($id))));
    if (!empty($result['success'])) {
      drupal_set_message(format_plural($result['success'],
        'Successfully generated one submission for @assignment',
        'Successfully generated @count submissions for @assignment',
        array('@assignment' => $assignment)));
    }
    if (!empty($result['errors'])) {
      drupal_set_message(format_plural(count($result['errors']),
        'Failed create @assignment submission for one party: @parties',
        'Failed create @assignment submissions for @count parties: @parties',
        array('@assignment' => $assignment, '@parties' => implode(', ', $result['errors']))), 'error');
    }
  }
}


/**
 * Queue API Callback for generating submissions for an assignment.
 */
function openac_assignments_queue_generate_submissions($data) {
  list ($id, $max, $current_party) = $data;
  $limit = min(20, $max);

  $assignment = reset(entity_load('assignment', array($id)));
  $controller = entity_get_controller('assignment');
  $errors = $controller->generateSubmissionsCallback($assignment, $current_party, $limit);

  if ($limit < $max) {
    // Queue up our next item.
    $queue = DrupalQueue::get('openac_assignments_generate_submissions');
    $queue->createItem(array($id, $max - $limit, $current_party));
  }
}

