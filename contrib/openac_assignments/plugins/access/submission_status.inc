<?php

/**
 * @file
 * Plugin to provide access control based upon submission status.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Submission status'),
  'description' => t('Control access by the status of a submission.'),
  'callback' => 'submission_status_access_check',
  'settings form' => 'submission_status_access_settings',
  'summary' => 'submission_status_access_summary',
  'required context' => new ctools_context_required(t('Submission'), 'entity:assignment_submission'),
);

/**
 * Settings form for the 'by term' access plugin
 */
function submission_status_access_settings($form, &$form_state, $conf) {
  $form['settings']['status'] = array(
    '#title' => t('Status'),
    '#description' => t('Select a status(es).'),
    '#default_value' => !empty($conf['status']) ? $conf['status'] : array(),
    '#multiple' => TRUE,
    '#type' => 'checkboxes',
    '#options' => openac_assignments_submission_status_options(),
  );

  $form['settings']['mode'] = array(
    '#title' => t('Status must be:'),
    '#type' => 'radios',
    '#default_value' => !empty($conf['mode']) ? $conf['mode'] : 'any',
    '#options' => array(
      'all' => t('All of the selected'),
      'any' => t('Any of the selected'),
    ),
  );

  return $form;
}

/**
 * Check for access.
 */
function submission_status_access_check($conf, $context) {
  // Get the hats.
  if (!isset($conf['status'])) {
    return FALSE;
  }
  $conf['status'] = array_filter($conf['status']);

  $statuses = openac_assignments_submission_get_statuses($context->data);

  if (!empty($conf['mode']) && $conf['mode'] == 'all') {
    return count($conf['status']) == count(array_intersect($conf['status'], $statuses));
  }
  else {
    return (bool) array_intersect($conf['status'], $statuses);
  }
}

/**
 * Provide a summary description based upon the checked terms.
 */
function submission_status_access_summary($conf, $context) {
  $statuses = array_intersect_key(openac_assignments_submission_status_options(), array_filter($conf['status']));
  $conf['mode'] = !empty($conf['mode']) ? $conf['mode'] : 'any';

  return format_plural(count($statuses),
    '@status can be the hat "@statuses"',
    '@status can be @mode of these statuses: @statuses',
    array('@statuses' => implode(', ', $statuses),
      '@status' => $context->identifier,
      '@mode' => t($conf['mode'])));
}
