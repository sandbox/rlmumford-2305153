<?php

/**
 * @file
 * CTools content for student submission form.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Submission Forms'),
  'content types' => 'submission_forms_content_types',
  'render callback' => 'submission_forms_render',
  'admin title' => 'submission_forms_admin_title',
);

/**
 * Return all sub types.
 */
function submission_forms_content_types() {
  $defaults = array(
    'category' => array(t('Submissions'), 0),
    'required context' => new ctools_context_required(t('Submission'), 'entity:assignment_submission'),
    'edit form' => 'submission_forms_edit_form',
  );
  return array(
    'upload' => array(
      'title' => t('Upload work'),
      'description' => t('Form for students to upload their work.'),
    ) + $defaults,
    'mark' => array(
      'title' => t('Mark assignment'),
      'description' => t('Form for marking an assignment.'),
    ) + $defaults,
  );
}

/**
 * Render callback for submission forms.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function submission_forms_render($subtype, $conf, $args, $context) {
  $types = submission_forms_content_types();

  // Start building our return
  $block = new stdClass();
  $block->title = $types[$subtype]['title'];

  // Get our form making sure we get all the build info.
  $submission = $context->data;
  $assignment = entity_load_single('assignment', $submission->assignment);
  $form = drupal_get_form('submission_forms_form__' . $subtype, $submission);

  $block->content = drupal_render($form);
  return $block;
}


/**
 * Alter our forms.
 */
function openac_assignments_form_submission_forms_form_alter(&$form, &$form_state) {
  // Make sure our includes are loaded.
  form_load_include($form_state, 'inc', 'openac_assignments', 'plugins/content_types/submission_forms');
  form_load_include($form_state, 'inc', 'openac_assignments', 'assignment_submission.admin');

  // Add our submit callback to handle redirects.
  $form['#submit'][] =
    $form['actions']['submit']['#submit'][] = 'submission_forms_submit';
}

function openac_assignments_form_submission_forms_form__upload_alter(&$form, &$form_state) {
  openac_assignments_form_submission_forms_form_alter($form, $form_state);

  // Set up an access list and make other adjustments.
  $access = array(
    'actions' => array('submit' => TRUE),
    'form_build_id' => TRUE,
    'form_token' => TRUE,
    'form_id' => TRUE,
    'submission_submission' => TRUE,
  );
  submission_forms_recursive_access($form, $access);

  $form['message'] = array(
    '#markup' => '<p style="color: red;">' . t('After selecting and uploading your file please click on the submit button below.') . '</p>',
    '#weight' => -5,
  );
  $form['actions']['submit']['#value'] = t('Submit Work');

  $form_state['submit_message'] = t('Your assignment has been successfully submitted.  You do not need to do anything else, but if you would like to resubmit your work before the deadline you may do so.');
}

function openac_assignments_form_submission_forms_form__mark_alter(&$form, &$form_state) {
  openac_assignments_form_submission_forms_form_alter($form, $form_state);

  // Set up an access list and make other adjustments.
  $access = array(
    'assignment_name' => TRUE,
    'student_name' => TRUE,
    'marker_name' => TRUE,
    'extension_date' => TRUE,
    'submission_admin_notes' => TRUE,
    'deduction' => TRUE,
    'submission_deduction_notes' => TRUE,
    'submission_submission' => TRUE,
  );
  submission_forms_recursive_access($form, $access, TRUE);
  submission_forms_recursive_access($form['actions'], array('submit' => TRUE));

  global $user;
  $party = party_user_get_party($user);
  $form['marker'] = array(
    '#type' => 'value',
    '#value' => isset($party->pid) ? $party->pid: NULL,
  );
}

/**
 * Submit callback for the form.
 */
function submission_forms_submit(&$form, &$form_state) {
  $form_state['redirect'] = current_path();

  if (isset($form_state['submit_message'])) {
    drupal_set_message($form_state['submit_message']);
  }
}

/**
 * 'Edit' callback for the content type.
 *
 * Return the form so we get all the defaults.
 */
function submission_forms_edit_form($form, &$form_state) {
  return $form;
}

function submission_forms_admin_title($subtype) {
  $types = submission_forms_content_types();
  return $types[$subtype]['title'];
}

/**
 * Recursive callback for removing access.
 *
 * @param $element
 *   The form element we're dealing with.
 * @param $access
 *   An array keyed by the child element names specifying how to behave. If the
 *   child element doesn't exist or is empty, it is denied. If it is non-strict
 *   TRUE, it is left as is. If it is an array, we recurse again for more
 *   granular control.
 * @param $mode
 *
 */
function submission_forms_recursive_access(&$element, $access, $mode = FALSE) {
  // Deny access to anything not in the list.
  foreach (element_children($element) as $key) {
    if (isset($access[$key]) && is_array($access[$key])) {
      submission_forms_recursive_access($element[$key], $access[$key], $mode);
    }
    elseif (!($mode ^ isset($access[$key]))) {
      unset($element[$key]);
    }
  }
}
