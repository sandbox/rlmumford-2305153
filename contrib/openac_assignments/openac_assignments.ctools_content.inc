<?php
/**
 * @file
 * openac_assignments.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function openac_assignments_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'user_dashboard__assignment_intro';
  $content->admin_title = 'User Dashboard - Assignment Intro';
  $content->admin_description = '';
  $content->category = 'User Dashboard';
  $content->settings = array(
    'admin_title' => 'User Dashboard - Assignment Intro',
    'title' => '',
    'body' => 'Welcome to My Assignments. You will see any assignments you have appear on this page as they become available and you can submit your work. You\'ll also be able to view your previous assignments and the marks you received.',
    'format' => 'panopoly_wysiwyg_text',
    'substitute' => 1,
  );
  $export[$content->name] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'user_dashboard__assignment_late_submissions';
  $content->admin_title = 'User Dashboard - Assignment Late Submissions';
  $content->admin_description = '';
  $content->category = 'User Dashboard';
  $content->settings = array(
    'admin_title' => 'User Dashboard - Assignment Late Submissions',
    'title' => 'Late Submissions',
    'body' => 'If you have missed a deadline, please contact the office. We will re-open your assignment and you will incur a deduction to your final mark for late submission.',
    'format' => 'panopoly_wysiwyg_text',
    'substitute' => 1,
  );
  $export[$content->name] = $content;

  return $export;
}
