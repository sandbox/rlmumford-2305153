<?php

/**
 * Implements hook_entity_property_info_alter().
 *
 * Add property information to submission and assignment entities.
 */
function openac_assignments_entity_property_info_alter(&$info) {
  $info['assignment']['properties']['class'] = array(
    'label' => t('Class'),
    'type' => 'class',
    'description' => t('The module the assignment is part of.'),
    'schema field' => 'class',
    'required' => TRUE,
  );
  
  $info['assignment']['properties']['submission_type'] = array(
    'label' => t('Submission Type'),
    'type' => 'assignment_submission_type',
    'description' => t('The module the assignment is part of.'),
    'schema field' => 'submission_type',
    'options list' => 'openac_assignments_submission_type_options_list',
    'required' => TRUE,
  );

  $info['assignment']['properties']['open_date'] = array(
    'label' => t('Open Date'),
    'type' => 'date',
    'description' => t('Start date for the assignment'),
    'schema field' => 'open_date',
  );

  $info['assignment']['properties']['due_date'] = array(
    'label' => t('Due Date'),
    'type' => 'date',
    'description' => t('Due date for the assignment'),
    'schema field' => 'due_date',
  );

  $info['assignment']['properties']['mark_date'] = array(
    'label' => t('Mark Release Date'),
    'type' => 'date',
    'description' => t('Scheduled date for release of marks'),
    'schema field' => 'mark_date',
  );

  $info['assignment_submission']['properties']['assignment'] = array(
    'label' => t('Assignment'),
    'type' => 'assignment',
    'description' => t('The assignment the submission relates to.'),
    'schema field' => 'assignment',
  );

  $info['assignment_submission']['properties']['student'] = array(
    'label' => t('Student'),
    'type' => 'party',
    'description' => t('The student who submitted the submission.'),
    'schema field' => 'student',
  );

  $info['assignment_submission']['properties']['marker'] = array(
    'label' => t('Marker'),
    'type' => 'party',
    'description' => t('The party that marked this submission.'),
    'schema field' => 'marker',
  );

  $info['assignment_submission']['properties']['extension_date'] = array(
    'label' => t('Extension Due Date'),
    'type' => 'date',
    'description' => t('The extended due date for this student.'),
    'schema field' => 'extension_date',
  );

  $info['assignment_submission']['properties']['status'] = array(
    'label' => t('Status'),
    'type' => 'list<integer>',
    'description' => t('The statuses of this submission.'),
    'options list' => 'openac_assignments_submission_status_options',
    'getter callback' => 'openac_assignments_submission_get_statuses',
    'computed' => TRUE,
  );

  $info['assignment_submission']['properties']['final_mark'] = array(
    'label' => t('Final Mark'),
    'type' => 'integer',
    'description' => t('The final mark (with deductions).'),
    'getter callback' => 'openac_assignments_submission_get_final_mark',
    'computed' => TRUE,
  );
}
