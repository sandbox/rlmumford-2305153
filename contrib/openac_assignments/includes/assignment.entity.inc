<?php
/**
 * @file assignment.entity.inc
 *   Classes and Controllers for the Assignment Entity
 */

/**
 * The class used for Assignment entities
 */
class Assignment extends OpenAcEntity {

  public function __construct($values = array()) {
    parent::__construct($values, 'assignment');
  }

  public function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return NULL;
  }
}

/**
 * The Controller for Assignment entities
 */
class AssignmentController extends OpenAcEntityController {

  /**
   * Create a wtc_assignment - we first set up the values that are specific
   * to our wtc_assignment schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the wtc_assignment.
   *
   * @return
   *   A wtc_assignment object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Assignment
    $values += array(
      'id' => '',
      'is_new' => TRUE,
      'name' => '',
    );

    $assignment = parent::create($values);
    return $assignment;
  }

  /**
   * Set up batching/queues for generating submissions for this assignment.
   *
   * @param $entity
   *   The assignment entity we're generating submissions for.
   * @param $ui
   *   Whether this is happening as part of a UI. This controls whether we use
   *   Batch API or the Queue. Batch API is used when this is happening as part
   *   of a UI, Queue is used when done programatically and things get added to
   *   the cron queue.
   */
  public function generateSubmissions(Assignment $entity = NULL, $ui = FALSE, $redirect = NULL) {
    // Perform our query to find parties that need a submission creating.
    $aliases = array();
    $query = $this->missingSubmissionQuery($aliases);
    $query->groupBy("{$aliases['assignment']}.id");
    $query->addField($aliases['assignment'], 'id', 'id');
    $query->addExpression('COUNT(DISTINCT party.pid)', 'parties');
    $query->addExpression('MIN(party.pid)', 'first_party');
    $query->havingCondition('parties', 0, '>');

    if (isset($entity)) {
      $query->condition("{$aliases['assignment']}.id", $entity->id);
    }

    // Load our results and process.
    $results = $query->execute()->fetchAllAssoc('id');
    if (!empty($results)) {
      if ($ui) {
        // Set up as a UI batch.
        $batch = array(
          'title' => t('Generating Submissions'),
          'operations' => array(),
          'finished' => 'openac_assignments_batch_generate_submissions_finished',
          'file' => drupal_get_path('module', 'openac_assignments') . '/openac_assignments.batch.inc',
        );

        // Add each batch as an operation to make our query simpler.
        foreach ($results as $id => $details) {
          $batch['operations'][] = array(
            'openac_assignments_batch_generate_submissions',
            array($id, $details->parties, $details->first_party),
          );
        }

        batch_set($batch);
        batch_process($redirect);
      }
      else {
        // Set up as a queue for crons.
        $queue = DrupalQueue::get('openac_assignments_generate_submissions');
        foreach ($results as $id => $details) {
          // We need to break these into smaller chunks for the queue.
          $queue->createItem(array($id, $details->parties, $details->first_party -1));
        }
      }
    }
  }

  /**
   * Generate submissions for an assignment.
   *
   * @param $assignment
   *   The assignment we're generating submissions for.
   * @param $current_party
   *   The party id of the last completed party to make batching easier and
   *   prevent us getting stuck in loops.
   * @param $limit
   *   How many parties to generate for at a time.
   * @param $progress int
   *   A variable passed by reference to keep track of progress.
   *
   * @return array
   *   An array of party ids which encountered an error.
   */
  public function generateSubmissionsCallback(Assignment $assignment, &$current_party = 0, $limit = NULL, &$progress = 0) {
    $errors = array();

    $aliases = array();
    $query = $this->missingSubmissionQuery($aliases)
      ->fields('party', array('pid'))
      ->condition("{$aliases['assignment']}.id", $assignment->id)
      ->condition('party.pid', $current_party, '>')
      ->groupBy('party.pid')
      ->orderBy('party.pid');
    if ($limit) {
      $query->range(0, $limit);
    }

    $parties = $query->execute()->fetchCol();
    foreach ($parties as $party) {
      $current_party = $party;

      $submission = entity_create('assignment_submission', array(
        'assignment' => $assignment->id,
        'type' => $assignment->submission_type,
        'student' => $party,
      ));

      if ($submission) {
        if (!$submission->save()) {
          $errors[$party] = $party;
          $party = party_load($party);
          watchdog('openac_assignments', 'Unable to save submission for @party.', array('@party' => $party->label), WATCHDOG_WARNING);
        }
      }
      else {
        $errors[$party] = $party;
        $party = party_load($party);
        watchdog('openac_assignments', 'Unable to create submission for @party.', array('@party' => $party->label), WATCHDOG_WARNING);
      }

      $progress++;
    }

    return $errors;
  }

  /**
   * Builds a fully constructed SelectQuery which with all the joins for
   * finding missing submissions.
   *
   * @param $aliases
   *   An array that will be filled out with the aliases used.
   *
   * @return SelectQuery.
   */
  protected function missingSubmissionQuery(&$aliases = array()) {
    $query = party_query();
    $aliases['class_registration'] = $query->join('class_registration', 'cr', '%alias.student = party.pid');
    $aliases['assignment'] = $query->join('assignment', 'a', "%alias.class = {$aliases['class_registration']}.class");
    $aliases['submission'] = $query->leftJoin('submission', 's', "%alias.student = party.pid AND %alias.assignment = {$aliases['assignment']}.id");

    // Make sure we only select ones without a submission.
    $query->condition("{$aliases['submission']}.submission_id", NULL);
    $query->condition("{$aliases['class_registration']}.status", "active");

    return $query;
  }

}
