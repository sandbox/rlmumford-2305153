<?php
/**
 * @file submission.entity.inc
 *   Classes and Controllers for the Submission Entity
 */

/**
 * The class used for Submission entities
 */
class AssignmentSubmission extends OpenAcEntity {

  public function __construct($values = array()) {
    parent::__construct($values, 'assignment_submission');
  }

  public function defaultLabel() {
    $assignment = entity_load_single('assignment', $this->assignment);
    $student = entity_load_single('party', $this->student);
    return t("@student's submission for @assignment", array(
      '@student' => entity_label('party', $student),
      '@assignment' => entity_label('assignment', $assignment),
    ));
  }

  protected function defaultUri() {
    return NULL;
  }
}

/**
 * The Controller for Submission entities
 */
class AssignmentSubmissionController extends OpenAcEntityController {

  /**
   * Create a wtc_submission - we first set up the values that are specific
   * to our wtc_submission schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the wtc_submission.
   *
   * @return
   *   A wtc_submission object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Submission
    $values += array(
      'submission_id' => '',
      'is_new' => TRUE,
      'marker' => 0,
    );

    $submission = parent::create($values);
    return $submission;
  }

}
