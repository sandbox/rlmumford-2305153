<?php

/**
 * @file
 * Assignment editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class AssignmentUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a assignment',
      'description' => 'Add a new assignment',
      'page callback'  => 'assignment_add_page',
      'access callback'  => 'assignment_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 20,
      'file' => 'includes/assignment.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    $items[$this->path . '/manage/%assignment'] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('assignment', $id_count + 1),
      'page callback' => 'assignment_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'assignment_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'type' => MENU_CALLBACK,
      'file' => 'includes/assignment.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    $items[$this->path . '/manage/%assignment/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    $items[$this->path . '/manage/%assignment/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'assignment_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'assignment_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'weight' => 10,
      'file' => 'includes/assignment.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    $items['admin/college/admin/classes/class/%class/add-assignment'] = array(
      'title' => t('Add Assignment'),
      'page callback' => 'assignment_add_form_wrapper',
      'page arguments' => array(5),
      'access callback' => 'openac_class_access',
      'access arguments' => array('edit', 5),
      'file' => 'includes/assignment.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );
    $items['admin/college/admin/classes/class/%class/add-assignment/%ctools_js'] = array(
      'page arguments' => array(5, 7),
    ) + $items['admin/college/admin/classes/class/%class/add-assignment'];

    return $items;
  }


  /**
   * Create the markup for the add Assignment Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage($class = NULL) {
    $assignment = assignment_create();
    if (!empty($class)) {
      $assignment->class = $class->id;
    }
    return assignment_form_wrapper($assignment);
  }

}

/**
 * Add page for assignments.
 */
function assignment_add_page($class = NULL) {
  return entity_ui_controller('assignment')->addPage($class);
}

/**
 * Assignment config page
 */
function assignment_config_page() {
  return "Some assignment Entity Settings";
}

/**
 * Form callback to add an assignment.
 */
function assignment_add_form_wrapper($class = NULL, $js = FALSE) {
  $values = array();
  if (!empty($class->id)) {
    $values['class'] = $class->id;
  }
  $assignment = entity_create('assignment', $values);

  return assignment_form_wrapper($assignment, 'create', $js);
}

/**
 * Form callback wrapper: create or edit a assignment.
 *
 * @param $assignment
 *   The assignment object being edited by this form.
 *
 * @see assignment_edit_form()
 */
function assignment_form_wrapper($assignment, $op = 'edit', $js = FALSE) {
  if (!$js) {
    return drupal_get_form('assignment_edit_form', $assignment, $op);
  }

  ctools_include('ajax');
  ctools_include('modal');

  // Work out Form title.
  $info = entity_get_info('assignment');
  $title = (empty($assignment->id)) ? t('Add !label', array('!label' => $info['label'])) : t('Edit @label', array('@label' => $assignment->name));

  $form_state = array(
    'title' => $title,
    'ajax' => TRUE,
    'build_info' => array(
        'args' => array(
        $assignment,
        $op,
      ),
    ),
  );
  $commands = ctools_modal_form_wrapper('assignment_edit_form', $form_state);

  if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
    // Overwrite the output if form submission was successfully executed.
    $commands = array();
    $commands[] = ctools_ajax_command_reload();
  }

  $output = array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
  ajax_deliver($output);
  drupal_exit();
}


/**
 * Form callback wrapper: delete a assignment.
 *
 * @param $assignment
 *   The assignment object being edited by this form.
 *
 * @see assignment_edit_form()
 */
function assignment_delete_form_wrapper($assignment) {
  // Add the breadcrumb for the form's location.
  //assignment_set_breadcrumb();
  return drupal_get_form('assignment_delete_form', $assignment);
}


/**
 * Form callback: create or edit a assignment.
 *
 * @param $assignment
 *   The assignment object to edit or for a create form an empty assignment object
 *     with only a assignment type defined.
 */
function assignment_edit_form($form, &$form_state, $assignment, $op = 'edit') {
  if ($op == 'delete') {
    return assignment_delete_form($form, $form_state, $assignment);
  }

  $assignment_wrapper = entity_metadata_wrapper('assignment', $assignment);

  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Assignment Title'),
    '#default_value' => isset($assignment->name) ? $assignment->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('Class'),
    '#max_length' => 60,
    '#default_value' => !empty($assignment->class) ? entity_label('class', $assignment_wrapper->class) . ' (' . $assignment->class . ')' : '',
    '#autocomplete_path' => 'class/autocomplete/any',
    '#weight' => -3,
    '#disabled' => !empty($assignment->class),
  );

  $form['open_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Open Date'),
    '#default_value' => isset($assignment->open_date) ? date('Y-m-d H:i:s', $assignment->open_date): NULL,
    '#date_format' => 'Y-m-d H:i',
    '#date_label_position' => 'within',
  );

  $form['due_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Due Date'),
    '#default_value' => isset($assignment->due_date) ? date('Y-m-d H:i:s', $assignment->due_date): NULL,
    '#date_format' => 'Y-m-d H:i',
    '#date_label_position' => 'within',
  );

  $form['mark_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Mark Date'),
    '#description' => t('The Date and time the marks will be released'),
    '#default_value' => isset($assignment->mark_date) ? date('Y-m-d H:i:s', $assignment->mark_date): NULL,
    '#date_format' => 'Y-m-d H:i',
    '#date_label_position' => 'within',
  );

  $form['marks_released'] = array(
    '#type' => 'checkbox',
    '#title' => t('Marks Released'),
    '#description' => t('Have the marks for this assigment been released? Note: this will only release marks where available.'),
    '#default_value' => !empty($assignment->marks_released),
  );

  // Add the field related form elements.
  $form_state['assignment'] = $assignment;
  field_attach_form('assignment', $assignment, $form, $form_state);

  $form['submission_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submission Settings'),
  );

  $form['submission_settings']['submission_type'] = array(
    '#type' => 'select',
    '#title' => t('Submission Type'),
    '#description' => t('Which type of submission should be used for this assignment'),
    '#options' => openac_assignments_submission_type_options_list(),
    '#default_value' => !empty($assignment->submission_type) ? $assignment->submission_type : '',
    '#required' => TRUE,
  );

  $form['submission_settings']['generate_submissions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate submissions immediately'),
    '#description' => t('Submissions will be generated automatically. However, if you need them generate immediately, check this box.'),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save assignment'),
    '#submit' => $submit + array('assignment_edit_form_submit'),
  );

  if (!empty($assignment->id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete assignment'),
      '#suffix' => l(t('Cancel'), 'admin/content/assignments'),
      '#submit' => $submit + array('assignment_edit_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'assignment_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the assignment form
 */
function assignment_edit_form_validate(&$form, &$form_state) {
  $assignment = $form_state['assignment'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('assignment', $assignment, $form, $form_state);
}


/**
 * Form API submit callback for the assignment form.
 *
 * @todo remove hard-coded link
 */
function assignment_edit_form_submit(&$form, &$form_state) {
  $assignment = entity_ui_controller('assignment')->entityFormSubmitBuildEntity($form, $form_state);

  // Get class id.
  preg_match('/\(([0-9]+)\)/', $form_state['values']['class'], $ids);
  $assignment->class = array_pop($ids);

  if (isset($assignment->open_date)) {
    if (!is_numeric($assignment->open_date)) {
      $assignment->open_date = strtotime($assignment->open_date);
    }
    if ($assignment->open_date === 0) {
      $assignment->open_date = NULL;
    }
  }

  if (isset($assignment->due_date)) {
    if (!is_numeric($assignment->due_date)) {
      $assignment->due_date = strtotime($assignment->due_date);
    }
    if ($assignment->due_date === 0) {
      $assignment->due_date = NULL;
    }
  }

  if (isset($assignment->mark_date)) {
    if (!is_numeric($assignment->mark_date)) {
      $assignment->mark_date = strtotime($assignment->mark_date);
    }
    if ($assignment->mark_date === 0) {
      $assignment->mark_date = NULL;
    }
  }

  $assignment->save();

  if (!empty($form_state['values']['generate_submissions'])) {
    $controller = entity_get_controller('assignment');
    $controller->generateSubmissions($assignment, TRUE, 'admin/college/admin/assignments');
  }
  else {
    $form_state['redirect'] = 'admin/college/admin/assignments';
  }
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function assignment_edit_form_submit_delete(&$form, &$form_state) {
  $form_state['build_info']['args'][1] = 'delete';
  $form_state['rebuild'] = TRUE;
  //$form_state['redirect'] = 'admin/content/assignments/assignment/' . $form_state['assignment']->id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a assignment.
 *
 * @param $assignment
 *   The assignment to delete
 *
 * @see confirm_form()
 */
function assignment_delete_form($form, &$form_state, $assignment) {
  $form_state['assignment'] = $assignment;

  $form['#validate'][] = 'assignment_delete_form_validate';
  $form['#submit'][] = 'assignment_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete assignment %name?', array('%name' => $assignment->title)),
    'admin/content/assignments/assignment',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Validate callback for assignment_delete_form
 */
function assignment_delete_form_validate($form, &$form_state) {
}

/**
 * Submit callback for assignment_delete_form
 */
function assignment_delete_form_submit($form, &$form_state) {
  $assignment = $form_state['assignment'];

  entity_delete('assignment', $assignment->id);

  // Get all submissions.
  $ids = db_select('submission', 's')
    ->fields('s', array('submission_id'))
    ->condition('s.assignment', $assignment->id)
    ->execute()
    ->fetchCol();
  foreach ($ids as $id) {
    entity_delete('assignment_submission', $id);
  }

  drupal_set_message(t('The assignment %name has been deleted.', array('%name' => $assignment->title)));
  watchdog('assignment', 'Deleted assignment %name.', array('%name' => $assignment->title));

  $form_state['redirect'] = 'admin/college/admin/assignments';
}
