<?php

/**
 * @file
 * Submission editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class AssignmentSubmissionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {

    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    // Loading and editing wtc_submission entities
    $items[$this->path . '/submission/' . $wildcard] = array(
      'page callback' => 'assignment_submission_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'assignment_submission_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/assignment_submission.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/submission/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$this->path . '/submission/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'assignment_submission_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'assignment_submission_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'includes/assignment_submission.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );

    return $items;
  }

}

/**
 * Submission config page
 */
function assignment_submission_config_page() {
  return "Some submission Entity Settings";
}

/**
 * Form callback wrapper: create or edit a submission.
 *
 * @param $submission
 *   The submission object being edited by this form.
 *
 * @see submission_edit_form()
 */
function assignment_submission_form_wrapper($submission) {
  return drupal_get_form('assignment_submission_edit_form', $submission);
}


/**
 * Form callback wrapper: delete a submission.
 *
 * @param $submission
 *   The submission object being edited by this form.
 *
 * @see submission_edit_form()
 */
function assignment_submission_delete_form_wrapper($submission) {
  return drupal_get_form('assignment_submission_delete_form', $submission);
}


/**
 * Form callback: create or edit a submission.
 *
 * @param $submission
 *   The submission object to edit or for a create form an empty submission object
 *     with only a submission type defined.
 */
function assignment_submission_edit_form($form, &$form_state, $submission) {
  $form_state['build_info']['files']['openac_assignments:includes/assignment_submission.admin.inc'] = array(
    'type' => 'inc',
    'module' => 'openac_assignments',
    'name' => 'includes/assignment_submission.admin',
  );
  $assignment = assignment_load($submission->assignment);
  $student = isset($submission->student) ? party_load($submission->student): NULL;
  $marker = isset($submission->marker) ? party_load($submission->marker): NULL;

  // Add the default field elements.
  $form['assignment_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Assignment'),
    '#default_value' => isset($assignment->name) ? $assignment->name : '',
    '#disabled' => empty($submission->is_new),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['student_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Student'),
    '#default_value' => isset($student->label) ? $student->label : '',
    '#disabled' => empty($submission->is_new),
    '#required' => TRUE,
    '#weight' => -4,
  );

  $form['marker_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Marker'),
    '#default_value' => isset($marker->label) ? $marker->label : '',
    '#disabled' => empty($submission->is_new),
    '#weight' => -3,
  );

  $form['mark'] = array(
    '#type' => 'textfield',
    '#title' => t('Mark'),
    '#default_value' => isset($submission->mark) ? $submission->mark : '',
    '#weight' => 1,
    '#size' => 5,
    '#description' => t("Note: once a mark has been entered, it is considered ready for release. You can enter marker's notes leaving the mark empty if you need to come back later."),
  );

  $form['deduction'] = array(
    '#type' => 'textfield',
    '#title' => t('Deduction'),
    '#description' => t('The percentage to be deducted from the total mark.'),
    '#default_value' => isset($submission->deduction) ? $submission->deduction : 0,
    '#weight' => 1,
    '#size' => 5,
  );

  $form['extension_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Revised Due Date'),
    '#description' => t('Leave blank to use the default assignment due date.'),
    '#default_value' => isset($submission->extension_date) ? date('Y-m-d H:i:s', $submission->extension_date): NULL,
    '#date_format' => 'Y-m-d H:i',
    '#date_label_position' => 'within',
    '#weight' => -3,
  );

  // Add the field related form elements.
  $form_state['assignment_submission'] = $submission;
  field_attach_form('assignment_submission', $submission, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save submission'),
    '#submit' => $submit + array('assignment_submission_edit_form_submit'),
  );

  if (!empty($submission->submission_id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete submission'),
      '#suffix' => l(t('Cancel'), 'admin/college/admin/submissions'),
      '#submit' => $submit + array('assignment_submission_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'assignment_submission_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the submission form
 */
function assignment_submission_edit_form_validate(&$form, &$form_state) {
  $submission = $form_state['assignment_submission'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('assignment_submission', $submission, $form, $form_state);

  // Check that deduction is between 0 and 100
  if (isset($form_state['values']['deduction']) && !(0 <= $form_state['values']['deduction'] && $form_state['values']['deduction'] <= 100)) {
    form_set_error('deduction', t('Deduction must be between 0% and 100%'));
  }
}


/**
 * Form API submit callback for the submission form.
 *
 * @todo remove hard-coded link
 */
function assignment_submission_edit_form_submit(&$form, &$form_state) {
  $submission = entity_ui_controller('assignment_submission')->entityFormSubmitBuildEntity($form, $form_state);

  if (isset($submission->extension_date)) {
    if (!is_numeric($submission->extension_date)) {
      $submission->extension_date = strtotime($submission->extension_date);
    }
    if ($submission->extension_date === 0) {
      $submission->extension_date = NULL;
    }
  }

  // Make sure we only save numeric values for the mark.
  if ($submission->mark === '') {
    $submission->mark = NULL;
  }

  // Save the submission and go back to the list of submissions
  $submission->save();
  $form_state['redirect'] = 'admin/college/admin/submissions';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function assignment_submission_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/college/admin/submissions/submission/' . $form_state['assignment_submission']->submission_id . '/delete';

  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();
}


/**
 * Form callback: confirmation form for deleting a submission.
 *
 * @param $submission
 *   The submission to delete
 *
 * @see confirm_form()
 */
function assignment_submission_delete_form($form, &$form_state, $submission) {
  $form_state['assignment_submission'] = $submission;

  $form['#submit'][] = 'assignment_submission_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete submission %name?', array('%name' => $submission->label())),
    'admin/college/admin/submissions/submission',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for submission_delete_form
 */
function assignment_submission_delete_form_submit($form, &$form_state) {
  $submission = $form_state['assignment_submission'];

  $submission_title = $submission->title;
  $submission->delete();

  drupal_set_message(t('The submission %name has been deleted.', array('%name' => $submission_title)));
  watchdog('submission', 'Deleted submission %name.', array('%name' => $submission_title));

  $form_state['redirect'] = 'admin/college/admin/submissions';
}
