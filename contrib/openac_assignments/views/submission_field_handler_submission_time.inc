<?php
/**
 * @file
 * Contains a Views field handler for student status on a submission.
 */

class submission_field_handler_submission_time extends views_handler_field_date {
  function construct() {
    parent::construct();

    $this->additional_fields['submission_id'] = 'submission_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function get_value($values, $field = NULL) {
    if ($field === NULL || $field == $this->field) {
      $submission = entity_load_single('assignment_submission', $this->get_value($values, 'submission_id'));
      return !empty($submission->submission_submission[LANGUAGE_NONE][0]['timestamp']) ? $submission->submission_submission[LANGUAGE_NONE][0]['timestamp']: NULL;
    }
    else {
      return parent::get_value($values, $field);
    }
  }

}
