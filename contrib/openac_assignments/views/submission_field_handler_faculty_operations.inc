<?php
/**
 * @file
 * Contains a Views field handler for student operations on a submission.
 */

class submission_field_handler_faculty_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['submission_id'] = 'submission_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $submission = entity_load_single('assignment_submission', $this->get_value($values, 'submission_id'));
    $statuses = openac_assignments_submission_get_statuses($submission);

    if (!empty($statuses['submitted'])) {
      $text = empty($statuses['marked_mark']) ? t('Mark'): t('Edit Mark');
    }

    if (!empty($text)) {
      return l($text, 'user/' . arg(1) . '/assignments/' . $values->submission_id, array(
        'query' => drupal_get_destination(),
        ));
    }
  }
}
