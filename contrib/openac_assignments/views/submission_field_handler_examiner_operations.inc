<?php
/**
 * @file
 * Contains a Views field handler for student operations on a submission.
 */

class submission_field_handler_examiner_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['submission_id'] = 'submission_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $submission = entity_load_single('assignment_submission', $this->get_value($values, 'submission_id'));
    $statuses = openac_assignments_submission_get_statuses($submission);

    return l(t('View'), 'user/' . arg(1) . '/assignments/' . $values->submission_id, array(
      'query' => array(
        'destination' => current_path(),
      ),
    ));
  }
}

