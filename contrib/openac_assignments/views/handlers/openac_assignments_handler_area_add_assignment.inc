<?php
/**
 * @file
 * Includes views area handler for adding assignment links.
 */

/**
 * OpenAC Assignments area handler.
 */
class openac_assignments_handler_area_add_assignment extends views_handler_area {

  /**
   * Render the area.
   */
  function render($empty = FALSE) {
    $link_id = NULL;
    if (!empty($this->view->argument['class']->value)) {
      $link_id = $this->view->argument['class']->value[0];
    }

    if (!empty($this->view->argument['id'])) {
      $arg = $this->view->argument['id'];
      $table = $arg->table;

      if ($table == 'class') {
        $link_id = $arg->value[0];
      }
    }

    $links = array();
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();
    $links['assignment'] = array(
      'href' => 'admin/college/admin/classes/class/'.$link_id.'/add-assignment/nojs',
      'title' => t('Add Assignment'),
      'localized_options' => array(
        'attributes' => array(
          'class' => array('ctools-use-modal'),
        ),
      ),
    );

    $output = array(
      '#prefix' => '<ul class="action-links">',
      '#suffix' => '</ul>',
    );
    foreach ($links as $key => $link) {
      $output[$key] = array(
        '#theme' => 'menu_local_action',
        '#link' => $link,
      );
    }

    return render($output);
  }

}
