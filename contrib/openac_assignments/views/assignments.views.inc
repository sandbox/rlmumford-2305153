<?php

/**
 *
 */

class AssignmentsViewsController extends EntityDefaultViewsController {
  protected function schema_fields() {
    $data = parent::schema_fields();

    $data['class']['relationship']['base field'] = 'id';

    $this->relationships['class']['assignment']['relationship']['relationship field'] = 'id';

    return $data;
  }
}
