<?php
/**
 * @file
 * Contains a Views field handler for student operations on a submission.
 */

class submission_field_handler_final_mark extends views_handler_field_numeric {
  function construct() {
    parent::construct();

    $this->additional_fields['mark'] = 'mark';
    $this->additional_fields['deduction'] = 'deduction';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function get_value($values, $field = NULL) {
    if ($field === NULL || $field == $this->field) {
      $mark = $this->get_value($values, 'mark');
      if (!isset($mark)) {
        return NULL;
      }

      $deduction = $this->get_value($values, 'deduction');

      return ($mark - $deduction) > 0 ? $mark - $deduction : 0;
    }
    else {
      return parent::get_value($values, $field);
    }
  }

}
