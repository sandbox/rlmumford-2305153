<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */

class submission_field_handler_edit_link extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['submission_id'] = 'submission_id';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $submission_id = $values->{$this->aliases['submission_id']};
    $destination = drupal_get_destination();
    $url = $destination['destination'];

    return l($text, 'admin/college/admin/submissions/submission/' . $submission_id, array('query' => array('destination' => $url)));
  }
}
