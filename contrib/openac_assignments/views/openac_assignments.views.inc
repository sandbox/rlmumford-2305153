<?php
/**
 * @file
 * Providing extra functionality for the Assignments UI via views.
 */

/**
 * Implements hook_views_data()
 */
function openac_assignments_views_data_alter(&$data) {
  // Add our additional fields.
  $data['assignment']['edit_link'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the assignment.'),
      'handler' => 'assignment_field_handler_edit_link',
    ),
  );

  $data['submission']['edit_link'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the submission.'),
      'handler' => 'submission_field_handler_edit_link',
    ),
  );

  $data['submission']['submission_status'] = array(
    'field' => array(
      'title' => t('Submission Status'),
      'help' => t('Whether a file has been uploaded.'),
      'handler' => 'submission_field_handler_submission_status',
    ),
  );

  $data['submission']['submission_time'] = array(
    'field' => array(
      'title' => t('Submission Time'),
      'help' => t('The time the submission was uploaded (if available).'),
      'handler' => 'submission_field_handler_submission_time',
    ),
  );

  $data['submission']['student_operations'] = array(
    'field' => array(
      'title' => t('Student Operations'),
      'help' => t('Provide relevant links for students based on the status.'),
      'handler' => 'submission_field_handler_student_operations',
    ),
  );

  $data['submission']['faculty_operations'] = array(
    'field' => array(
      'title' => t('Faculty Operations'),
      'help' => t('Provide relevant links for faculty based on the status.'),
      'handler' => 'submission_field_handler_faculty_operations',
    ),
  );

  $data['submission']['examiner_operations'] = array(
    'field' => array(
      'title' => t('Examiner Operations'),
      'help' => t('Provide relevant links for examiners based on the status.'),
      'handler' => 'submission_field_handler_examiner_operations',
    ),
  );

  $data['submission']['final_mark'] = array(
    'field' => array(
      'title' => t('Final Mark'),
      'help' => t('Final mark including deductions.'),
      'handler' => 'submission_field_handler_final_mark',
    ),
  );

  $data['views']['openac_assignments_add_area'] = array(
    'title' => t('Add Assignment Link'),
    'help' => t('Display links to add assignments'),
    'area' => array(
      'handler' => 'openac_assignments_handler_area_add_assignment',
    ),
  );
}
