<?php
/**
 * @file
 * Contains a Views field handler for student status on a submission.
 */

class submission_field_handler_submission_status extends views_handler_field_boolean {
  function construct() {
    parent::construct();

    $this->additional_fields['submission_id'] = 'submission_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function get_value($values, $field = NULL) {
    if ($field === NULL || $field == $this->field) {
      $submission = entity_load_single('assignment_submission', $this->get_value($values, 'submission_id'));
      $statuses = openac_assignments_submission_get_statuses($submission);
      return $statuses['submission'];
    }
    else {
      return parent::get_value($values, $field);
    }
  }

}
