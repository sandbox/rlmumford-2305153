<?php
/**
 * @files
 * Contains field definitions for the assigments system.
 */

 $t = get_t();
$fields = array();

// Assignment Description
$field_name = 'assignment_description';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'assignment',
    'bundle' => 'assignment',
    'label' => 'Description',
    'description' => $t('Assignment Instructions'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array('type' => 'text_textarea'),
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'type' => 'text_default',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'type' => 'text_default',
      ),
    ),
  ),
);