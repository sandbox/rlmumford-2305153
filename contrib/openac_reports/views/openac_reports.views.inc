<?php
/**
 * @file
 * Providing extra functionality for the Reports via views.
 */

/**
 * Implements hook_views_data_alter()
 */
function openac_reports_views_data_alter(&$data) {
  // Load views defined in info hooks.
  $mysql_views = openac_reports_get_mysql_views_info();

  // Build a views definition for each mysql view.
  foreach ($mysql_views as $name => $info) {
    $table_name = 'openac_reports__'.$name;
    $data[$table_name]['table']['group'] = $info['label'];
    $data[$table_name]['table']['base'] = array(
      'field' => $info['primary_field'],
      'title' => $info['label'],
      'help' => $info['description'],
      'weight' => -30,
    );

    foreach ($info['fields'] as $field_name => $definition) {
      $title = !empty($definition['title']) ? $definition['title'] : ucwords(strtr($field_name, array('_' => ' ')));

      $data[$table_name][$field_name] = array(
        'title' => $title,
        'help' => $definition['description'],
      );
      // Add the relevant handlers based on type.
      if (in_array($definition['type'], array('serial', 'int'))) {
        $data[$table_name][$field_name] += array(
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_numeric',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument_numeric',
          ),
        );
      }
      if (in_array($definition['type'], array('varchar', 'text'))) {
        $data[$table_name][$field_name] += array(
          'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE,
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
          'filter' => array(
            'handler' => 'views_handler_filter',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument',
          ),
        );
      }
      if (in_array($definition['type'], array('boolean'))) {
        $data[$table_name][$field_name] += array(
          'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE,
          ),
          'sort' => array(
            'handler' => 'views_handler_sort',
          ),
          'filter' => array(
            'handler' => 'views_handler_filter_boolean',
          ),
          'argument' => array(
            'handler' => 'views_handler_argument_numeric',
          ),
        );
      }

      // Add a relationship if appropriate.
      if (!empty($info['foreign keys'][$field_name])) {
        $foreign = $info['foreign keys'][$field_name];
        list($left_field, $base_field) = each($foreign['columns']);
        $data[$table_name][$field_name]['relationship'] = array(
          'base' => $foreign['table'],
          'base field' => $base_field,
          'left_field' => $left_field,
          'handler' => 'views_handler_relationship',
          'label' => ucwords($field_name),
        );

        foreach (entity_get_info() as $entity_type => $einfo) {
          if ($foreign['table'] == $einfo['base table']) {
            $data[$table_name][$field_name]['relationship']['entity_type'] = $entity_type;
            $data[$table_name][$field_name]['relationship']['label'] = $einfo['label'];
          }
        }
      }
    }
  }
}

/**
 * Implements hook_views_query_alter()
 */
function openac_reports_views_query_alter(&$view, &$query) {
  // Adjust for our 'none' options.
  if (in_array($view->name, array('party_selection', 'hub_dashboard', 'enrolled_dashboard', 'invoicing_dashboard'))) {
    foreach ($view->filter as &$filter) {
      // Ignore anything we're not fiddling with
      if (!in_array($filter->options['expose']['identifier'], array('hub', 'course', 'year'))) {
        continue;
      }

      // Check if we need to make an adjustment
      if ($filter->options['expose']['identifier'] == 'year') {
        if ($filter->value['value'] != 'none') {
          continue;
        }
        $value = 'none';
      }
      else {
        if (reset($filter->value) != 'none') {
          continue;
        }
        $value = $filter->value;
      }

      foreach ($query->where[$filter->options['group']]['conditions'] AS &$condition) {
        // Find our matching condition
        if ($condition['field'] == "{$filter->table_alias}.{$filter->field}" && $condition['value'] == $value && $condition['operator'] == $filter->operator) {
          $condition['operator'] = 'IS NULL';
          $condition['value'] = NULL;
        }
      }
    }
  }
}
