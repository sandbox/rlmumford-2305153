<?php
/**
 * @file
 * Define default page_manager pages for the openac discussion module.
 */

/**
 * Implements hook_default_page_manager_pages_alter().
 */
function openac_discussion_default_page_manager_pages_alter(&$pages) {
  // Add discussion areas.
  $page = &$pages['openac__my_course__classes'];
  $handler = &$page->default_handlers['page_openac__my_course__classes_panel_context'];
  $display = &$handler->conf['display'];

  $pane = new stdClass();
  $pane->pid = 'discussion';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'class:openac_comment';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'string_equal',
        'settings' => array(
          'operator' => '=',
          'value' => 'discussion',
          'case' => 0,
        ),
        'context' => 'argument_string_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'reply_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'argument_entity_id:class_1',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $display->content['discussion'] = $pane;
  $display->panels['contentmain'][] = 'discussion';

  $page = &$pages['openac__my_course'];
  $handler = &$page->default_handlers['page_openac__my_course_panel_context_2'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'discussion';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'course:openac_comment';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'string_equal',
        'settings' => array(
          'operator' => '=',
          'value' => 'discussion',
          'case' => 0,
        ),
        'context' => 'argument_string_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'context' => array(
      0 => 'relationship_course_current_from_party_1',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $display->content['discussion'] = $pane;
  $display->panels['contentmain'][] = 'discussion';

  $page = &$pages['openac__my_course__classes__section'];
  $handler = &$page->default_handlers['page_openac__my_course__classes__section_panel_context'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'discussion';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'class_section:openac_comment';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'reply_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'argument_entity_id:class_section_1',
    'override_title' => 1,
    'override_title_text' => 'Say Something',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $display->content['discussion'] = $pane;
  $display->panels['contentmain'][] = 'discussion';

  $page = &$pages['openac__my_classes__classes'];
  $handler = &$page->default_handlers['page_openac__my_classes__classes_panel_context'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'discussion';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'class:openac_comment';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'title',
    'formatter' => 'reply_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'argument_entity_id:class_1',
    'override_title' => 1,
    'override_title_text' => 'Say Something',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $display->content['discussion'] = $pane;
  $display->panels['contentmain'][] = 'discussion';
}
