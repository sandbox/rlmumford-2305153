<?php
/**
 * @file
 *   Contains field config for the openac discussion module.
 */
$t = get_t();

$fields = array();
$fields['openac_comment_body'] = array(
  'field' => array(
    'field_name' => 'openac_comment_body',
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => 'openac_comment_body',
    'entity_type' => 'reply',
    'bundle' => 'openac_comment',
    'label' => $t('Comment'),
  ),
);

$fields['course:openac_comment'] = array(
  'field' => array(
    'field_name' => 'openac_comment',
    'type' => 'reply',
    'cardinality' => 1,
    'settings' => array(
      'bundle' => 'openac_comment',
      'display' => REPLY_LIST_TREE,
      'form' => REPLY_FORM_PAGE_SAME,
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_comment',
    'entity_type' => 'course',
    'bundle' => 'course',
    'label' => $t('Discussion'),
  ),
);

$fields['class:openac_comment'] = array(
  'field' => array(
    'field_name' => 'openac_comment',
    'type' => 'reply',
    'cardinality' => 1,
    'settings' => array(
      'bundle' => 'openac_comment',
      'display' => REPLY_LIST_TREE,
      'form' => REPLY_FORM_PAGE_SAME,
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_comment',
    'entity_type' => 'class',
    'bundle' => 'class',
    'label' => $t('Discussion'),
  ),
);

$fields['section:openac_comment'] = array(
  'field' => array(
    'field_name' => 'openac_comment',
    'type' => 'reply',
    'cardinality' => 1,
    'settings' => array(
      'bundle' => 'openac_comment',
      'display' => REPLY_LIST_TREE,
      'form' => REPLY_FORM_PAGE_SAME,
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_comment',
    'entity_type' => 'class_section',
    'bundle' => 'class_section',
    'label' => $t('Discussion'),
  ),
);
