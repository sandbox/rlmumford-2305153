; Open Academy Application  make file for use with an existing OpenCRM site.
; Builds any requirements that are additional to OpenCRM.

api = 2
core = 7.x

; Utilities

projects[reply][subdir] = contrib
projects[reply][version] = 1.0-alpha1
