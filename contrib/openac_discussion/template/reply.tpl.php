<div id="reply-<?php print $elements['#entity']->id; ?>" class="<?php print $classes ?> boxed block-grey-alt">
  <div class="reply-submitted"><?php print $submitted; ?></div>
  <div class="reply-body"><?php print render($content) ?></div>
  <div class="reply-links"><?php print render($links) ?></div>
</div>
