<?php
/**
 * @file
 * Contains resource types for OpenAC Resources.
 */

/**
 * Implements hook_default_resource_type().
 */
function openac_resources_default_resource_type() {
  $types = array();

  $types['lecture'] = entity_import('resource_type', '{
    "type" : "lecture",
    "label" : "Lecture",
    "resources" : {
      "video" : {
        "label" : "Video",
        "machine_name" : "video",
        "type" : "video",
        "cardinality" : "1",
        "delete" : 0
      },
      "audio" : {
        "label" : "Audio",
        "machine_name" : "audio",
        "type" : "download",
        "cardinality" : "1",
        "delete" : 0
      },
      "notes" : {
        "label" : "Notes",
        "machine_name" : "notes",
        "type" : "download",
        "cardinality" : "1",
        "delete" : 0
      }
    },
    "data" : []
  }');
  $types['handout'] = entity_import('resource_type', '{
    "type" : "handout",
    "label" : "Handout",
    "resources" : { "handout" : {
        "label" : "Handout",
        "machine_name" : "handout",
        "type" : "download",
        "cardinality" : "1",
        "delete" : 0
      }
    },
    "data" : []
  }');
  $types['course_material'] = entity_import('resource_type', '{
    "type" : "course_material",
    "label" : "Course Material",
    "resources" : { "download" : {
        "label" : "Download",
        "machine_name" : "download",
        "type" : "download",
        "cardinality" : "1",
        "delete" : 0
      }
    },
    "data" : []
  }');

  return $types;
}
