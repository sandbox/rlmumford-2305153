<?php
/**
 * @file
 *   Stores field configuration.
 */

$fields = array();

$fields['module:openac_resources'] = array(
  'field' => array(
    'field_name' => 'openac_resources',
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'resource',
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_resources',
    'entity_type' => 'module',
    'bundle' => 'module',
    'label' => t('Resources'),
    'description' => t('Resources that should appear on the module page.'),
    'widget' => array(
      'type' => 'entityreference_view_widget',
      'settings' => array(
        'view' => 'openac_resources__entity_reference_selector|entityreference_view_widget_1',
        'pass_argument' => TRUE,
        'close_modal' => TRUE,
      ),
    ),
  ),
);

$fields['class:openac_resources'] = array(
  'field' => array(
    'field_name' => 'openac_resources',
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'resource',
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_resources',
    'entity_type' => 'class',
    'bundle' => 'class',
    'label' => t('Resources'),
    'description' => t('Resources that should appear on the class page.'),
    'widget' => array(
      'type' => 'entityreference_view_widget',
      'settings' => array(
        'view' => 'openac_resources__entity_reference_selector|entityreference_view_widget_1',
        'pass_argument' => TRUE,
        'close_modal' => TRUE,
      ),
    ),
  ),
);

$fields['class_section:openac_resources'] = array(
  'field' => array(
    'field_name' => 'openac_resources',
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'resource',
    ),
  ),
  'instance' => array(
    'field_name' => 'openac_resources',
    'entity_type' => 'class_section',
    'bundle' => 'class_section',
    'label' => t('Resources'),
    'description' => t('Resources that should appear on the session page.'),
    'widget' => array(
      'type' => 'entityreference_view_widget',
      'settings' => array(
        'view' => 'openac_resources__entity_reference_selector|entityreference_view_widget_1',
        'pass_argument' => TRUE,
        'close_modal' => TRUE,
      ),
    ),
  ),
);
