; Open Academy make file for use with an existing OpenCRM site.
; Builds any requirements that are additional to OpenCRM.

api = 2
core = 7.x

; Utilities

projects[inline_entity_form][subdir] = contrib
projects[inline_entity_form][version] = 1.8

projects[entityreference_view_widget][subdir] = contrib
projects[entityreference_view_widget][version] = 2.0-rc7

projects[resource_library][subdir] = contrib
projects[resource_library][type] = module
projects[resource_library][download][url] = https://git.drupal.org/sandbox/rlmumford/2054565.git
projects[resource_library][download][revision] = 34e88e9c6c56f155b62a9b9393f45a8c9bb253ce
projects[resource_library][download][branch] = 7.x-1.x
