<?php
/**
 * @file
 *   Administration functions for OpenAC Resources.
 */

/**
 * Main Page Wrapper for Class Resource Page.
 */
function openac_resources_add_class_resource_page_wrapper($type, $class, $js) {
  $form_id = 'resource_form';
  $resource = entity_create('resource', array(
    'type' => $type,
    'resource_classes' => array(
      LANGUAGE_NONE => array(
        0 => array('target_id' => $class->id),
      ),
    ),
  ));

  $resource->openac_resources_auto_connect = array(
    'class' => array($class->id),
  );

  if (!empty($_GET['resource_class_sections']) && is_numeric($_GET['resource_class_sections'])) {
    $section = $_GET['resource_class_sections'];
    $resource->resource_class_sections[LANGUAGE_NONE][]['target_id'] = $section;
    $resource->openac_resources_auto_connect['class_section'] = array($section);
    unset($resource->openac_resources_auto_connect['class']);
  }

  if (!$js) {
    return drupal_get_form($form_id, $resource);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Add Resource',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($resource),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}
