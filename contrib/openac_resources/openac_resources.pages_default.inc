<?php
/**
 * @file
 * Define default page_manager pages for the openac module.
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function openac_resources_default_page_manager_pages() {
  $export = array();

  // Scan directory for any .page files
  $files = file_scan_directory(dirname(__FILE__) . '/page_manager_pages', '/\.page$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$page->name] = $page;
    }
  }

  return $export;
}

/**
 * Implements hook_default_page_manager_pages_alter().
 */
function openac_resources_default_page_manager_pages_alter(&$pages) {
  // Add resources lists.
  $page = &$pages['openac__my_course__classes'];
  $handler = &$page->default_handlers['page_openac__my_course__classes_panel_context'];
  $display = &$handler->conf['display'];

  $pane = new stdClass();
  $pane->pid = 'resourse_list';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openac_resources__class_resources-display_pane';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'string_equal',
        'settings' => array(
          'operator' => '=',
          'value' => 'resources',
          'case' => 0,
        ),
        'context' => 'argument_string_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:class_1',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $display->content['resourse_list'] = $pane;
  $display->panels['contentmain'][] = 'resourse_list';

  $page = &$pages['openac__my_course'];
  $handler = &$page->default_handlers['page_openac__my_course_panel_context_2'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'resources_list';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openac_resources__list-resources_by_course';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'string_equal',
        'settings' => array(
          'operator' => '=',
          'value' => 'resources',
          'case' => 0,
        ),
        'context' => 'argument_string_1',
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'context' => array(
      0 => 'relationship_course_current_from_party_1',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $display->content['resources_list'] = $pane;
  $display->panels['contentmain'][] = 'resources_list';

  $page = &$pages['openac__my_course__classes__section'];
  $handler = &$page->default_handlers['page_openac__my_course__classes__section_panel_context'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'resources_list';
  $pane->panel = 'top';
  $pane->type = 'views_panes';
  $pane->subtype = 'openac_resources__section_resources-display_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:class_section_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $display->content['resources_list'] = $pane;
  $display->panels['top'][] = 'resources_list';

  $page = &$pages['openac__my_classes__classes'];
  $handler = &$page->default_handlers['page_openac__my_classes__classes_panel_context'];
  $display = &$handler->conf['display'];
  $pane = new stdClass();
  $pane->pid = 'resources_list';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openac_resources__class_resources-display_pane';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_entity_id:class_1',
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $display->content['resources_list'] = $pane;
  $display->panels['contentmain'][] = 'resources_list';
}
