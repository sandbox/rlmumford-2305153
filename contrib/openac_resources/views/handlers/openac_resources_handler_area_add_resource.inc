<?php
/**
 * @file
 * Includes views area handler for adding resource links.
 */

/**
 * OpenAC Resources area handler.
 */
class openac_resources_handler_area_add_resource extends views_handler_area {

  /**
   * Render the area.
   */
  function render($empty = FALSE) {
    $link = FALSE;
    $link_id = NULL;
    if (!empty($this->view->argument['resource_classes_target_id']->value)) {
      $link = 'class';
      $link_id = $this->view->argument['resource_classes_target_id']->value[0];
      if (!empty($this->view->argument['resource_class_sections_target_id']->value)) {
        $section_id = $this->view->argument['resource_class_sections_target_id']->value[0];
      }
    }

    if (!empty($this->view->argument['id'])) {
      $arg = $this->view->argument['id'];
      $table = $arg->table;

      if ($table == 'class') {
        $link = 'class';
        $link_id = $arg->value[0];
      }
      if ($table == 'class_section') {
        $link = 'section';
        $section_id = $arg->value[0];
      }
    }

    $resource_info = entity_get_info('resource');
    $types = $resource_info['bundles'];
    $links = array();
    switch ($link) {
      case 'class':
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        foreach ($types as $bundle => $type) {
          if (in_array($bundle, array('audio'))) {
            continue;
          }
          $links[$bundle] = array(
            'href' => 'admin/college/admin/classes/class/'.$link_id.'/add-resource/'.$bundle.'/nojs',
            'title' => t('Add !label', array('!label' => $type['label'])),
            'localized_options' => array(
              'attributes' => array(
                'class' => array('ctools-use-modal'),
              ),
            ),
          );

          if (!empty($section_id)) {
            $links[$bundle]['localized_options']['query'] = array(
              'resource_class_sections' => $section_id,
            );
          }
        }
        break;
      default:
        ctools_include('ajax');
        ctools_include('modal');
        ctools_modal_add_js();
        foreach ($types as $bundle => $type) {
          if (in_array($bundle, array('audio'))) {
            continue;
          }
          $links[$bundle] = array(
            'href' => 'resource/add/'.$bundle.'/nojs',
            'title' => t('Add !label', array('!label' => $type['label'])),
            'localized_options' => array(
              'attributes' => array(
                'class' => array('ctools-use-modal'),
              ),
            ),
          );

          if (!empty($section_id)) {
            $links[$bundle]['localized_options']['query']['resource_class_sections'] = $section_id;
          }
        }
        break;
    }

    $output = array(
      '#prefix' => '<ul class="action-links">',
      '#suffix' => '</ul>',
    );
    foreach ($links as $key => $link) {
      $output[$key] = array(
        '#theme' => 'menu_local_action',
        '#link' => $link,
      );
    }

    return render($output);
  }

}
