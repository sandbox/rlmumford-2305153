<?php
/**
 * @file
 *  Views integration for openac resources module.
 */

/**
 * Implement hook_views_data()
 */
function openac_resources_views_data() {
  $data = array();
  $data['views']['openac_resources_add_area'] = array(
    'title' => t('Add Resource Links'),
    'help' => t('Display links to add resources'),
    'area' => array(
      'handler' => 'openac_resources_handler_area_add_resource',
    ),
  );

  return $data;
}
