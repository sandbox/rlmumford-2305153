<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class openac_module_handler_edit_link_field extends openac_module_handler_link_field {
  function render($values) {
    //Creating a dummy wtc_module to check access against
    $dummy_module = new stdClass;
    if (!openac_module_access('edit', $dummy_module)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $id = $values->{$this->aliases['id']};

    return l($text, 'admin/college/admin/modules/module/' . $id . '/edit');
  }
}
