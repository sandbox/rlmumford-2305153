<?php
/**
 * @file
 * Definition of openac_module_handler_argument_managed_modules.
 */

/**
 * Argument handler to determine if a module is managed by a given party
 * id. This checks against the module_tutor field and optionally the
 * field_module_teaching_assistant field.
 *
 * @ingroup views_argument_handlers
 */
class openac_module_handler_argument_managed_modules extends views_handler_argument_numeric {

  /**
   * Define an option for whether or not to include teaching assistants
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['include_teaching_assistants'] = array('default' => TRUE, 'bool' => TRUE);

    return $options;
  }

  /**
   * Remove break_phrase from the options form as we can't handle multiple
   * party ids.
   *
   * @overrides views_handler_argument_numeric::options_form()
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['break_phrase']);

    $form['include_teaching_assistants'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include Teaching Assistants'),
      '#description' => t('If selected, include modules for which the party is a teaching assistant.'),
      '#default_value' => !empty($this->options['include_teaching_assistants']),
      '#fieldset' => 'more',
    );
  }

  /**
   * Work out the query
   */
  function query($group_by = FALSE) {
    $this->ensure_my_table();

    // Calculate id values
    $module_ids = array();

    // Modules the party tutors
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'module')
      ->fieldCondition('module_tutor', 'target_id', $this->argument)
      ->execute();

    if (!empty($entities['module'])) {
      $module_ids = array_keys($entities['module']);
    }

    // Modules the party is a teaching assistant
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'module')
      ->fieldCondition('module_assistants', 'target_id', $this->argument)
      ->execute();

    if (!empty($entities['module'])) {
      $module_ids += array_keys($entities['module']);
    }

    $this->value = $module_ids;

    $placeholder = $this->placeholder();
    $null_check = empty($this->options['not']) ? '' : "OR $this->table_alias.$this->real_field IS NULL";

    if (count($this->value) > 1) {
      $operator = empty($this->options['not']) ? 'IN' : 'NOT IN';
      $this->query->add_where_expression(0, "$this->table_alias.$this->real_field $operator($placeholder) $null_check", array($placeholder => $this->value));
    }
    else {
      $operator = empty($this->options['not']) ? '=' : '!=';
      $this->query->add_where_expression(0, "$this->table_alias.$this->real_field $operator $placeholder $null_check", array($placeholder => $this->value));
    }
  }

}
