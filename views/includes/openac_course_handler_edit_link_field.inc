<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying edit links
 * as fields
 */


class openac_course_handler_edit_link_field extends openac_course_handler_link_field {

  function render($values) {
    //Creating a dummy wtc_course to check access against
    $dummy_course = new stdClass();
    if (!openac_course_access('edit', $dummy_course)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
    $course_id = $values->{$this->aliases['id']};

    return l($text, 'admin/college/admin/courses/course/' . $course_id . '/edit');
  }
}
