<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class openac_course_handler_delete_link_field extends openac_course_handler_link_field {

  function render($values) {
    //Creating a dummy course to check access against
    $dummy_course = new stdClass();
    if (!openac_course_access('edit', $dummy_course)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $course_id = $values->{$this->aliases['id']};

    return l($text, 'admin/college/admin/courses/course/' . $course_id . '/delete');
  }
}
