<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying view links
 * as fields
 */


class openac_class_handler_view_link_field extends openac_class_handler_link_field {
  function render($values) {
    //Creating a dummy wtc_class to check access against
    $dummy_class = new stdClass;
    if (!openac_class_access('view', $dummy_class)) {
      return;
    }

    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');
    $id = $values->{$this->aliases['id']};

    return l($text, 'admin/college/admin/classes/class/' . $id . '/view');
  }
}
