<?php
/**
 * @file
 * Contains a Views field handler to take care of displaying links to entities
 * as fields.
 */

class openac_class_registration_handler_link_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['class'] = 'class';
    $this->additional_fields['student'] = 'student';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['text'] = array('default' => 'edit', 'translatable' => TRUE);
    $options['modal'] = array('default' => 0);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );

    $form['modal'] = array(
      '#type' => 'checkbox',
      '#title' => t('Open this link in a modal.'),
      '#default_value' => $this->options['modal'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');
    $class_id = $values->{$this->aliases['class']};
    $party_id = $values->{$this->aliases['student']};

    if ($this->options['modal']) {
      ctools_include('ajax');
      ctools_include('modal');
      ctools_modal_add_js();

      return l($text, 'admin/college/admin/classes/class/' . $class_id . '/student/'.$party_id.'/nojs', array('html' => TRUE, 'attributes' => array('class' => array('ctools-use-modal'))));
    }
    else {
      return l($text, 'admin/college/admin/classes/class/' . $class_id . '/student/'.$party_id);
    }
  }
}
