<?php
/**
 * @file
 * Includes views area handler for adding section links.
 */

/**
 * OpenAC section add link area handler.
 */
class openac_handler_area_add_section extends views_handler_area {

  /**
   * Render the area.
   */
  function render($empty = FALSE) {
    $link = FALSE;
    $link_id = NULL;
    if (!empty($this->view->argument['class']->value)) {
      $link = 'class';
      $link_id = $this->view->argument['class']->value[0];
    }
    $links = array();
    switch ($link) {
      case 'class':
        //ctools_include('ajax');
        //ctools_include('modal');
        //ctools_modal_add_js();
        $links['add_section'] = array(
          'href' => 'admin/college/admin/classes/class/'.$link_id.'/section/add',
          'title' => t('Add Session'),
        );
        break;
      default:
        break;
    }

    $output = array(
      '#prefix' => '<ul class="action-links">',
      '#suffix' => '</ul>',
    );
    foreach ($links as $key => $link) {
      $output[$key] = array(
        '#theme' => 'menu_local_action',
        '#link' => $link,
      );
    }

    return render($output);
  }

}
