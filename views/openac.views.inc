<?php
/**
 * @file
 * Extended views integrations.
 */

/**
 * Implements hook_views_data_alter().
 */
function openac_views_data_alter(&$data) {
  // Course Extras
  $data['course']['edit_course'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      'handler' => 'openac_course_handler_edit_link_field',
    ),
  );
  $data['course']['delete_course'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      'handler' => 'openac_course_handler_delete_link_field',
    ),
  );

  // Module Extras
  $data['module']['edit_module'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the module.'),
      'handler' => 'openac_module_handler_edit_link_field',
    ),
  );
  $data['module']['delete_module'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the module.'),
      'handler' => 'openac_module_handler_delete_link_field',
    ),
  );

  // Add an extra argument on the module id to accept a party
  $data['module']['managed_modules'] = array(
    'argument' => array(
      'title' => t('Managed Modules'),
      'help' => t('Only show the modules a given party can manage.'),
      'real field' => 'id',
      'handler' => 'openac_module_handler_argument_managed_modules',
    ),
  );

  // Class Extras
  $data['class']['edit_class'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the class.'),
      'handler' => 'openac_class_handler_edit_link_field',
    ),
  );
  $data['class']['view_class'] = array(
    'field' => array(
      'title' => t('View Link'),
      'help' => t('Provide a link to view the class.'),
      'handler' => 'openac_class_handler_view_link_field',
    ),
  );
  $data['class']['manage_class'] = array(
    'field' => array(
      'title' => t('Manage Link'),
      'help' => t('Provide a link to manage the class.'),
      'handler' => 'openac_class_handler_manage_link_field',
    ),
  );
  $data['class']['delete_class'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the class.'),
      'handler' => 'openac_class_handler_delete_link_field',
    ),
  );

  // Course Registration
  $data['course_registration']['edit_link'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Add a link to edit the course registration.'),
      'handler' => 'openac_course_registration_handler_link_field',
    ),
  );
  
  // Class Registration
  $data['class_registration']['edit_link'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Add a link to edit the class registration.'),
      'handler' => 'openac_class_registration_handler_link_field',
    ),
  );

  // Areas
  $data['views']['openac_add_section_area'] = array(
    'title' => t('Add Session Link'),
    'help' => t('Display link to add a session'),
    'area' => array(
      'handler' => 'openac_handler_area_add_section',
    ),
  );
}
