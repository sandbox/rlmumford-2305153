<?php
/**
 * @file Contains rules integration hooks.
 */

/**
 * Implements hook_rules_action_info().
 */
function openac_rules_action_info() {
  $actions = array();
  $actions['openac_course_register'] = array(
    'label' => t('Enroll a Student on a Course'),
    'base' => 'openac_rules_action_course_register',
    'group' => t('OpenCRM Academy'),
    'parameter' => array(
      'student' => array(
        'type' => 'party',
        'label' => t('Student'),
      ),
      'course' => array(
        'type' => 'course',
        'label' => t('Course'),
      ),
      'save' => array(
        'type' => 'boolean',
        'label' => t('Immediately Save Registration?'),
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'registration' => array(
        'type' => 'course_registration',
        'label' => t('Registration'),
      ),
    ),
  );

  $actions['openac_class_register'] = array(
    'label' => t('Enroll a Student on a Class'),
    'base' => 'openac_rules_action_class_register',
    'group' => t('OpenCRM Academy'),
    'parameter' => array(
      'student' => array(
        'type' => 'party',
        'label' => t('Student'),
      ),
      'course' => array(
        'type' => 'course',
        'label' => t('Course'),
      ),
      'class' => array(
        'type' => 'class',
        'label' => t('Class'),
      ),
      'save' => array(
        'type' => 'boolean',
        'label' => t('Immediately Save Registration?'),
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'registration' => array(
        'type' => 'class_registration',
        'label' => t('Registration'),
      ),
    ),
  );
  return $actions;
}

/**
 * Action: Register a student on a course.
 */
function openac_rules_action_course_register($student, $course, $save) {
  $reg = CourseRegistration::register($student, $course);
  if ($save) {
    $reg->save();
  }
  return array('registration' => $reg);
}

/**
 * Action: Register a student on a class.
 */
function openac_rules_action_class_register($student, $course, $class, $save) {
  $reg = ClassRegistration::register($class, $student, $course);
  if ($save) {
    $reg->save();
  }
  return array('registration' => $reg);
}
