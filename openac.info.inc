<?php
/**
 * @file
 * Entity Metadata Information.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function openac_entity_property_info_alter(&$info) {
  $course_props = &$info['course']['properties'];
  $course_props['start'] = array(
    'type' => 'date',
    'label' => t('Start Date'),
    'schema field' => 'start',
    'description' => t('The date the @lcourse starts.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  );
  $course_props['end'] = array(
    'type' => 'date',
    'label' => t('End Date'),
    'schema field' => 'end',
    'description' => t('The date the @lcourse ends.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  );

  $module_props = &$info['module']['properties'];
  $module_props['code']['label'] = t('Code');

  $class_props = &$info['class']['properties'];
  $class_props['module']['type'] = 'module';
  $class_props['module']['required'] = TRUE;
  $class_props['module']['options list'] = 'openac_module_options_list';
  $class_props['start']['type'] = 'date';
  $class_props['status']['options list'] = 'openac_class_status_options_list';

  $class_section_props = &$info['class_section']['properties'];
  $class_section_props['class']['type'] = 'class';
  $class_section_props['class']['required'] = TRUE;
  $class_section_props['publish_date']['type'] = 'date';

  $class_reg_props = &$info['class_registration']['properties'];
  $class_reg_props['student']['type'] = 'party';
  $class_reg_props['student']['description'] = t('The student registered');
  $class_reg_props['student']['required'] = TRUE;
  $class_reg_props['class']['type'] = 'class';
  $class_reg_props['class']['description'] = t('The class registered to');
  $class_reg_props['class']['required'] = TRUE;
  $class_reg_props['course']['type'] = 'course';
  $class_reg_props['course']['description'] = t('The @lcourse this registration concerns.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));
  $class_reg_props['course']['required'] = TRUE;

  $course_reg_props = &$info['course_registration']['properties'];
  $course_reg_props['student']['type'] = 'party';
  $course_reg_props['student']['description'] = t('The student registered');
  $course_reg_props['student']['required'] = TRUE;
  $course_reg_props['course']['type'] = 'course';
  $course_reg_props['course']['description'] = t('The @lcourse registered to', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));
  $course_reg_props['course']['required'] = TRUE;
  $course_reg_props['status']['options list'] = 'openac_course_registration_status_options_list';

  $party_props = &$info['party']['properties'];
  $party_props['courses'] = array(
    'type' => 'list<course>',
    'label' => openac_entity_type_label('course', TRUE),
    'description' => t('List of the @lcourses the party is registered to.', array('@lcourses' => openac_entity_type_label('course', TRUE, TRUE))),
    'getter callback' => 'openac_party_property_get',
  );
  $party_props['classes'] = array(
    'type' => 'list<class>',
    'label' => t('Classes'),
    'description' => t('List of the classes the party is registered to.'),
    'getter callback' => 'openac_party_property_get',
  );
  $party_props['openac_locations'] = array(
    'type' => 'list<opencrm_location>',
    'label' => t('Locations'),
    'description' => t('The locations the party is registered to study at.'),
    'getter callback' => 'openac_party_property_get',
  );
}
