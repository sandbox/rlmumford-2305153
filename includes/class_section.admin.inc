<?php
/**
 * @file
 * Class Section editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class ClassSectionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $class_entity_info = entity_get_info('class');
    $class_path = $class_entity_info['admin ui']['path'];

    $id_count = count(explode('/', $class_path));
    $class_wildcard = isset($class_entity_info['admin ui']['menu wildcard']) ? $class_entity_info['admin ui']['menu wildcard'] : '%class';
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    // Add a class.
    $items[$class_path . '/class/' . $class_wildcard . '/section/add'] = array(
      'title' => t('Add Session'),
      'page callback' => 'openac_class_section_add_page_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'openac_class_section_access',
      'access arguments' => array('add', NULL, NULL, $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/class_section.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Loading and editing wtc_class entities
    $items[$class_path . '/class/' . $class_wildcard . '/section/' . $wildcard] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('class_section', $id_count + 3),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_class_section_form', $id_count + 3, 'edit'),
      'access callback' => 'openac_class_section_access',
      'access arguments' => array('edit', $id_count + 3, NULL, $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/class_section.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$class_path . '/class/' . $class_wildcard . '/section/' . $wildcard . '/edit'] = array(
      'title' => t('Edit'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );

    $items[$class_path . '/class/' . $class_wildcard . '/section/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_class_section_delete_form', $id_count + 3),
      'access callback' => 'openac_class_section_access',
      'access arguments' => array('edit', $id_count + 3, NULL, $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'includes/class_section.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    return $items;
  }
}

/**
 * Page callback for the section page.
 */
function openac_class_section_add_page_wrapper($class, $js = FALSE) {
  $form_id = 'openac_class_section_form';

  $section = entity_create('class_section', array(
    'class' => $class->id,
  ));

  if (!$js) {
    return drupal_get_form($form_id, $section, 'create');
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Add Session',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($section, 'create'),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}

/**
 * Form callback: create or edit a section.
 *
 * @param $section
 *   The OpenAcSection object to edit or for creation an empty section object
 *     with only the class defined.
 */
function openac_class_section_form($form, &$form_state, $section = NULL, $op = 'edit') {
  $form_state['class_section'] = $form['#class_section'] = $section;

  // Add the default field elements.
  $form['number'] = array(
    '#type' => 'textfield',
    '#title' => t('#'),
    '#default_value' => isset($section->number) ? $section->number : '',
    '#maxlength' => 16,
    '#size' => 4,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($section->title) ? $section->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['publish_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Publish Date'),
    '#description' => t('The date that this session an its resources should become available to students.'),
    '#date_format' => "d/m/Y H:i",
    '#default_value' => isset($section->publish_date) ? date("Y-m-d H:i", $section->publish_date) : '',
    '#required' => TRUE,
    '#weight' => -4,
  );

  // Add the field related form elements.
  field_attach_form('class_section', $section, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('openac_class_section_form_submit'),
    '#suffix' => (!empty($section->id) || empty($section->class)) ? '' : l(t('Cancel'), 'admin/college/classes/class/' . $section->class . '/manage'),
  );

  if (!empty($section->id)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#suffix' => l(t('Cancel'), 'admin/college/classes/class/' . $section->class . '/manage'),
      '#submit' => $submit + array('openac_class_section_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'openac_class_section_form_validate';
  return $form;
}


/**
 * Form API validate callback for the section form
 */
function openac_class_section_form_validate(&$form, &$form_state) {
  $section = $form_state['class_section'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('class_section', $section, $form, $form_state);
}


/**
 * Form API submit callback for the class form.
 *
 * @todo remove hard-coded link
 */
function openac_class_section_form_submit(&$form, &$form_state) {
  // Prepare form state values.
  $vals = &$form_state['values'];
  $vals['publish_date'] = DateTime::createFromFormat("Y-m-d H:i", $vals['publish_date'])
    ->getTimestamp();

  $section = entity_ui_controller('class_section')->entityFormSubmitBuildEntity($form, $form_state);
  $section->save();
  $form_state['redirect'] = 'admin/college/admin/classes/class/' . $section->class . '/manage';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function openac_class_section_form_submit_delete(&$form, &$form_state) {
  $section = $form_state['class_section'];
  $form_state['redirect'] = 'admin/college/admin/classes/class/' . $section->class . '/section/' . $section->id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a section.
 *
 * @param $section
 *   The section to delete
 *
 * @see confirm_form()
 */
function openac_class_section_delete_form($form, &$form_state, $section) {
  $form_state['class_section'] = $section;

  $form['#submit'][] = 'openac_class_section_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete %name?', array('%name' => $section->defaultLabel())),
    'admin/college/admin/classes/class/'.$section->class . '/manage',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for class_delete_form
 */
function openac_class_section_delete_form_submit($form, &$form_state) {
  $section = $form_state['class_section'];
  $section->delete();

  drupal_set_message(t('%name has been deleted.', array('%name' => $section->title)));
  watchdog('class_section', 'Deleted %name.', array('%name' => $section->title));

  $form_state['redirect'] = 'admin/college/admin/classes/class/'.$section->class . '/manage';
}
