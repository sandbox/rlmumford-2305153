<?php
/**
 * @file
 * Contains ClassRegistration.
 */

/**
 * The class used for Class Registration entities
 */
class ClassRegistration extends OpenAcEntity {

  /**
   * The class.
   * @var OpenAcClass
   */
  protected $class_entity;

  /**
   * The party taking this class.
   * @var Party.
   */
  protected $student_entity;

  public function __construct($values = array()) {
    parent::__construct($values, 'class_registration');
  }

  public function defaultLabel() {
    return $this->getStudent()->label() . ' - ' . $this->getClass()->label();
  }

  public function getStudent() {
    if (empty($this->student_entity)) {
      $this->student_entity = entity_load_single('party', $this->student);
    }

    return $this->student_entity;
  }

  public function getClass() {
    if (empty($this->class_entity)) {
      $this->class_entity = entity_load_single('class', $this->class);
    }

    return $this->class_entity;
  }

  public static function register(OpenAcClass $class, Party $student, Course $course) {
    return entity_get_controller($this->entity_type)->createForRegistration($class, $student, $course);
  }
}

/**
 * Controller class
 */
class ClassRegistrationController extends OpenAcEntityController {

  /**
   * Create a registration.
   */
  public function createForRegistration(OpenAcClass $class, Party $student, Course $course) {
    $values = array(
      'student' => $student->pid,
      'class' => $class->id,
      'course' => $course->id,
    );
    return $this->create($values);
  }

}
