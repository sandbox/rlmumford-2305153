<?php
/**
 * @file
 * Contains Course.
 */

/**
 * The class used for Course entities
 */
class Course extends OpenAcEntity {

  /**
   * Relations to modules.
   * @var array
   */
  protected $modules = array();

  /**
   * Removed modules.
   * @var array
   */
  protected $removed_modules = array();

  /**
   * Whether or not the modules have been loaded.
   * @var bool.
   */
  protected $modules_loaded = FALSE;


  public function __construct($values = array()) {
    parent::__construct($values, 'course');
  }

  public function defaultLabel() {
    return $this->title . " (" . date('Y', $this->start) . ")";
  }

  protected function defaultUri() {
    return array('path' => 'course/' . $this->course_id);
  }

  /**
   * Get the module relations from this entity.
   *
   * @return array
   *   An array of loaded relations.
   *
   * @todo: Consider moving actual logic to a controller.
   * @todo: Consider keying the modules array by module id.
   */
  public function getModuleRelations() {
    // If we've already loaded the modules or the course isn't save yet skip.
    if ($this->modules_loaded || empty($this->id)) {
      return $this->modules;
    }

    $this->modules = array();
    $query = new RelationQuery('course', $this->id, 0);
    $query->entityCondition('bundle', 'openac_course_module');
    $result = $query->execute();

    if (!empty($result)) {
      $this->modules += entity_load('relation', array_keys($result));
    }

    $this->modules_loaded = TRUE;
    return $this->modules;
  }

  /**
   * Add a module to this course.
   *
   * @param int $module
   *   The module id.
   * @param bool $required
   *   Whether or not the module is required.
   */
  public function addModule($module, $required = TRUE) {
    // Find a new array key.
    $key = 'new'; $i = 0;
    while (isset($this->modules[$key])) {
      $key = "new_$i"; $i++;
    }

    $endpoints = array(
      array('entity_type' => 'course', 'entity_id' => !empty($this->id) ? $this->id : FALSE),
      array('entity_type' => 'module', 'entity_id' => $module),
    );
    $rel = relation_create('openac_course_module', $endpoints);
    $rel->course_module_required[LANGUAGE_NONE][0]['value'] = (int) $required;
    $this->modules[$key] = $rel;
  }

  /**
   * Remove a module.
   *
   * Removes a course/module relationship. Removal will not be saved until the
   * course is saved.
   */
  public function removeModule($key) {
    $this->removed_modules[$key] = $this->modules[$key];
    unset($this->modules[$key]);
  }

  /**
   * Act just after this has been saved.
   */
  public function postSave($update) {
    // Remove any modules that have been removed.
    foreach ($this->removed_modules as $relation) {
      entity_delete('relation', $relation->rid);
    }

    // Save any module relations that are still present.
    foreach ($this->modules as $relation) {
      if (empty($relation->rid)) {
        if (empty($relation->endpoints[LANGUAGE_NONE][0]['entity_id'])) {
          $relation->endpoints[LANGUAGE_NONE][0]['entity_id'] = $this->id;
        }
      }
    
      entity_save('relation', $relation);
      unset($relation->is_new);
    }
  }
}
