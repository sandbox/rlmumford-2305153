<?php
/**
 * @file
 * Contains administration UI function for course registrations.
 */

/**
 * Page callback for the add student page.
 */
function openac_course_registration_edit_page_wrapper($course, $student, $js = FALSE) {
  $form_id = 'openac_course_register_form';
  if (!$js) {
    return drupal_get_form($form_id, $course, $student);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Edit Registration',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($course, $student),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}

/**
 * Form callback to register a student to a course.
 */
function openac_course_register_form($form, &$form_state, $course = FALSE, $student = FALSE) {

  if (!empty($course) && !empty($student)) {
    $id = db_select('course_registration', 'cr')
      ->condition('student', $student->pid)
      ->condition('course', $course->id)
      ->fields('cr', array('id'))
      ->execute()
      ->fetchField();
    $form['#registration'] =
      $form_state['registration'] =
      entity_load_single('course_registration', $id);
  }

  if (empty($form_state['registration'])) {
    $form['#registration'] =
      $form_state['registration'] =
      entity_create('course_registration', array());
  }

  $course_options = openac_course_options_list();
  $form['course'] = array(
    '#title' => openac_entity_type_label('course'),
    '#required' => TRUE,
    '#description' => t('The @lcourse to register a student for.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#type' => 'select',
    '#options' => $course_options,
  );
  if (!empty($course)) {
    $form['course']['#disabled'] = TRUE;
    $form['course']['#default_value'] = $course->id;
  }

  $form['student'] = array(
    '#title' => t('Student'),
    '#required' => TRUE,
    '#description' => t('The student to register'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'party_extras/autocomplete/party_indiv',
  );
  if (!empty($student)) {
    $form['student']['#disabled'] = TRUE;
    $form['student']['#default_value'] = "{$student->label} ({$student->pid})";
  }

  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#required' => TRUE,
    '#description' => t('The status of this registration.'),
    '#options' => openac_course_registration_status_options_list(),
    '#default_value' => 'active',
  );

  field_attach_form('course_registration', $form['#registration'], $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($form_state['registration']->id) ? t('Register') : t('Update'),
  );

  return $form;
}

/**
 * Form Validation Callback to Register students.
 */
function openac_course_register_form_validate($form, &$form_state) {
  $regex = '/\((?<id>[0-9]+)\)/';

  // Get and check the party id.
  if (empty($form['#registration']->id)) {
    preg_match($regex, $form_state['values']['student'], $matches);
    $party_id = !empty($matches['id']) ? $matches['id'] : FALSE;
    if (!is_numeric($party_id) || $party_id < 1) {
      form_error($form['student'], t('Invalid Student: Could not extract ID'));
    }
    else if (!party_load($party_id)) {
      form_error($form['student'], t('Invalid Student: Student does not exist'));
    }
    else if (!party_hat_has_hat(party_load($party_id), 'openac_enrolled')) {
      form_error($form['student'], t('Invalid Student: That student is not currently enrolled'));
    }
  }

  // If we are making a new registration check it does not already exist.
  $course_id = $form_state['values']['course'];
  if (is_numeric($party_id) && is_numeric($course_id) && empty($form_state['registration']->id)) {
    $class_registered = db_select('course_registration', 'cr')
      ->condition('student', $party_id)
      ->condition('course', $course_id)
      ->countQuery()
      ->execute()
      ->fetchField();
    if ($class_registered) {
      form_error($form, t('That student is already registered for this @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
    }
  }

  field_attach_form_validate('course_registration', $form['#registration'], $form, $form_state);
}

/**
 * Form Submit Callback to Register Students to classes.
 */
function openac_course_register_form_submit($form, &$form_state) {
  $regex = '/\((?<id>[0-9]+)\)/';

  // Get the student id.
  if (empty($form_state['registration']->id)) {
    preg_match($regex, $form_state['values']['student'], $matches);
    $party_id = !empty($matches['id']) ? $matches['id'] : FALSE;
  }

  // Get the course id.
  $course_id = $form_state['values']['course'];

  $registration = $form_state['registration'];
  if (isset($party_id)) {
    $student = party_load($party_id);
    party_hat_hats_assign($student, array('openac_enrolled'));

    $registration->student = $party_id;
    $registration->course = $course_id;
  }
  $registration->status = $form_state['values']['status'];

  field_attach_submit('course_registration', $registration, $form, $form_state);
  $registration->save();

  drupal_set_message(t('Student successfully registered to the @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
}

