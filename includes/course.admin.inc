<?php
/**
 * @file
 * Course editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class CourseUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => openac_entity_type_label('course', TRUE),
      'description' => t('Add, edit and update @lcourses.', array('@lcourses' => openac_entity_type_label('course', TRUE, TRUE))),
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'position' => 'left',
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => t('Add a @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      'description' => t('Add a new @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      'page callback'  => 'drupal_get_form',
      'page arguments' => array('openac_course_form', NULL, 'add'),
      'access callback'  => 'openac_course_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 20,
      'file' => 'includes/course.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Loading and editing wtc_course entities
    $items[$this->path . '/course/' . $wildcard] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('course', $id_count + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_course_form', $id_count + 1, 'edit'),
      'access callback' => 'openac_course_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/course.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/course/' . $wildcard . '/edit'] = array(
      'title' => t('Edit'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    $items[$this->path . '/course/' . $wildcard . '/add-student'] = array(
      'title' => t('Add Student'),
      'page callback' => 'openac_course_add_student_page_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'openac_course_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'includes/course.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/course/' . $wildcard . '/add-student/%ctools_js'] = array(
      'page arguments' => array($id_count + 1, $id_count + 3),
    ) + $items[$this->path . '/course/' . $wildcard . '/add-student'];
    $items[$this->path . '/course/' . $wildcard . '/delete'] = array(
      'title' => t('Delete'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_course_delete_form', $id_count + 1),
      'access callback' => 'openac_course_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'includes/course.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    $items[$this->path . '/course/' . $wildcard . '/registration/%party'] = array(
      'title' => t('Edit Registration'),
      'page callback' => 'openac_course_registration_edit_page_wrapper',
      'page arguments' => array($id_count + 1, $id_count + 3),
      'access callback' => 'openac_course_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'includes/course_registration.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/course/' . $wildcard . '/registration/%party/%ctools_js'] = array(
      'page arguments' => array($id_count + 1, $id_count + 3, $id_count + 4),
    ) + $items[$this->path . '/course/' . $wildcard . '/registration/%party'];

    // General course setting pages.
    $items['admin/config/college/courses'] = array(
      'title' => t('@course Settings', array('@course' => openac_entity_type_label('course'))),
      'description' => t('Configure the @course Entity', array('@course' => openac_entity_type_label('course'))),
      'page callback' => 'openac_course_config_page',
      'access arguments' => array('administer courses'),
      'file' => 'includes/course.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormSubmitBuildEntity($form, &$form_state) {
    $course = $form_state['course'];
    $course->title = $form_state['values']['title'];
    $course->start = $form_state['values']['start'];
    $course->end = $form_state['values']['end'];

    field_attach_submit('course', $course, $form, $form_state);

    return $course;
  }
}

/**
 * Course config page
 */
function openac_course_config_page() {
  return "Some course Entity Settings";
}

/**
 * Form callback: create or edit a course.
 *
 * @param $course
 *   The course object to edit or for a create form an empty course object
 *     with only a course type defined.
 */
function openac_course_form($form, &$form_state, $course = NULL, $op = 'edit') {
  if ($op == 'add' && is_null($course)) {
    $course = entity_create('course', array());
  }

  // Set the course object everywhere important.
  $form_state['build_info']['args'][0] =
    $form_state['course'] =
    $form['#course'] = $course;

  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('@course Title', array('@course' => openac_entity_type_label('course'))),
    '#default_value' => isset($course->title) ? $course->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Date'),
    '#date_format' => "d/m/Y",
    '#default_value' => isset($course->start) ? date("Y-m-d", $course->start) : '',
    '#required' => TRUE,
    '#weight' => -4,
  );

  $form['end'] = array(
    '#type' => 'date_popup',
    '#title' => t('End Date'),
    '#date_format' => "d/m/Y",
    '#default_value' => isset($course->end) ? date("Y-m-d", $course->end) : '',
    '#required' => TRUE,
    '#weight' => -3,
  );

  // Add the field related form elements.
  field_attach_form('course', $course, $form, $form_state);

  $form['modules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Modules'),
  );

  $form['modules']['table'] = array(
    '#theme' => 'table',
    '#parents' => array('modules'),
    '#tree' => TRUE,
    '#header' => array(
      t('Module'),
      t('Required'),
      t('Class'),
      t('Operations'),
    ),
    '#rows' => array(),
  );

  foreach ($course->getModuleRelations() as $key => $relation) {
    $module_id = $relation->endpoints[LANGUAGE_NONE][1]['entity_id'];
    if (empty($module_id)) { continue; }
    $module = entity_load_single('module', $module_id);
    if (empty($module)) { continue; }
    $row = array();

    $form_state['modules'][$key] = $relation;  

    // Module Column
    $row['module'] = array(
      '#markup' => entity_label('module', $module),
    );

    // Required Column
    $row['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('This module is required for this @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      '#title_display' => 'invisible',
      '#default_value' => !empty($relation->course_module_required[LANGUAGE_NONE][0]['value']),
    );

    $class_id = NULL;
    if (!empty($relation->course_module_class[LANGUAGE_NONE][0]['target_id'])) {
      $class_id = $relation->course_module_class[LANGUAGE_NONE][0]['target_id'];
    }
    $classes = db_select('class', 'c')
      ->condition('module', $module_id)
      ->condition(db_or()
         ->condition('id', $class_id)
         ->condition(db_and()
           ->condition('start', (!empty($course->start)) ? $course->start : time(), '>=')
           ->condition('status', array('pending', 'open'))
         )
      )
      ->fields('c', array('id', 'title', 'start'))
      ->execute()
      ->fetchAllAssoc('id');
    $class_options = array(
      'new' => t('- Generate New Class -'),
    );
    foreach ($classes as $id => $value) {
      $class_options[$id] = t('!title (!start)', array('!title' => $value->title, '!start' => date("M Y", $value->start)));
    }

    // Class Column
    $row['class']['id'] = array(
      '#type' => 'select',
      '#options' => $class_options,
      '#title_display' => 'invisible',
      '#default_value' =>  $class_id,
    );

    $row['class']['date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Start Date'),
      '#date_format' => "d/m/Y",
      '#date_label_position' => 'none',
      '#states' => array(
        'visible' => array(
          ':input[name="modules['.$key.'][class][id]"]' => array('value' => 'new'),
        ),
      ),
    );

    // Operations Column
    $row['operations'] = array(
      '#type' => 'submit',
      '#value' => t('Remove Module'),
      '#name' => 'remove-'.$key,
      '#submit' => array('openac_course_form_submit_remove_module'),
      '#key' => $key,
    );

    $form['modules']['table'][$key] = array(
      'module' => &$row['module'],
      'required' => &$row['required'],
      'class' => &$row['class'],
      'operations' => &$row['operations'],
      '#relation' => $relation,
    );
    $form['modules']['table']['#rows'][$key] = array(
      'module' => array('data' => &$row['module']),
      'required' => array('data' => &$row['required']),
      'class' => array('data' => &$row['class']),
      'operations' => array('data' => &$row['operations']),
    );
  }

  $add_row = array(
    'module' => array(
      '#type' => 'select',
      '#title' => t('Module'),
      '#title_display' => 'invisible',
      '#options' => openac_module_options_list(),
    ),
    'required' => array(
      '#type' => 'checkbox',
      '#title' => t('This module is required for this @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      '#title_display' => 'invisible',
      '#default_value' => 1,
    ),
    'operations' => array(
      '#type' => 'submit',
      '#value' => t('Add Module'),
      '#submit' => array('openac_course_form_submit_add_module'),
      '#limit_validation_errors' => array(array('modules')),
    ),
  );

  $form['modules']['table']['add_new'] = array(
    'module' => &$add_row['module'],
    'required' => &$add_row['required'],
    'operations' => &$add_row['operations'],
  );
  $form['modules']['table']['#rows']['add_new'] = array(
    'module' => array('data' => &$add_row['module']),
    'required' => array('data' => &$add_row['required']),
    'operations' => array('colspan' => 2, 'data' => &$add_row['operations']),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#submit' => $submit + array('openac_course_form_submit'),
  );

  if (!empty($course->title)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete @lcourse', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
      '#suffix' => l(t('Cancel'), 'admin/content/courses'),
      '#submit' => $submit + array('openac_course_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'openac_course_form_validate';
  return $form;
}


/**
 * Form API validate callback for the course form
 */
function openac_course_form_validate($form, &$form_state) {
  $course = $form_state['course'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('course', $course, $form, $form_state);
}


/**
 * Form API submit callback for the course form.
 *
 * @todo remove hard-coded link
 */
function openac_course_form_submit($form, &$form_state) {
  // Prepare form state values.
  $vals = &$form_state['values'];
  $vals['start'] = DateTime::createFromFormat("Y-m-d", $vals['start'])
    ->getTimestamp();
  $vals['end'] = DateTime::createFromFormat("Y-m-d", $vals['end'])
    ->getTimestamp();

  $course = entity_ui_controller('course')->entityFormSubmitBuildEntity($form, $form_state);

  // Update course_module relations.
  foreach (element_children($form['modules']['table']) as $rid) {
    if ($rid == 'add_new') {
      continue;
    }

    $row = $form['modules']['table'][$rid];
    $relation = $form_state['modules'][$rid];
    $relation->course_module_required[LANGUAGE_NONE][0]['value'] = $vals['modules'][$rid]['required'];

    $class_vals = &$vals['modules'][$rid]['class'];
    if ($class_vals['id'] == 'new') {
      $module_id = $relation->endpoints[LANGUAGE_NONE][1]['entity_id'];
      $module = entity_load_single('module', $module_id);

      $class_start = $course->start;
      if (!empty($class_vals['date'])) {
        $class_start = DateTime::createFromFormat("Y-m-d", $class_vals['date'])
          ->getTimestamp();
      }

      $values = array(
        'module' => $module_id,
        'title' => $module->title,
        'start' => $class_start,
      );
      $class = entity_create('class', $values);
      $class->save();

      $class_vals['id'] = $class->id;
    }
    $relation->course_module_class[LANGUAGE_NONE][0]['target_id'] = $class_vals['id'];
  }
  
  // Save the course and go back to the list of courses
  $course->save();
  $form_state['redirect'] = 'admin/college/admin/courses';
}

/**
 * Form API submit callback for the add a module button.
 */
function openac_course_form_submit_add_module($form, &$form_state) {
  $values = $form_state['values']['modules']['add_new'];
  $course = $form_state['course'];
  $course->addModule($values['module'], $values['required']);
  $form_state['rebuild'] = TRUE;
}

/**
 * Form API submit callback for the remove module button.
 */
function openac_course_form_submit_remove_module($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $course = $form_state['course'];
  $course->removeModule($button['#key']); 
  $form_state['rebuild'] = TRUE;
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function openac_course_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/courses/course/' . $form_state['course']->course_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a course.
 *
 * @param $course
 *   The course to delete
 *
 * @see confirm_form()
 */
function openac_course_delete_form($form, &$form_state, $course) {
  $form_state['course'] = $course;

  $form['#submit'][] = 'openac_course_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete @lcourse %name?', array('%name' => $course->title, '@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'admin/content/courses/course',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for course_delete_form
 */
function openac_course_delete_form_submit($form, &$form_state) {
  $course = $form_state['course'];
  $course->delete();

  drupal_set_message(t('The @lcourse %name has been deleted.', array('%name' => $course->title, '@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
  watchdog('course', 'Deleted @lcourse %name.', array('%name' => $course->title, '@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));

  $form_state['redirect'] = 'admin/content/courses';
}

/**
 * Page callback for the add student page.
 */
function openac_course_add_student_page_wrapper($course, $js = FALSE) {
  $form_id = 'openac_course_register_form';
  if (!$js) {
    return drupal_get_form($form_id, $course);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Add Student',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($course),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}
