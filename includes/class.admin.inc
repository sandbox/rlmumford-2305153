<?php
/**
 * @file
 * Class editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class ClassUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Classes',
      'description' => 'Add edit and update classes.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'position' => 'left',
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a class',
      'description' => 'Add a new class',
      'page callback'  => 'drupal_get_form',
      'page arguments' => array('openac_class_add_preform'),
      'access callback'  => 'openac_class_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 20,
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add/%module'] = array(
      'title' => 'Add a class',
      'description' => 'Add a new class',
      'page callback'  => 'drupal_get_form',
      'page arguments' => array('openac_class_form', $id_count + 1, 'add'),
      'access callback'  => 'openac_class_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 20,
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Loading and editing wtc_class entities
    $items[$this->path . '/class/' . $wildcard] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('class', $id_count + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_class_form', $id_count + 1, 'edit'),
      'access callback' => 'openac_class_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/class/' . $wildcard . '/edit'] = array(
      'title' => t('Edit'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    $items[$this->path . '/class/' . $wildcard . '/add-student'] = array(
      'title' => t('Add Student'),
      'page callback' => 'openac_class_student_page_wrapper',
      'page arguments' => array($id_count + 1, NULL),
      'access callback' => 'openac_class_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/class/' . $wildcard . '/add-student/%ctools_js'] = array(
      'page arguments' => array($id_count + 1, NULL, $id_count + 3),
    ) + $items[$this->path . '/class/' . $wildcard . '/add-student'];
    $items[$this->path . '/class/' . $wildcard . '/student/%party'] = array(
      'title' => t('Add Student'),
      'page callback' => 'openac_class_student_page_wrapper',
      'page arguments' => array($id_count + 1, $id_count + 3),
      'access callback' => 'openac_class_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/class/' . $wildcard . '/student/%party/%ctools_js'] = array(
      'page arguments' => array($id_count + 1, $id_count + 3, $id_count + 4),
    ) + $items[$this->path . '/class/' . $wildcard . '/student/%party'];
    $items[$this->path . '/class/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_class_delete_form', $id_count + 1),
      'access callback' => 'openac_class_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Class Autocomplete.
    $items['class/autocomplete/open'] = $items['class/autocomplete/any'] = array(
      'title' => 'Class autocomplete',
      'page callback' => 'openac_class_autocomplete',
      'page arguments' => array(FALSE),
      'access arguments' => array('administer classes'),
      'type' => MENU_CALLBACK,
      'file' => 'includes/class.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items['class/autocomplete/open']['page arguments'] = array(TRUE);

    return $items;
  }
}

/**
 * Form Callback: Select a module to create a class for.
 */
function openac_class_add_preform($form, &$form_state) {
  $form['module'] = array(
    '#type' => 'select',
    '#title' => t('Module'),
    '#description' => t('Please select the module this class is going to teach.'),
    '#options' => openac_module_options_list(),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Form submit callback: Redirect to the add class page proper.
 */
function openac_class_add_preform_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/college/admin/classes/add/' . $form_state['values']['module'];
}

/**
 * Form callback: create or edit a class.
 *
 * @param $class
 *   The class object to edit or for a create form an empty class object
 *     with only a class type defined.
 */
function openac_class_form($form, &$form_state, $class = NULL, $op = 'edit') {
  if ($op == 'add' && $class instanceof Module) {
    $module = $class;
    $class = entity_create('class', array());
    $class->module = $module->id;
    $class->title = $module->title;
    $class->class_body = $module->module_description;
  }

  $form_state['class'] = $form['#class'] = $class;

  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($class->title) ? $class->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['module'] = array(
    '#type' => 'select',
    '#title' => t('Module'),
    '#default_value' => isset($class->module) ? $class->module : '',
    '#options' => openac_module_options_list(),
    '#required' => TRUE,
    '#weight' => -5,
    '#disabled' => !empty($class->id),
  );

  $form['start'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Date'),
    '#date_format' => "d/m/Y",
    '#default_value' => isset($class->start) ? date("Y-m-d", $class->start) : '',
    '#required' => TRUE,
    '#weight' => -4,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => isset($class->status) ? $class->status : 'pending',
    '#options' => openac_class_status_options_list(),
    '#required' => TRUE,
    '#weight' => -3,
  );

  // Add the field related form elements.
  field_attach_form('class', $class, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save class'),
    '#submit' => $submit + array('openac_class_form_submit'),
  );

  if (!empty($class->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete class'),
      '#suffix' => l(t('Cancel'), 'admin/college/classes'),
      '#submit' => $submit + array('openac_class_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'openac_class_form_validate';
  return $form;
}


/**
 * Form API validate callback for the class form
 */
function openac_class_form_validate(&$form, &$form_state) {
  $class = $form_state['class'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('class', $class, $form, $form_state);
}


/**
 * Form API submit callback for the class form.
 *
 * @todo remove hard-coded link
 */
function openac_class_form_submit(&$form, &$form_state) {
  // Prepare form state values.
  $vals = &$form_state['values'];
  $vals['start'] = DateTime::createFromFormat("Y-m-d", $vals['start'])
    ->getTimestamp();

  $class = entity_ui_controller('class')->entityFormSubmitBuildEntity($form, $form_state);
  $class->save();
  $form_state['redirect'] = 'admin/college/admin/classes';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function openac_class_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/college/admin/classes/class/' . $form_state['class']->id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a class.
 *
 * @param $class
 *   The class to delete
 *
 * @see confirm_form()
 */
function openac_class_delete_form($form, &$form_state, $class) {
  $form_state['class'] = $class;

  $form['#submit'][] = 'openac_class_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete class %name?', array('%name' => $class->defaultLabel())),
    'admin/college/admin/classes',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for class_delete_form
 */
function openac_class_delete_form_submit($form, &$form_state) {
  $class = $form_state['class'];
  $class->delete();

  drupal_set_message(t('The class %name has been deleted.', array('%name' => $class->name)));
  watchdog('class', 'Deleted class %name.', array('%name' => $class->name));

  $form_state['redirect'] = 'admin/college/admin/classes';
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions
 * for classes.
 *
 * @param $only_open
 *   Only show classes that are currently accepting registrations.
 */
function openac_class_autocomplete($only_open = TRUE, $string = '') {
  $matches = array();
  if ($string) {
    $query = db_select('class', 'c')->fields('c', array('id', 'start'));
    $query->join('module', 'm', 'm.id = c.module');
    $query->fields('m', array('title', 'code'));
    $query->condition(db_or()
        ->condition('m.title', db_like($string) . '%', 'LIKE')
        ->condition('m.code', db_like($string) . '%', 'LIKE'));
    // At the moment we assume any class in the future is open.
    // @todo: Implement a status.
    if ($only_open) {
      $query->condition('start', time(), '>');
    }
    $results = $query->range(0, 10)->execute();
    foreach ($results as $class) {
      $string = "{$class->title} ".date("M Y", $class->start)." ({$class->id})";
      $matches[$string] = check_plain($string);
    }
  }

  drupal_json_output($matches);
}

/**
 * Page callback for the add student page.
 */
function openac_class_student_page_wrapper($class, $student = NULL, $js = FALSE) {
  $form_id = 'openac_class_register_form';
  if (!$js) {
    return drupal_get_form($form_id, $class, $student);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Add Student',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($class, $student),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}
