<?php
/**
 * @file
 * Contains Module.
 */

/**
 * The class used for Module entities
 */
class Module extends OpenAcEntity {

  /**
   * Store a list of section templates.
   */
  public $section_templates = array();

  public function __construct($values = array()) {
    parent::__construct($values, 'module');
  }

  public function defaultLabel() {
    return $this->title . ' (' . $this->code .')';
  }

  protected function defaultUri() {
    return array('path' => 'module/' . $this->code);
  }
}
