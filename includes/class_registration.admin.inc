<?php
/**
 * @file
 * Contains administration UI function for class registrations.
 */

/**
 * Form callback to register a student to a class.
 */
function openac_class_register_form($form, &$form_state, $class = FALSE, $student = FALSE, $course = FALSE) {
  if (!empty($class) && !empty($student)) {
    $id = db_select('class_registration', 'cr')
      ->condition('student', $student->pid)
      ->condition('class', $class->id)
      ->fields('cr', array('id'))
      ->execute()
      ->fetchField();
    $form['#registration'] =
      $form_state['registration'] =
      entity_load_single('class_registration', $id);
  }

  if (empty($form_state['registration'])) {
    $form['#registration'] =
      $form_state['registration'] =
      entity_create('class_registration', array());
  }

  $form['class'] = array(
    '#title' => t('Class'),
    '#required' => TRUE,
    '#description' => t('The class the student will be registered to.'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'class/autocomplete/any',
  );
  if (!empty($class)) {
    $form['class']['#disabled'] = TRUE;
    $form['class']['#default_value'] = "{$class->title} ".date("M Y", $class->start)." ({$class->id})";
  }

  $form['student'] = array(
    '#title' => t('Student'),
    '#required' => TRUE,
    '#description' => t('The student to register'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'party_extras/autocomplete/openac_enrolled',
    '#ajax' => array(
      'callback' => 'openac_class_register_course_options_ajax_callback',
      'wrapper' => 'course-select',
    ),
  );
  if (!empty($student)) {
    $form['student']['#disabled'] = TRUE;
    $form['student']['#default_value'] = "{$student->label} ({$student->pid})";
  }

  $course_options = openac_course_options_list();
  $form['course'] = array(
    '#title' => openac_entity_type_label('course'),
    '#required' => TRUE,
    '#description' => t('The @lcourse this class counts towards', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    '#type' => 'select',
    '#options' => $course_options,
    '#prefix' => '<div id="course-select">',
    '#suffix' => '</div>',
    '#default_value' => !empty($form_state['registration']->course) ? $form_state['registration']->course : NULL,
  );

  // Only offer courses that the student is a part of.
  if (!empty($student) || !empty($form_state['input']['student'])) {
    if (!empty($student)) {
      $pid = $student->pid;
    }
    else {
      preg_match('/\((?<id>[0-9]+)\)/', $form_state['input']['student'], $matches);
      $pid = $matches['id'];
    }

    $student_courses = db_select('course_registration', 'cr')
      ->fields('cr', array('course'))
      ->condition('student', $pid)
      ->execute()->fetchCol();

    foreach ($course_options as $key => $label) {
      if (!in_array($key, $student_courses)) {
        unset($course_options[$key]);
      }
    }

    if (!empty($course_options)) {
      $form['course']['#options'] = $course_options;
    }
  }
  if (!empty($course)) {
    $form['course']['#disabled'] = TRUE;
    $form['course']['#default_value'] = $course->id;
  }

  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#required' => TRUE,
    '#description' => t('The status of this registration.'),
    '#options' => openac_class_registration_status_options_list(),
    '#default_value' => !empty($form_state['registration']->status) ? $form_state['registration']->status : 'active',
  );

  field_attach_form('class_registration', $form['#registration'], $form, $form_state);

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => empty($form_state['registration']->id) ? t('Register') : t('Update'),
  );

  return $form;
}

/**
 * Ajax wrapper.
 */
function openac_class_register_course_options_ajax_callback($form, $form_state) {
  return $form['course'];
}

/**
 * Form Validation Callback to Register students.
 */
function openac_class_register_form_validate($form, &$form_state) {
  $regex = '/\((?<id>[0-9]+)\)/';

  // Get the Clas ID.
  preg_match($regex, $form_state['values']['class'], $matches);
  $class_id = !empty($matches['id']) ? $matches['id'] : FALSE;
  if (!is_numeric($class_id) || $class_id < 1) {
    form_error($form['class'], t('Invalid Class: Could not extract ID'));
  }
  else if (!class_load($class_id)) {
    form_error($form['class'], t('Invalid Class: Class does not exist.'));
  }

  // Get and check the party id.
  if (empty($form['#registration']->id)) {
    preg_match($regex, $form_state['values']['student'], $matches);
    $party_id = !empty($matches['id']) ? $matches['id'] : FALSE;
    if (!is_numeric($party_id) || $party_id < 1) {
      form_error($form['student'], t('Invalid Student: Could not extract ID'));
    }
    else if (!party_load($party_id)) {
      form_error($form['student'], t('Invalid Student: Student does not exist'));
    }
    else if (!party_hat_has_hat(party_load($party_id), 'openac_enrolled')) {
      form_error($form['student'], t('Invalid Student: That student is not currently enrolled'));
    }
  }

  // Check the course.
  if (is_numeric($party_id)) {
    $course_id = $form_state['values']['course'];
    $registered = db_select('course_registration', 'cr')
      ->condition('student', $party_id)
      ->condition('course', $course_id)
      ->countQuery()
      ->execute()
      ->fetchField();
    if (!$registered) {
      form_error($form['course'], t('Invalid @course: The student is not enrolled on that @lcourse.', array('@course' => openac_entity_type_label('course'), '@lcourse' => openac_entity_type_label('course', FALSE, TRUE))));
    }
  }

  // Check the registration does not already exist.
  if (is_numeric($party_id) && is_numeric($class_id) && is_numeric($course_id) && empty($form['#registration']->id)) {
    $class_registered = db_select('class_registration', 'cr')
      ->condition('student', $party_id)
      ->condition('course', $course_id)
      ->condition('class', $class_id)
      ->countQuery()
      ->execute()
      ->fetchField();
    if ($class_registered) {
      form_error($form, t('That student is already registered for this class.'));
    }
  }

  field_attach_form_validate('class_registration', $form['#registration'], $form, $form_state);
}

/**
 * Form Submit Callback to Register Students to classes.
 */
function openac_class_register_form_submit($form, &$form_state) {
  $registration = $form_state['registration'];
  $registration->course = $form_state['values']['course']; 
  $registration->status = $form_state['values']['status'];
 
  $regex = '/\((?<id>[0-9]+)\)/';

  // Get the Clas ID.
  if (empty($registration->id)) {
    preg_match($regex, $form_state['values']['class'], $matches);
    $class_id = !empty($matches['id']) ? $matches['id'] : FALSE;
    $registration->class = $class_id;
  }

  // Get the student id.
  if (empty($registration->id)) {
    preg_match($regex, $form_state['values']['student'], $matches);
    $party_id = !empty($matches['id']) ? $matches['id'] : FALSE;
    $registration->student = $party_id;
  }

  field_attach_submit('class_registration', $registration, $form, $form_state);
  $registration->save();

  drupal_set_message(t('Student successfully registered to the class.'));
}
