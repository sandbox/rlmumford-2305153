<?php
/**
 * @file
 * Contains OpenAcEntityController.
 */

/**
 * Base Entity Controller for OpenAc Entities.
 */
class OpenAcEntityController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    if (in_array($hook, array('update', 'insert'))) {
      $entity->postSave($hook);
    }
    if (in_array($hook, array('presave'))) {
      $entity->preSave();
    }
    if ($hook == 'delete') {
      $entity->postDelete();
    }

    parent::invoke($hook, $entity);
  }
}

/**
 * Base entity class for OpenAC entities.
 */
class OpenAcEntity extends Entity {

  /**
   * Perform logic just after a entity has been saved.
   *
   * @param string $update
   *   Either 'insert' or 'update' depending on whether the entity is being
   *   inserted or updated.
   */
  public function postSave($update) {}
  public function preSave() {}
  public function postDelete() {}
}
