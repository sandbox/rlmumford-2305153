<?php
/**
 * @file
 * Contains Class.
 */

/**
 * The class used for Class entities
 */
class OpenAcClass extends OpenAcEntity {

  /**
   * The module this class is teaching.
   * @var Module.
   */
  protected $module_entity;

  public function __construct($values = array()) {
    parent::__construct($values, 'class');

    if (empty($values['module'])) {
      return;
    }

    // Carry over values from the module.
    if (empty($this->title)) {
      $this->title = $this->getModule()->title;
    }
    if (empty($this->class_body)) {
      $this->class_body = $this->getModule()->module_description;
    }
    if (empty($this->class_tutor)) {
      $this->class_tutor = $this->getModule()->module_tutor;
    }
    if (empty($this->class_assistants)) {
      $this->class_assistants = $this->getModule()->module_assistants;
    }
    if (module_exists('openac_resources') && empty($this->openac_resources)) {
      $this->openac_resources = $this->getModule()->openac_resources;
    }
  }

  public function defaultLabel() {
    return $this->getModule()->title . ' (' . date('m Y', $this->start) .')';
  }

  protected function defaultUri() {
    global $user;
    return array('path' => 'user/'.$user->uid.'/my-course/class/' . $this->id);
  }

  public function getModule() {
    if (empty($this->module_entity)) {
      $this->module_entity = entity_load_single('module', $this->module);
    }

    return $this->module_entity;
  }

  /**
   * Set up all the default sections.
   */
  public function postSave($hook) {
    if ($hook != 'insert') {
      return;
    }

    foreach ($this->getModule()->section_templates as $template) {
      if (!is_array($template)) {
        $template = drupal_json_decode($template);
      }

      if (empty($template) || !is_array($template)) {
        continue;
      }
      $section = entity_create('class_section', $template);
      $section->class = $this->id;
      $section->publish_date = $this->start;
      $section->save();
    }
  }

  /**
   * Remove any class registrations when the class is deleted.
   */
  public function postDelete() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'class_registration');
    $query->propertyCondition('class', $this->id);
    $result = $query->execute();

    if (!empty($result['class_registration'])) {
      $controller = entity_get_controller('class_registration');
      $controller->delete(array_keys($result['class_registration']));
    }
  }

  /**
   * Register a student to a class.
   */
  public function register(Party $student, Course $course) {
    entity_get_controller($this->entityType)->registerStudent($this, $student, $course);
  }
}

/**
 * The class controller for class entities.
 */
class OpenAcClassController extends OpenAcEntityController {

  /**
   * Get a list of student ID's studying for this class.
   */
  public function getRegisteredStudents($class) {
    if (is_object($class)) {
      $class = $class->id;
    }

    $students = db_select('class_registration', 'cr')
      ->condition('class', $class)
      ->fields('cr', array('id', 'student'))
      ->execute()->fetchAllKeyed();

    return $students;
  }

  /**
   * Register a student to a class.
   */
  public function registerStudent(OpenAcClass $class, Party $student, Course $course) {
    $registration = entity_get_controller('class_registration')->createForRegistration($class, $student, $course);
    $registration->save();
  }

}
