<?php
/**
 * @file
 * Module editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class ModuleUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Modules',
      'description' => 'Add edit and update modules.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'position' => 'left',
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a module',
      'description' => 'Add a new module',
      'page callback'  => 'drupal_get_form',
      'page arguments' => array('openac_module_form', NULL, 'add'),
      'access callback'  => 'openac_module_access',
      'access arguments' => array('edit'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 20,
      'file' => 'includes/module.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    // Loading and editing wtc_module entities
    $items[$this->path . '/module/' . $wildcard] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('module', $id_count + 1),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_module_form', $id_count + 1, 'edit'),
      'access callback' => 'openac_module_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'includes/module.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/module/' . $wildcard . '/edit'] = array(
      'title' => t('Edit'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    $items[$this->path . '/module/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('openac_module_delete_form', $id_count + 1),
      'access callback' => 'openac_module_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'includes/module.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );
    $items[$this->path . '/module/' . $wildcard . '/section/%/%ctools_js'] = array(
      'title' => 'Edit Session',
      'page callback' => 'openac_module_section_page_wrapper',
      'page arguments' => array($id_count + 1, $id_count + 3, $id_count + 4),
      'access callback' => 'openac_module_access',
      'access arguments' => array('edit', $id_count + 1),
      'file' => 'includes/module.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    $items['module/autocomplete'] = array(
      'title' => 'Module autocomplete',
      'page callback' => 'openac_module_autocomplete',
      'access arguments' => array('administer openac modules'),
      'type' => MENU_CALLBACK,
      'file' => 'includes/module.admin.inc',
      'file path' => drupal_get_path('module', 'openac'),
    );

    return $items;
  }
}

/**
 * Form callback: create or edit a module.
 *
 * @param $module
 *   The module object to edit or for a create form an empty module object
 *     with only a module type defined.
 */
function openac_module_form($form, &$form_state, $module = NULL, $op = 'edit') {
  if ($op == 'add' && is_null($module)) {
    $module = entity_create('module', array());
  }

  if (empty($form_state['module'])) {
    $form_state['saved_sections'] = array_keys($module->section_templates);
  }
  $form_state['module'] = $form['#module'] = $module;

  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($module->title) ? $module->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['code'] = array(
    '#type' => 'textfield',
    '#title' => t('Module Code'),
    '#default_value' => isset($module->code) ? $module->code : '',
    '#maxlength' => 7,
    '#required' => TRUE,
    '#weight' => -4,
  );

  // Add the field related form elements.
  field_attach_form('module', $module, $form, $form_state);

  $form['section_templates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sessions'),
  );

  $form['section_templates']['table'] = array(
    '#theme' => 'table',
    '#parents' => array('section_templates'),
    '#tree' => TRUE,
    '#header' => array(
      t('Number'),
      t('Title'),
      t('Operations'),
    ),
    '#rows' => array(),
  );

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  foreach ($module->section_templates as $number => $template) {
    $template_entity = entity_import('class_section', $template);

    $row['number'] = array(
      '#markup' => $template_entity->number,
    );

    $row['title'] = array(
      '#markup' => $template_entity->title,
    );

    if (in_array($number, $form_state['saved_sections'])) {
      $row['operations'] = array(
        '#markup' => l('edit', 'admin/college/admin/modules/module/'.$module->id.'/section/'.$template_entity->number.'/nojs', array(
            'attributes' => array(
              'class' => array(),
            ),
            'query' => drupal_get_destination(),
        )),
      );
    }
    else {
      $row['operations'] = array(
        '#markup' => '<b>' . t('Save the Module to edit these templates.') . '</b>',
      );
    }


    $form['section_templates']['table'][$number] = array(
      'number' => &$row['number'],
      'title' => &$row['title'],
      'operations' => &$row['operations'],
      '#value' => $template,
    );
    $form['section_templates']['table']['#rows'][$number] = array(
      'number' => array('data' => &$row['number']),
      'title' => array('data' => &$row['title']),
      'operations' => array('data' => &$row['operations']),
    );
    unset($row);
  }


  $row = array();
  $row['number'] = array(
    '#type' => 'textfield',
    '#prefix' => '#',
    '#default_value' => '',
    '#maxlength' => 16,
    '#size' => 4,
  );
  $row['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#title_display' => 'attribute',
    '#maxlength' => 255,
  );
  $row['operations'] = array(
    '#type' => 'submit',
    '#value' => t('Add Session'),
    '#submit' => array(
      'openac_module_form_submit_add_new_section',
    ),
  );

  $form['section_templates']['table']['_add_new'] = array(
    'number' => &$row['number'],
    'title' => &$row['title'],
    'operations' => &$row['operations'],
  );
  $form['section_templates']['table']['#rows']['_add_new'] = array(
    'number' => array('data' => &$row['number']),
    'title' => array('data' => &$row['title']),
    'operations' => array('data' => &$row['operations']),
  );

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save module'),
    '#submit' => $submit + array('openac_module_form_submit'),
  );

  if (!empty($module->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete module'),
      '#suffix' => l(t('Cancel'), 'admin/college/modules'),
      '#submit' => $submit + array('openac_module_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'openac_module_form_validate';

  // Do this to make things work with ajax for some unknown reason.
  $form_state['rebuild'] = TRUE;
  return $form;
}


/**
 * Form API validate callback for the module form
 */
function openac_module_form_validate(&$form, &$form_state) {
  $module = $form_state['module'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('module', $module, $form, $form_state);
}


/**
 * Form API submit callback for the module form.
 *
 * @todo remove hard-coded link
 */
function openac_module_form_submit(&$form, &$form_state) {
  unset($form_state['values']['section_templates']);
  $module = entity_ui_controller('module')->entityFormSubmitBuildEntity($form, $form_state);
  $module->save();
  $form_state['redirect'] = 'admin/college/admin/modules';
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function openac_module_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/college/admin/modules/module/' . $form_state['module']->id . '/delete';
}

function openac_module_form_submit_add_new_section($form, &$form_state) {
  $new_vals = $form_state['values']['section_templates']['_add_new'];
  $section = entity_create('class_section', $new_vals);
  $module = $form_state['module'];
  $module->section_templates[$section->number] = entity_export('class_section', $section);
  unset($form_state['values']['section_templates']['_add_new']);
  unset($form_state['input']['section_templates']['_add_new']);

  $form_state['rebuild'] = TRUE;
}


/**
 * Form callback: confirmation form for deleting a module.
 *
 * @param $module
 *   The module to delete
 *
 * @see confirm_form()
 */
function openac_module_delete_form($form, &$form_state, $module) {
  $form_state['module'] = $module;

  $form['#submit'][] = 'openac_module_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete module %name?', array('%name' => $module->defaultLabel())),
    'admin/college/admin/modules',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for module_delete_form
 */
function openac_module_delete_form_submit($form, &$form_state) {
  $module = $form_state['module'];
  $module->delete();

  drupal_set_message(t('The module %name has been deleted.', array('%name' => $module->name)));
  watchdog('module', 'Deleted module %name.', array('%name' => $module->name));

  $form_state['redirect'] = 'admin/college/admin/modules';
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions
 * for existing module.
 */
function openac_module_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('module')
      ->fields('module', array('title', 'code'))
      ->condition(db_or()
        ->condition('title', db_like($string) . '%', 'LIKE')
        ->condition('code', db_like($string) . '%', 'LIKE'))
      ->range(0, 10)
      ->execute();
    foreach ($result as $module) {
      $matches[$module->title . ' (' . $module->code .')'] = check_plain($module->title . ' (' . $module->code .')');
    }
  }

  drupal_json_output($matches);
}

/**
 * Return a page to edit the section template on a module.
 */
function openac_module_section_page_wrapper($module, $section_number, $js = FALSE) {
  $form_id = 'openac_module_section_form';
  if (!$js) {
    return drupal_get_form($form_id, $module, $section_number);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => 'Edit Session',
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($module, $section_number),
      ),
    );

    $commands = ctools_modal_form_wrapper($form_id, $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}

function openac_module_section_form($form, &$form_state, $module, $section_number) {
  $form_state['module'] = $module;
  $form_state['section_number'] = $section_number;

  $section_export = $module->section_templates[$section_number];
  $form_state['section'] = entity_import('class_section', $section_export);

  form_load_include($form_state, 'inc', 'openac', 'includes/class_section.admin');
  $form = openac_class_section_form($form, $form_state, $form_state['section']);
  unset($form['publish_date']);
  $form['actions']['submit']['#submit'] = array('openac_module_section_form_submit');
  unset($row['actions']['cancel']);
  unset($row['actions']['submit']['#suffix']);

  return $form;
}

function openac_module_section_form_validate($form, &$form_state) {
  if ($form_state['values']['number'] != $form_state['section_number']) {
    $module = $form_state['module'];
    if (in_array($form_state['values']['number'], array_keys($module->section_templates))) {
      form_error($form['number'], t('Cannot change the session number to this as this session number already exists'));
    }
  }
}

function openac_module_section_form_submit($form, &$form_state) {
  $section = entity_ui_controller('class_section')->entityFormSubmitBuildEntity($form, $form_state);

  $module = $form_state['module'];
  if ($section->number == $form_state['section_number']) {
    $module->section_templates[$section->number] = entity_export('class_section', $section);
  }
  else {
    unset($module->section_templates[$form_state['section_number']]);
    $module->section_templates[$section->number] = entity_export('class_section', $section);
  }

  $module->save();
}
