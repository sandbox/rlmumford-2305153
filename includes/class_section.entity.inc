<?php
/**
 * @file
 * Contains OpenAcSection.
 */

/**
 * The class used for section entities
 */
class OpenAcSection extends OpenAcEntity {

  /**
   * The class this section belongs to.
   * @var Class.
   */
  protected $class_entity;

  public function __construct($values = array()) {
    parent::__construct($values, 'class_section');
  }

  public function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'class/' . $this->getClass()->id . '/section/' . $this->id);
  }

  public function getModule() {
    if (empty($this->class_entity)) {
      $this->class_entity = entity_load_single('class', $this->class);
    }

    return $this->class_entity;
  }
}
