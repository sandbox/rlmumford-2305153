<?php
/**
 * @file
 * Contains CourseRegistration.
 */

/**
 * The class used for Course Registration entities
 */
class CourseRegistration extends OpenAcEntity {

  /**
   * The course.
   * @var cCourse
   */
  protected $course_entity;

  /**
   * The party taking this course.
   * @var Party.
   */
  protected $student_entity;

  public function __construct($values = array()) {
    parent::__construct($values, 'course_registration');
  }

  public function defaultLabel() {
    return $this->getStudent()->label() . ' - ' . $this->getCourse()->label();
  }

  public function getStudent() {
    if (empty($this->student_entity)) {
      $this->student_entity = entity_load_single('party', $this->student);
    }

    return $this->student_entity;
  }

  public function getCourse() {
    if (empty($this->course_entity)) {
      $this->course_entity = entity_load_single('course', $this->course);
    }

    return $this->course_entity;
  }

  public static function register(Party $student, Course $course) {
    $values = array(
      'student' => $student->pid,
      'course' => $course->id,
    );
    return new CourseRegistration($values);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($hook) {
    if ($hook != 'insert') {
      return;
    }

    // Automatically register the student to default classes.
    $course = entity_load_single('course', $this->course);
    $student = entity_load_single('party', $this->student);
    foreach ($course->getModuleRelations() as $relation) {
      if (empty($relation->course_module_class[LANGUAGE_NONE][0]['target_id'])) {
        continue;
      }

      $class_id = $relation->course_module_class[LANGUAGE_NONE][0]['target_id'];
      $class = entity_load_single('class', $class_id);
      if ($class) {
        $class->register($student, $course);
      } else {
        watchdog('OpenAC Academy', 'Unable to register @student to class @class: Could not load class.', array('@student' => $student->label, '@class' => $class_id));
      }
    }
  }
}
