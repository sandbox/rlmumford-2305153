<?php
/**
 * @file
 * openac.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function openac_default_page_manager_pages() {
  $export = array();

  // Scan directory for any .panel files
  $files = file_scan_directory(dirname(__FILE__) . '/page_manager_pages', '/\.page$/', array('key' => 'name'));
  foreach ($files as $file) {
    if ((include $file->uri) == 1) {
      $export[$page->name] = $page;
    }
  }

  return $export;
}
