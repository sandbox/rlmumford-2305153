<?php

/**
 * @file
 * Plugin to provide an relationship handler for term from node.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Current @course from Party', array('@course' => openac_entity_type_label('course'))),
  'keyword' => 'course',
  'description' => t('Adds the @lcourse entity from the Party context', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  'required context' => new ctools_context_required(t('Party'), 'party'),
  'context' => 'openac_course_current_from_party_context',
  'edit form' => 'openac_course_current_from_party_settings_form',
);

/**
 * Return a new context based on an existing context.
 */
function openac_course_current_from_party_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('entity:course', NULL);
  }

  $party = $context->data;

  // @todo: Find a way to distinguish between multiple courses.
  $query = db_select('course_registration', 'cr')
    ->condition('cr.status', 'active')
    ->condition('cr.student', $party->pid)
    ->fields('cr', array('course'));
  // Filter by courses that haven't ended/
  // @todo: Actually update statuses when a course ends.
  $query->join('course', 'c', 'c.id = cr.course');
  $query->addExpression('CASE WHEN c.start < :time THEN 1 ELSE 0 END', 'started', array(':time' => time()));
  $query->condition('c.end', time(), '>');
  $query->orderBy('started', 'DESC');
  $query->orderBy('c.start', 'DESC');
  $course_id = $query->execute()
    ->fetchField();

  if (empty($course_id)) {
    return ctools_context_create_empty('entity:course', NULL);
  }

  $course = entity_load_single('course', $course_id);
  if ($course) {
    return ctools_context_create('entity:course', $course);
  }

  return ctools_context_create_empty('entity:course', NULL);
}

/**
 * Settings form for the relationship.
 */
function openac_course_current_from_party_settings_form($form, &$form_state) {
  return $form;
}
