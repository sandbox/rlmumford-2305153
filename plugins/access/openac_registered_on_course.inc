<?php
/**
 * @file
 * Plugin to provide access control based course registration.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("@course Registration", array('@course' => openac_entity_type_label('course'))),
  'description' => t('Control access by @lcourse the Party is registered to.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  'callback' => 'openac_openac_registered_on_course_ctools_access_check',
  'settings form' => 'openac_openac_registered_on_course_ctools_access_settings',
  'settings form validation' => 'ctools_term_ctools_access_settings_validate',
  'settings form submit' => 'ctools_term_ctools_access_settings_submit',
  'summary' => 'openac_openac_registered_on_course_ctools_access_summary',
  'required context' => new ctools_context_required(t('Party'), 'party'),
);

/**
 * Settings form for the 'by term' access plugin
 */
function openac_openac_registered_on_course_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['#tree'] = TRUE;
  $form['settings']['course'] = array(
    '#title' => openac_entity_type_label('course', TRUE),
    '#description' => t('Select a @lcourse or @lcourses.', , array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE), '@lcourses' => openac_entity_type_label('course', TRUE, TRUE))),
    '#default_value' => !empty($conf['course']) ? $conf['course'] : '',
    '#multiple' => TRUE,
    '#type' => 'checkboxes',
    '#options' => openac_course_options_list(),
  );

  return $form;
}

/**
 * Check for access.
 *
 * This plugin returns true if a is registered to ANY of the chosen courses.
 */
function openac_openac_registered_on_course_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  // Get the hats.
  if (!isset($conf['course'])) {
    return FALSE;
  }

  $courses = array_filter($conf['course']);
  $class_registered = db_select('course_registration', 'cr')
    ->condition('student', $context->data->pid)
    ->condition('course', $courses)
    ->countQuery()
    ->execute()
    ->fetchField();

  return $class_registered;
}

/**
 * Provide a summary description based upon the checked courses.
 */
function openac_openac_registered_on_course_ctools_access_summary($conf, $context) {
  return t('@student is registered to a @lcourse.', array('@student' => $context->identifier, '@lcourse' => openac_entity_type_label('course', FALSE, TRUE)));
}
