<?php

/**
 * @file
 * CTools content for student form
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Student Status Form'),
  'content_types' => 'openac_student_status_form',
  // 'single' means not to be subtyped.
  'single' => TRUE,
  // Name of a function which will render the block.
  'render callback' => 'openac_student_status_form_render',

  // Icon goes in the directory with the content type.
  'description' => t('Show a student status form.'),
  'required context' => array(
     new ctools_context_required(t('Student'), 'entity:party'),
  ),
  'edit form' => 'openac_student_status_form_edit_form',
  'admin title' => t('Student status form'),

  // presents a block which is used in the preview of the data.
  // Pn Panels this is the preview pane shown on the panels building page.
  'category' => t('OpenCRM Academy'),
);

/**
 * Render the Student Status Form
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function openac_student_status_form_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();
  $block->title = t('Student Status Form');
  $block->content = '';

  $context = reset($contexts);
  if (!empty($context->data)) {
    // Get hold of the form
    $form = drupal_get_form('openac_student_status_form_form', $context->data);

    // Render it...
    $block->content = drupal_render($form);
  }

  return $block;
}

/**
 * Form
 */
function openac_student_status_form_form($form, &$form_state, $party) {
  form_load_include($form_state, 'inc', 'openac', 'plugins/content_types/openac_student_status_form.inc');

  $form['#party'] = $party;

  // Work out if the party is withdrawn.
  $is_withdrawn = in_array('openac_withdrawn', array_keys(party_hat_get_hats($party)));
  $is_on_study_break = in_array('openac_study_break', array_keys(party_hat_get_hats($party)));

  if ($is_withdrawn) {
    $description = t('@name has been withdrawn from studying at WTC', array('@name' => $party->label));
  }
  else if ($is_on_study_break) {
    $description = t('@name is on a study break and is expected to resume studying in the future.', array('@name' => $party->label));
  }
  else {
    $description = t('Use the buttons below to alter the status of this student');
  }

  $form['description'] = array(
    '#markup' => $description,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['withdraw'] = array(
    '#type' => 'submit',
	'#submit' => array(
	  'openac_student_status_form_form_withdraw_submit',
	),
	'#value' => t('Withdraw'),
	'#access' => !$is_withdrawn,
  );
  $form['actions']['study_break'] = array(
    '#type' => 'submit',
	'#submit' => array(
	  'openac_student_status_form_form_study_break_submit',
	),
	'#value' => t('Study Break'),
	'#access' => !$is_withdrawn && !$is_on_study_break,
  );
  $form['actions']['resume_course'] = array(
    '#type' => 'submit',
	'#submit' => array(
	  'openac_student_status_form_form_resume_course_submit',
	),
	'#value' => t('Resume Studying'),
	'#access' => !$is_withdrawn && $is_on_study_break,
  );

  return $form;
}

function openac_student_status_form_form_withdraw_submit($form, &$form_state) {
  $party = $form['#party'];

  party_hat_hats_assign($party, array('openac_withdrawn'), FALSE);
  party_hats_unassign_hats($party, array('openac_enrolled', 'openac_study_break'), TRUE);
}

function openac_student_status_form_form_study_break_submit($form, &$form_state) {
  $party = $form['#party'];
  party_hat_hats_assign($party, array('openac_study_break'), FALSE);
  party_hat_hats_unassign($party, array('openac_enrolled'), TRUE);
}

function openac_student_status_form_form_resume_course_submit($form, &$form_state) {
  $party = $form['#party'];
  party_hat_hats_assign($party, array('openac_enrolled'), FALSE);
  party_hat_hats_unassign($party, array('openac_study_break'), TRUE);
}

/**
 * Config Form
 */
function openac_student_status_form_edit_form($form, &$form_state) {
  return $form;
}
