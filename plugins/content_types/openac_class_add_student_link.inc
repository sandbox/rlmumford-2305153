<?php
$plugin = array(
  'single' => TRUE,
  'title' => t('Add Student to Class Modal Action'),
  'description' => t('Adds an "Add Student" button that opens in a modal.'),
  'category' => t('OpenCRM Academy'),
  'required context' => array(
     new ctools_context_required(t('Class'), 'entity:class'),
  ),
  'edit form' => 'openac_class_add_student_link_edit_form',
  'render callback' => 'openac_class_add_student_link_render',
  'defaults' => array(
    'text' => t('Add Student'),
  ),
);

function openac_class_add_student_link_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['text'] = array(
    '#title' => t('Link Text'),
    '#type' => 'textfield',
    '#default_value' => $conf['text'],
    '#required' => TRUE,
  );

  return $form;
}

function openac_class_add_student_link_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

function openac_class_add_student_link_render($subtype, $conf, $panel_args, $context = NULL) {
  $class = $context[0]->data;
  $link_text = $conf['text'];
  $link_url = "admin/college/admin/classes/class/{$class->id}/add-student/nojs";

  ctools_include('ajax'); ctools_include('modal');
  ctools_modal_add_js();

  $block = new stdClass();
  $block->title = '';
  $block->content = ctools_modal_text_button($link_text, $link_url, 'Add a student');

  return $block;
}
