<?php
/**
 * @file
 * OpenAc Course Menu
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('@course Menu', array('@course' => openac_entity_type_label('course'))),
  'content types' => 'openac_course_menu_content_types',
  'render callback' => 'openac_course_menu_render',
  'admin title' => 'openac_course_menu_admin_title',
);

/**
 * Return all sub types.
 */
function openac_course_menu_content_types() {
  $defaults = array(
    'category' => array(openac_entity_type_label('course'), 0),
    'required context' => array(
      new ctools_context_required(t('User'), 'entity:user'),
      new ctools_context_optional(t('Sub Page'), 'string'),
    ),
    'edit form' => 'openac_course_menu_edit_form',
    'description' => t('Render a sub menu for the @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
  );

  $content_types = array();
  $content_types['home'] = array(
    'title' => t('Home Menu'),
    'category' => array(t('User Dashboard'), 0),
    'description' => t('Render a sub menu for the my account home.'),
  ) + $defaults;
  $content_types['faculty'] = array(
    'title' => t('Faculty Menu'),
    'category' => array(t('User Dashboard'), 0),
    'description' => t('Render a sub menu for the faculty my account home.'),
  ) + $defaults;
  $content_types['course'] = array(
    'title' => t('@course Menu', array('@course' => openac_entity_type_label('course'))),
  ) + $defaults;
  $content_types['course']['required context'][] = new ctools_context_required(openac_entity_type_label('course'), 'entity:course');
  $content_types['class'] = array(
    'title' => t('Class Menu'),
    'category' => array(t('Class'), 0),
  ) + $defaults;
  $content_types['class']['required context'][] = new ctools_context_required(t('Class'), 'entity:class');
  $content_types['section'] = array(
    'title' => t('Session Menu'),
    'category' => array(t('Class'), 0),
  ) + $defaults;
  $content_types['section']['required context'][] = new ctools_context_required(t('Class'), 'entity:class');
  $content_types['faculty_classes'] = array(
    'title' => t('Faculty Classes'),
    'category' => array(t('Faculty'), 0),
  ) + $defaults;

  if (module_exists('openac_location')) {
    $content_types['location'] = array(
      'title' => t('Location Menu'),
      'category' => array(t('Location'), 0),
    ) + $defaults;
    $content_types['location']['required context'][] = new ctools_context_required(t('Location'), 'entity:opencrm_location');
  }

  return $content_types;
}

/**
 * Render callback for group menu
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time
 * @param $args
 * @param $context
 *   Context - in this case we don't have any
 *
 * @return
 *   An object with at least title and content members
 */
function openac_course_menu_render($subtype, $conf, $args, $context) {
  // Load our information.
  global $user;
  $account = !empty($context[0]->data) ? $context[0]->data: $user;
  $party = party_user_get_party($account);
  $subpage = !empty($context[1]->data) ? $context[1]->data: '';
  $path = strlen($subpage) > 0 ? substr(current_path(), 0, -strlen($subpage) - 1): current_path();
  $entityContext = !empty($context[2]) ? $context[2]: FALSE;

  if ($subtype == 'section') {
    $path_bits = explode('/', $path);
    $section_id = array_pop($path_bits); array_pop($path_bits);
    $path = implode('/', $path_bits);
    $subpage = "section/$section_id";
  }

  // Start building our return
  $block = new stdClass();
  $block->title = t('Menu');

  // Everything goes inside a menu_tree.
  $content = array(
    '#sorted' => TRUE,
    '#theme_wrappers' => array('menu_tree__openac_openac_course_menu'),
  );
  $admin = array(
    '#prefix' => '<h3>' . t('Administration') . '</h3>',
    '#sorted' => TRUE,
    '#theme_wrappers' => array('menu_tree__openac_openac_course_menu'),
    '#attributes' => array(
      'class' => array('admin-links'),
    ),
  );

  // The defaults so we don't have to specify them every time.
  $defaults = array(
    '#theme' => 'menu_link__openac_openac_course_menu',
    '#attributes' => array(
      'class' => array('leaf'),
    ),
    '#localized_options' => array('alter' => TRUE),
    '#below' => array(),
  );

  // The default item, may have it's title overridden later.
  $content[''] = array(
    '#attributes' => array(
      'class' => array('first', 'leaf'),
    ),
    '#title' => t('Info'),
    '#href' => $path,
  );

  $enrolled = party_hat_has_hat($party, 'openac_enrolled');
  $mooc = FALSE;
  switch ($subtype) {
    // The Home menu
    // @todo: This can't go in a generic module!
    case 'home':
      if ($enrolled || $mooc) {
        $content['']['#title'] = t('News');
      }
      if ($enrolled) {
        $content['summer-reading-list'] = array(
          '#title' => t('Summer Reading List'),
          '#href' => $path . '/summer-reading-list',
        );
        $content['study-skills'] = array(
          '#title' => t('Study Skills'),
          '#href' => $path . '/study-skills',
        );
        $content['student-handbook'] = array(
          '#title' => t('Student Handbook'),
          '#href' => $path . '/student-handbook',
        );
        $content['library'] = array(
          '#title' => t('Library'),
          '#href' => $path . '/library',
        );
        $content['donations'] = array(
          '#title' => t('Donations'),
          '#href' => $path . '/donations',
        );
      }
      if ($mooc) {
        $content['beyond-openwtc'] = array(
          '#title' => t('Beyond OpenWTC'),
          '#href' => $path . '/beyond-openwtc',
        );
        $content['mooc-feedback'] = array(
          '#title' => t('Feedback'),
          '#href' => $path . '/mooc-feedback',
        );
        $content['support-openwtc'] = array(
          '#title' => t('Donate'),
          '#href' => $path . '/support-openwtc',
        );
      }
      break;
    case 'faculty_classes':
      $classes = array();
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'class');
      $query->fieldCondition('class_tutor', 'target_id', $party->pid);
      $query->propertyCondition('status', 'finished', '<>');
      $result = $query->execute();
      if (!empty($result['class'])) {
        $classes = entity_load('class', array_keys($result['class']));
        foreach ($classes as $class) {
          $class_links["{$class->id}"] = $class->title;
        }

        // Output our module links
        foreach ($class_links as $id => $title) {
          $content['class/' . $id] = array(
            '#title' => $title,
            '#href' => $path . '/class/' . $id,
            '#localized_options' => array(
              'html' => TRUE,
            ),
          );
        }
      }
      break;
    case 'faculty':
      break;
    // All other menus match the general group menu.
    default:
      // Add in our links for our different classes.
      $classes = array();
      if ($subtype == 'course' && !empty($entityContext->data) && $entityContext->plugin == 'entity:course') {
        // Get hold of our relationships.
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'class_registration');
        $query->propertyCondition('course', $entityContext->data->id);
        $query->propertyCondition('student', $party->pid);
        $query->propertyCondition('status', 'active');
        $results = $query->execute();

        // Process our relationships to get our modules.
        if (!empty($results['class_registration'])) {
          foreach (entity_load('class_registration', array_keys($results['class_registration'])) as $reg) {
            $classes[] = $reg->class;
          }
        }
        $classes = entity_load('class', $classes);
      }

      if (!empty($classes)) {
        // Extract our links and sort
        $class_links = array();
        foreach ($classes as $class) {
          if ($class->status == 'in_progress') {
            $class_links["{$class->id}"] = $class->title;
          }
        }

        // Output our module links
        foreach ($class_links as $id => $title) {
          $content['class/' . $id] = array(
            '#title' => $title,
            '#href' => $path . '/class/' . $id,
            '#localized_options' => array(
              'html' => TRUE,
            ),
          );
        }
      }

      if (in_array($subtype, array('section', 'class')) && !empty($entityContext->data) && $entityContext->plugin == 'entity:class') {
        // Get sections.
        $class_id = $entityContext->data->id;
        $sections = db_select('class_section', 'cs')
          ->fields('cs', array('id', 'number', 'title'))
          ->condition('class', $class_id)
          ->condition('publish_date', time(), '<')
          ->execute()
          ->fetchAllAssoc('id');

        foreach ($sections as $section) {
          $content['section/'.$section->id] = array(
            '#title' => "{$section->number}: {$section->title}",
            '#href' => $path . '/section/'.$section->id,
            '#localized_options' => array(
              'html' => TRUE,
            ),
            '#attributes' => array(
              'class' => array('leaf'),
            ),
          );
        }
      }

      // Students and discussions are just for courses and hubs.
      if (in_array($subtype, array('course', 'class', 'section', 'location'))
        && party_hat_has_hat($party, 'openac_enrolled')) {
        $content['students'] = array(
          '#title' => t('Students'),
          '#href' => $path . '/students',
        );
        if ($subtype == 'course') {
          $content['students']['#title'] = t('@course Students', array('@course' => openac_entity_type_label('course')));
        }
      }
    break;
  }
  $context['path'] = $path;
  drupal_alter('openac_sub_menu_content', $content, $subtype, $context);

  // Add defaults to all items.
  foreach (element_children($content) as $key) {
    $item = $content[$key];
    $content[$key] = $item + $defaults;
  }

  if (isset($content[$subpage])) {
    $content[$subpage]['#attributes']['class'][] = 'active-trail';
  }
  elseif (isset($admin[$subpage])) {
    $admin[$subpage]['#attributes']['class'][] = 'active-trail';
  }

  $block->content = drupal_render($content);
  if (count($admin) > 4) {
    $block->content .= drupal_render($admin);
  }
  return $block;
}

/**
 * 'Edit' callback for the content type.
 *
 * Return the form so we get all the defaults.
 */
function openac_course_menu_edit_form($form, &$form_state) {
  return $form;
}

function openac_course_menu_admin_title($subtype) {
  $types = openac_course_menu_content_types();
  return $types[$subtype]['title'];
}
