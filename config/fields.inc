<?php
/**
 * @file
 * Stores field definitions for this module.
 */

$fields = array();

// Course Description
$field_name = 'course_description';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_with_summary',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'course',
    'bundle' => 'course',
    'label' => t('Description'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea_with_summary',
    ),
  ),
);

// Module Description
$field_name = 'module_description';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_with_summary',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'module',
    'bundle' => 'module',
    'label' => t('Description'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea_with_summary',
    ),
  ),
);

// Default Module Tutor.
$field_name = 'module_tutor';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'party',
      'handler' => 'base',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'module',
    'bundle' => 'module',
    'label' => t('Tutor'),
    'description' => t('The tutor who teaches the @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
  ),
);

// Teaching Assistants
$field_name = 'module_assistants';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'party',
      'handler' => 'base',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'module',
    'bundle' => 'module',
    'label' => t('Teaching Assistants'),
    'description' => t('The default teaching assistants for classes of this module. Teaching assistants have permission to mark assignments associated with a class.'),
    'widget' => array(
      'type' => 'entityreference_autocomplete_tags',
    ),
  ),
);

// Class Description
$field_name = 'class_body';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_with_summary',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'class',
    'bundle' => 'class',
    'label' => t('Body'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea_with_summary',
    ),
  ),
);

// Class Tutor.
$field_name = 'class_tutor';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => 1,
    'settings' => array(
      'target_type' => 'party',
      'handler' => 'base',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'class',
    'bundle' => 'class',
    'label' => t('Tutor'),
    'widget' => array(
      'type' => 'entityreference_autocomplete',
    ),
  ),
);

// Teaching Assistants
$field_name = 'class_assistants';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'party',
      'handler' => 'base',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'class',
    'bundle' => 'class',
    'label' => t('Teaching Assistants'),
    'widget' => array(
      'type' => 'entityreference_autocomplete_tags',
    ),
  ),
);

// Course-Module Relation
$field_name = 'course_module_required';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'list_boolean',
    'cardinality' => 1,
    'settings' => array(
      0 => t('Optional'),
      1 => t('Required'),
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'relation',
    'bundle' => 'openac_course_module',
    'label' => t('This module is required to complete this @lcourse.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'widget' => array(
      'type' => 'options_onoff',
      'settings' => array(
        'display_label' => 0,
      ),
    ),
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
  ),
);

// Course-Module Class
$field_name = 'course_module_class';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'entityreference',
    'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    'settings' => array(
      'target_type' => 'class',
      'handler' => 'base',
    ),
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'relation',
    'bundle' => 'openac_course_module',
    'label' => t('The class to register students of this @lcourse to.', array('@lcourse' => openac_entity_type_label('course', FALSE, TRUE))),
    'widget' => array(
      'type' => 'entityreference_autocomplete_tags',
    ),
  ),
);

// Class Section Summary
$field_name = 'section_summary';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_with_summary',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'class_section',
    'bundle' => 'class_section',
    'label' => t('Summary'),
    'settings' => array(
      'text_processing' => 1,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  ),
);

// Staff Position Fields.
$field_name = 'openac_staff_position';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'profile2',
    'bundle' => 'openac_staff',
    'label' => t('Position'),
  ),
);

// Staff Bio Field
$field_name = 'openac_staff_bio';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'profile2',
    'bundle' => 'openac_staff',
    'label' => t('Bio'),
    'settings' => array(
      'text_processing' => 0,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  ),
);

// Faculty Bio Field
$field_name = 'openac_faculty_bio';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'profile2',
    'bundle' => 'openac_faculty',
    'label' => t('Bio'),
    'settings' => array(
      'text_processing' => 0,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  ),
);

// Student Bio Field
$field_name = 'openac_student_bio';
$fields[$field_name] = array(
  'field' => array(
    'field_name' => $field_name,
    'type' => 'text_long',
    'cardinality' => 1,
  ),
  'instance' => array(
    'field_name' => $field_name,
    'entity_type' => 'profile2',
    'bundle' => 'openac_student',
    'label' => t('Bio'),
    'settings' => array(
      'text_processing' => 0,
    ),
    'widget' => array(
      'type' => 'text_textarea',
    ),
  ),
);